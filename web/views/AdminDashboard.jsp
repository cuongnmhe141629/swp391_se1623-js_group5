<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Dashboard</title>
        <!-- all the meta tags -->
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="shortcut icon" href="/images/favicon.png">
    </head>
    <body>
        <%@include file="AdminMenu_top.jsp" %>
        <!-- PAGE CONTAINER-->
        <div class="content-page">
            <div class="content">
                <!-- BEGIN PlACE PAGE CONTENT HERE -->
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> Dashboard</h4>
                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div>


                <div class="row">
                    <div class="col-12">
                        <div class="card widget-inline">
                            <div class="card-body p-0">
                                <div class="row no-gutters">
                                    <div class="col-sm-6 col-xl-3">

                                        <div class="card shadow-none m-0">
                                            <div class="card-body text-center">
                                                <i class="dripicons-archive text-muted" style="font-size: 24px;"></i>
                                                <h3><span>..</span></h3>
                                                <p class="text-muted font-15 mb-0">Tổng số khóa học</p>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-sm-6 col-xl-3">

                                        <div class="card shadow-none m-0 border-left">
                                            <div class="card-body text-center">
                                                <i class="dripicons-camcorder text-muted" style="font-size: 24px;"></i>
                                                <h3><span>..</span></h3>
                                                <p class="text-muted font-15 mb-0">Tổng số bài giảng</p>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-sm-6 col-xl-3">

                                        <div class="card shadow-none m-0 border-left">
                                            <div class="card-body text-center">
                                                <i class="dripicons-network-3 text-muted" style="font-size: 24px;"></i>
                                                <h3><span>..</span></h3>
                                                <p class="text-muted font-15 mb-0">Tổng số lượt ghi danh</p>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-sm-6 col-xl-3">

                                        <div class="card shadow-none m-0 border-left">
                                            <div class="card-body text-center">
                                                <i class="dripicons-user-group text-muted" style="font-size: 24px;"></i>
                                                <h3><span>..</span></h3>
                                                <p class="text-muted font-15 mb-0">Tổng số người học</p>
                                            </div>
                                        </div>

                                    </div>

                                </div> <!-- end row -->
                            </div>
                        </div> <!-- end card-box-->
                    </div> <!-- end col-->
                </div>
                <script type="text/javascript">
                    $('#unpaid-instructor-revenue').mouseenter(function () {
                        $('#go-to-instructor-revenue').show();
                    });
                    $('#unpaid-instructor-revenue').mouseleave(function () {
                        $('#go-to-instructor-revenue').hide();
                    });
                </script>
            </div>
        </div>
        <!-- END CONTENT -->
        <%@include file="AdminMenu_floor.jsp" %>
    </body>
</html>