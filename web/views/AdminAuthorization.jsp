<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <title>Admin Authorization</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Sarala&display=swap" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.min.css" rel="stylesheet" />
        <!-- Font Awesome -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
        <!-- MDB -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.css" rel="stylesheet" />
        <!-- MDB -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.js"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

        <link href="../css/courseTab.css" rel="stylesheet" />
        <link href="../css/verifyOrder.css" rel="stylesheet" />
        <link href="../css/admin.css" rel="stylesheet" />
    </head>
    <body>
        <div class="wrapper">
            <div class="sidebar">
                <img src="https://www.kindpng.com/picc/m/736-7369377_man-in-green-tie-icon-.png" style="width: 100px; height: 100px; border-radius: 50%; margin-left: 30%; border: 2px solid white;">
                <h5>ADMIN PAGE</h5>
                <a class="Header-A" style="margin-left: 20%">ONLINE LEARN</a>
                <ul class="tabs">
                    <li class="tab"> <a href="/Dashboard" data-switcher data-tab="1"><i class="fas fa-columns"></i>Dashboard</a></li>
                    <li class="tab"> <a href="/UserProflie" data-switcher data-tab="2"><i class="fas fa-user-plus"></i>Hồ Sơ</a></li>
                    <li class="tab active"> <a href="/AdminAuthor" data-switcher data-tab="3"><i class="fas fa-user-shield"></i>Phân quyền</a></li>
                    <li class="tab"><a href="#" data-switcher data-tab="4"><i class="fas fa-stream"></i>Quản lí Người Sử Dụng</a></li>
                    <li class="tab"><a href="#" data-switcher data-tab="5"><i class="fas fa-plus-square"></i>Quản lý Khoá Học</a></li>
                    <!--                <li class="tab"><a href="#" data-switcher data-tab="3"><i class="fas fa-plus-square"></i>Quản lý Bài Giảng</a></li>
                                    <li class="tab"><a href="#" data-switcher data-tab="3"><i class="fas fa-plus-square"></i>Quản lý Câu Hỏi</a></li>
                                    <li class="tab"><a href="#" data-switcher data-tab="3"><i class="fas fa-plus-square"></i>Quản lý Blogs</a></li>
                                    <li class="tab"><a href="#" data-switcher data-tab="3"><i class="fas fa-plus-square"></i>Quản lý Quizs</a></li>-->
                    <li class="tab"><a href="/Home" data-switcher data-tab="6"><i class="fas fa-home"></i>Quay trở về trang chủ</a></li>
                    <li class="tab"><a href="/LogOutServlet" data-switcher data-tab="7"><i class="fas fa-sign-out-alt"></i>Đăng Xuất</a></li>
                </ul>
            </div>
            <div class="main_content" >
                <div class="header">Trang Cho Quản Trị Viên Của ONLINE LEARN</div>
                <div class="info" style="width: auto; height: auto;">
                    <div style="width: 100%; height: 100%; min-height: 600px;">
                        <section class="pages">
                            <div class="page is-active" data-page="2">
                                <div class="header-2">Phân quyền</div>
                                <div id="wapper-verify">
                                    <label>Role</label>&nbsp;&nbsp;
                                    <select name="role" id="name" class="selectpicker" data-style="btn-info">
                                        <c:forEach var="r" items="${lsRole}">
                                            <option value="${r.role_id}">${r.role_name}</option> 
                                        </c:forEach>
                                    </select>

                                    <div class="active-purple-3 active-purple-4 mb-4">
                                        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
                                    </div>
                                    <h3>Thiết lập quyền</h3>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table table-hover">
                                            <thead class="bg-secondary text-white">
                                                <tr>
                                                    <th rowspan="3">Chức năng</th>
                                                        <c:forEach var="a" items="${lsAction}">
                                                        <th rowspan="2">${a.action_name}</th>
                                                        </c:forEach>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach var="f" items="${lsFunction}">
                                                    <tr>
                                                        <td style="font-weight: 600; font-size: 1.2rem">${f.function_name}</td>
                                                        <c:forEach var="a" begin="1" end="4" varStatus="stat" >
                                                            <%--<c:if test="${a.action_id == stat}">--%>
                                                            <td>                                    
                                                                <!--<div class="myTest custom-control custom-checkbox">-->
                                                                <!--<input type="checkbox" class="custom-control-input" id="customCheck1" checked="true">-->
                                                                <!--<label class="custom-control-label" for="customCheck1"></label>-->
                                                                <!--</div>-->
                                                                <%--<c:if test="${lsAction.size()>0}">--%>
                                                                <input type="checkbox" id="ck" name="ck" value="ck" style="width: 35px; height: 35px;" checked="true">
                                                                <%--</c:if>--%>
                                                                <%--<c:if test="${lsAction.size()<0}">--%>
                                                                <!--<input type="checkbox" id="ck" name="ck" value="ck" style="width: 35px; height: 35px">-->
                                                                <%--</c:if>--%>
                                                            </td>   
                                                            <%--</c:if>--%>
                                                            <c:if test="${lsAction.size()<0}">
                                                                <td></td>
                                                            </c:if>
                                                        </c:forEach>
                                                    </tr>                                                                          
                                                </c:forEach>      
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
        <script src="../js/admin.js"></script>
        <script src="../js/courseTab.js"></script>
        <script src="../js/courseTab2.js"></script>
    </body>
</html>
