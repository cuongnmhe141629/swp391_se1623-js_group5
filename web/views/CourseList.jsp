<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Danh sách khóa học</title>  
        <link href="/css/Category.css" rel="stylesheet" />
        <link href="/css/user.css" rel="stylesheet" />
        <link href="/css/style1.css" rel="stylesheet" />

        <style>
            body {
                background-color: #FFFFFF !important;
            }
        </style>
    </head>
    <body>  
        <header>
            <jsp:include page="Header.jsp"></jsp:include>
        </header>
        <c:set var="id" value="${requestScope.caId}" />
        <div id="wrapper">
            <div class="tcnvcd nav nav-tabs" id="nav-tab" role="tablist" style="text-align:center;">
                <c:forEach items="${listCategory}" var="ca">
                    <c:if test="${id == ca.id}">
                        <a class="tcnvcd nav-link active" id="nav-khdm-tab" style="font-size:17px; font-weight:700;" data-bs-target="#khdm" role="tab" href="/CourseList?caId=${ca.id}">
                            ${ca.name}
                        </a>                
                    </c:if>
                    <c:if test="${requestScope.caId != ca.id}">
                        <a class="tcnvcd nav-link" id="nav-khdm-tab" style="font-size:17px; font-weight:700;" data-bs-target="#khdm" role="tab" href="/CourseList?caId=${ca.id}">
                            ${ca.name}
                        </a>                
                    </c:if>
                </c:forEach>
            </div>
            <div id="tabsTop">
                <div class="tabTop">
                    <c:forEach items="${listCourse}" var="c">
                        <div class="item">
                            <div class="card_item">
                                <div class="card_inner">
                                    <a href="/CourseDetail?course_id=${c.id}"><img src="${c.thumbnail}"></a>
                                    <div class="role_name">
                                        <a style="color:#e36686;" href="/CourseDetail?course_id=${c.id}">${c.title}</a>
                                    </div>
                                    <div class="real_name">Gv: ${c.owner}</div>
                                    <c:if test="${c.is_free_course == 0}">
                                        <div class="film">${c.price} VNĐ</div>
                                    </c:if>
                                    <c:if test="${c.is_free_course == 1}">
                                        <div class="film">Miễn phí</div>
                                    </c:if>
                                    <div class="nav-detail">
                                        <a class="register-course" href="/CourseDetail?course_id=${c.id}">Đăng ký ngay</a>
                                        <a class="detail-course" href="/CourseDetail?course_id=${c.id}">Xem chi tiết</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>

                <center>
                    <div class="col-md-2">
                        <ul class="pagination ">
                            <c:set var="page" value="${requestScope.page}" />

                            <c:if test="${page==1}">
                                <li class="page-item disabled "><a class="page-link" href="# " tabindex="-1 ">Previous</a></li>
                                </c:if>

                            <c:if test="${page>1}">
                                <li class="page-item"><a class="page-link" href="<%=request.getContextPath()%>/CourseList?caId=${id}&page=${page-1}">Previous</a></li>
                                </c:if>

                            <c:forEach var="i" begin="1" end="${requestScope.num}">
                                <c:if test="${i==page}">
                                    <li class="page-item active"><a class="page-link" href="<%=request.getContextPath()%>/CourseList?caId=${id}&page=${i}">${i}</a>
                                    </li>
                                </c:if>
                                <c:if test="${i!=page}">
                                    <li class="page-item"><a class="page-link" href="<%=request.getContextPath()%>/CourseList?caId=${id}&page=${i}">${i}</a>
                                    </li>
                                </c:if>
                            </c:forEach>

                            <c:if test="${page==requestScope.num}">
                                <li class="page-item disabled "><a class="page-link " href="# " tabindex="-1 ">Next</a></li>
                                </c:if>
                                <c:if test="${page<requestScope.num}">
                                <li class="page-item"><a class="page-link " href="<%=request.getContextPath()%>/CourseList?caId=${id}&page=${page+1}">Next</a></li>
                                </c:if>
                        </ul>

                        <!--                    <div class="paging">
                                                <a class="active" href="/category?CaID=1&&index=1">1</a>
                                                <a href="/category?CaID=1&&index=2">2</a>-->
                    </div>
                </center>
            </div>
        </div>
        <footer>
            <jsp:include page="Footer.jsp"></jsp:include>
        </footer>
    </div>
</body>
</html>
