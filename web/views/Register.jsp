<%-- 
    Document   : Register
    Created on : Aug 10, 2022, 9:57:04 AM
    Author     : Admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Đăng ký</title>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css" />
        <link href="../css/unicons.css" type="text/css" rel="stylesheet" />
        <link href="../css/semantic/semantic.min.css" type="text/css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
         Difference CSS  
        <link rel="stylesheet" type="text/css" href="../css/home.css" />
        <link rel="stylesheet" type="text/css" href="../css/demo.css" />
        <meta name="csrf-token" content="W2gijDDalPkcKeO8wOWv8hwpSZv2sdNWEUsfphLs" />
        <link rel="stylesheet" type="text/css" href="../css/login.css" />
        <link rel="stylesheet" type="text/css" href="../css/register.css" />
</head>
    <body>
        <header>
            <jsp:include page="Header.jsp"></jsp:include>
            </header>
            <div class="form-ctn">
                <form action="../Register" method="post" onsubmit="return regist();">
                    <input type="hidden" name="_token" value="W2gijDDalPkcKeO8wOWv8hwpSZv2sdNWEUsfphLs">
                    <h1 data-language="14">ĐĂNG KÝ</h1>
                <c:if test="${mess!=null}">
                    <p style="color:red">${mess}</p>

                </c:if>     
                <p class="text-14px" style="color:red">${requestScope.mess}</p>
                <input type="text"  name="fullname" placeholder="Tên đầy đủ" required/>
                <input type="text"  name="phone" pattern="^[0]\d{8,11}$" placeholder="Số điện thoại" required />
                <input type="email" name="email" placeholder="Email" required />
                <div class="gender-radio">
                    <label class="gioi-tinh">Giới Tính</label>
                    <input type='radio' id='male' checked='checked' name='gender' value="0">
                    <label for='male'>Nam</label>
                    <input type='radio' id='female' name='gender' value="1">
                    <label for='female'>Nữ</label>
                </div>


                <input type="password" class="password_form" name="password" placeholder="Mật khẩu" required/>
                <input type="password" class="password_form" name="re_password" placeholder="Nhập lại mật khẩu" required />
                <button class="btn-dangky" type="submit" style="text-transform: uppercase;">ĐĂNG KÝ</button>
                <a href="Login.jsp" >Đã có tài khoản?</a>
                <a class="forgot-pass" href="ResetPassword.jsp">Quên mật khẩu?</a>
            </form>
        </div>
        <footer>
            <jsp:include page="Footer.jsp"></jsp:include>
        </footer>
        <script  type="text/javascript" src="../js/regist.js"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>

        <!-- <script src="css/login/includeHTML.js"></script>
        <script>
            includeHTML();
        </script> -->

    </body>

</html>