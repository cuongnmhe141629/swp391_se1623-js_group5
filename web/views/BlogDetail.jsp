<%@page import="model.User"%>
<%@page import="model.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js" integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    </head>
    <body>
        <header>
            <jsp:include page="Header.jsp"></jsp:include>
            </header>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <div class="container mt-5">
                <div class="row">
                    <div class="col-lg-8">
                        <article>
                            <header class="mb-4">
                                <h1 class="fw-bolder mb-1">${blog.title}</h1>
                            <div class="text-muted fst-italic mb-2">${blog.author}</div>
                            <div class="text-muted fst-italic mb-2">${blog.title_blog}</div>
                            <div class="text-muted fst-italic mb-2">${blog.added_date}</div>
                        </header>
                        <figure class="mb-4"><img class="img-fluid rounded" src="${blog.thumbnail}" alt="" /></figure>
                        <!-- Post content-->
                        <section class="mb-5">
                            <p class="fs-5 mb-4">${blog.description}</p>
                        </section>
                    </article>
                    <h1>Comment</h1>
                    <section class="mb-2">
                        <div class="card bg-light">
                            <div class="card-body">
                                <form  class="mb-4" method="POST" action="blogdetail">
                                    <input hidden="" type="text" name="txtblogid" value="${blog.blog_id}">
                                    <input hidden="" type="text" name="txtuserid" value="${blog.user_id}">
                                    <input hidden="" type="text" name="txtstatus" value="1">
                                    <textarea class="form-control" rows="3" placeholder="Join the discussion and leave a comment!" name="txtcomment"></textarea>
                                    <button>Send</button>
                                </form>
                            </div>
                            <c:forEach items="${blog_comments}" var="bc">
                                <div class="d-flex mb-4">
                                    <div class="flex-shrink-0"><img class="rounded-circle" src="blog_comments" alt="" /></div>
                                    <div class="ms-3">
                                        <div class="fw-bold">${bc.name}</div>
                                        <div class="fw-bold">${bc.comment}</div> 
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </body>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js" integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous"></script>
</html>