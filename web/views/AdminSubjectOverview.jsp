<%-- 
    Document   : AdminSubjectOverview
    Created on : Oct 18, 2022, 3:55:31 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Enchanter</title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="post" id="registration">
                            <nav>
                                <div class="nav nav-pills nav-fill" id="nav-tab" role="tablist">
                                    <a class="nav-link active" id="step1-tab" data-bs-toggle="tab" href="#step1">Step 1</a>
                                    <a class="nav-link" id="step2-tab" data-bs-toggle="tab" href="#step2">Step 2</a>
                                    <a class="nav-link" id="step3-tab" data-bs-toggle="tab" href="#step3">Step 3</a>
                                </div>
                            </nav>
                            <div class="tab-content py-4">
                                <div class="tab-pane fade show active" id="step1">
                                    <h4>Personal data</h4>
                                    <div class="mb-3">
                                        <label for="field1">Required text field 1</label>
                                        <input type="text" name="field1" class="form-control" id="field1" required>
                                    </div>
                                    <div class="mb-3">
                                        <label for="field2">Required email field 2</label>
                                        <input type="email" name="field2" class="form-control" id="field2" required>
                                    </div>
                                    <div class="mb-3">
                                        <label for="field3">Optional field</label>
                                        <input type="text" name="field3" class="form-control" id="field3">
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="step2">

                                    <h4>Contact information</h4>
                                    <div class="mb-3">
                                        <label for="field4">Required field 1</label>
                                        <input type="text" name="field4" class="form-control" id="field4" required>
                                    </div>
                                    <div class="mb-3">
                                        <label for="field5">Optional field</label>
                                        <input type="text" name="field5" class="form-control" id="field5">
                                    </div>
                                    <div class="mb-3">
                                        <label for="textarea1">Required field 2</label>
                                        <textarea name="textarea1" rows="5" class="form-control" id="textarea1" required></textarea>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="step3">

                                    <h4 class="mb-3 header-title">Subject dimension</h4> 
                                    <div class="table-responsive-sm mt-4">
                                        <a href="./add_new_course.html" class="btn btn-outline-primary btn-rounded alignToTitle"><i class="mdi mdi-plus"></i>Add new subject dimension</a>
                                        <table id="basic-datatable" class="table table-striped table-centered mb-0">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Title</th>
                                                    <th>Category</th>
                                                    <th>Lesson and section</th>
                                                    <th>Enrolled student</th>
                                                    <th>Price</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <tr>
                                                    <td>${o.id}</td>
                                                    <td>${o.title}</td>
                                                    <td>Category</td>
                                                    <td>Total section: 0 
                                                        <br>
                                                        Total lesson: 0</td>
                                                    <td>Total enrolment: 0</td>
                                                    <td> ${o.price}</td>
                                                    <td>
                                                        <div class="dropright dropright">
                                                            <button type="button" class="btn btn-sm btn-outline-primary btn-rounded btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="mdi mdi-dots-vertical"></i>
                                                            </button>
                                                            <ul class="dropdown-menu">
                                                                <li><a class="btn btn-sm btn-outline-primary btn-rounded btn-icon" href="AdminCourseDetail?id=${o.id}">
                                                                        <i class="mdi mdi-dots-vertical">Tùy chỉnh</i>
                                                                    </a></li>
                                                                <li><a class="dropdown-item" href="AdminDeleteCourse?id=${o.id}" onclick="#">Xóa</a></li>
                                                            </ul>
                                                        </div>

                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>

                                    </div>
                                </div>

                                <!--                            <div class="tab-pane fade" id="step3">
                                                                <h4>Review your information</h4>
                                                                <div class="mb-3">
                                                                    <label for="first_name">Required field 1</label>
                                                                    <input type="text" class="form-control-plaintext" value="Lorem ipsum dolor sit amet">
                                                                </div>
                                                                <div class="mb-3">
                                                                    <label for="first_name">Optional field</label>
                                                                    <input type="text" class="form-control-plaintext" value="Lorem ipsum dolor sit amet">
                                                                </div>
                                                                <div class="mb-3">
                                                                    <label for="first_name">Required field 2</label>
                                                                    <input type="text" class="form-control-plaintext" value="Lorem ipsum dolor sit amet">
                                                                </div>
                                                            </div>-->
                            </div>
                            <div class="row justify-content-between">
                                <div class="col-auto"><button type="button" class="btn btn-secondary" data-enchanter="previous">Previous</button></div>
                                <div class="col-auto">
                                    <button type="button" class="btn btn-primary" data-enchanter="next">Next</button>
                                    <button type="submit" class="btn btn-primary" data-enchanter="finish">Finish</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- JavaScript and dependencies -->
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
            <!-- JavaScript for validations only -->
            <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
            <!-- Our script! :) -->
            <script src="/js/enchanter.js"></script>
            <script>
                                                                        var registrationForm = $('#registration');
                                                                        var formValidate = registrationForm.validate({
                                                                            errorClass: 'is-invalid',
                                                                            errorPlacement: () => false
                                                                        });

                                                                        const wizard = new Enchanter('registration', {}, {
                                                                            onNext: () => {
                                                                                if (!registrationForm.valid()) {
                                                                                    formValidate.focusInvalid();
                                                                                    return false;
                                                                                }
                                                                            }
                                                                        });
            </script>
    </body>
</html>

