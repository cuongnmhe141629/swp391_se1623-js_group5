
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8" />
        <title>Đổi mật khẩu</title>
        <link rel="stylesheet" href="../css/create_new_password.css" />
        <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css"/>

        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    </head>
    <body>
        <header>
            </header>
            <div class="wrapper-login">
            <header>ĐỔI MẬT KHẨU</header>

            <div class="form-container">
                <div class="form-inner">
                    <form action="../ResetPasswordServlet" method="POST" class="signup">
                        <p style="color: red"class="text-danger">${requestScope.mess}</p>

                        <div class="input-box">
                            <input
                                class="p-input"
                                name="npass"
                                type="password"
                                spellcheck="false"
                                required
                                />
                            <label for="">Mật khẩu mới</label>
                            <i class="uil uil-eye-slash toggle"></i>
                        </div>

                        <div class="input-box">
                            <input
                                class="p-inputt"
                                name="repass"
                                type="password"
                                spellcheck="false"
                                required
                                />
                            <label for="">Nhập lại mật khảu</label>
                            <i class="uil uil-eye-slash togglee"></i>
                        </div>
                        <div class="field btn">
                            <div class="btn-layer"></div>
                            <input type="hidden" name="email" value="${requestScope.mail}"/>
                            <input type="hidden" name="choise" value="forgot"/>
                            <input type="submit" value="Lưu" />
                        </div>
                    </form>
                </div>
            </div>
        </div>         

        <script src="../js/resetPassword.js"></script>
    </body>
</html>
