<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Cập nhật</title>
        <!-- all the meta tags -->
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="shortcut icon" href="/images/favicon.png">
    </head>
    <body>
        <%@include file="AdminMenu_top.jsp" %>
        <!-- PAGE CONTAINER-->
        <div class="content-page">
            <div class="content">
                <!-- BEGIN PlACE PAGE CONTENT HERE -->
                <div class="row ">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6"><h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon">Detail and Update Lesson</i></h4></div>
                                    <div class="col-md-6"><a href="/AdminLessonServlet?id=${les.course_id}" class="alignToTitle btn btn-outline-secondary btn-rounded btn-sm my-1"> <i class=" mdi mdi-keyboard-backspace"></i>Trở lại danh sách Lesson</a></div>                               
                                </div> <!-- end card body-->
                            </div>
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div>

                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="header-title my-1">Update Lesson</h4>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xl-12">
                                        <form class="required-form" action="/AdminLessonDetail?l_id=${les.id}&id=${les.course_id}" method="post" enctype="multipart/form-data">
                                            <div id="basicwizard">

                                                <ul class="nav nav-pills nav-justified form-wizard-header">
                                                    <li class="nav-item">
                                                        <a href="#basic" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                                            <i class="mdi mdi-fountain-pen-tip"></i>
                                                            <span class="d-none d-sm-inline">Lesson${les.course_id} </span>
                                                        </a>
                                                    </li>



                                                    <li class="w-100 bg-white pb-3">
                                                        <!--ajax page loader-->
                                                        <div class="ajax_loader w-100">
                                                            <div class="ajax_loaderBar"></div>
                                                        </div>
                                                        <!--end ajax page loader-->
                                                    </li>
                                                </ul>

                                                <div class="tab-content b-0 mb-0">
                                                    <div class="tab-pane" id="basic">
                                                        <div class="row justify-content-center">
                                                            <div class="col-xl-8">
                                                                <input type="hidden" name = "course_type" value="general">
                                                                <div class="form-group row mb-3">
                                                                    <label class="col-md-3 col-form-label">Title: <span class="required">*</span> </label>
                                                                    <div class="col-md-9">
                                                                        <input type="text" class="form-control" id="course_title" name = "title" placeholder="Nhập title" value="${les.title}" required>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-3">
                                                                    <label class="col-md-3 col-form-label">Topic_id: <span class="required">*</span> </label>
                                                                    <div class="col-md-9">
                                                                        <input type="number" class="form-control" id="course_title" name = "Topic_id" placeholder="Nhập topic_id" value="${les.topic_id}"  required>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-3">
                                                                    <label class="col-md-3 col-form-label">lesson_type: <span class="required">*</span> </label>
                                                                    <div class="col-md-9">
                                                                        <input type="text" class="form-control" id="course_title" name = "lesson_type" placeholder="Nhập type" value="${les.lesson_type}"  required>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-3">
                                                                    <label class="col-md-3 col-form-label">Summary</label>
                                                                    <div class="col-md-9">
                                                                        <textarea name="summary" rows="5" id = "description" class="form-control">${les.summary}</textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-3">
                                                                    <label class="col-md-3 col-form-label">Video</label>
                                                                    <div class="col-md-9">
                                                                        <input type="file" class="form-control" id="course_title" accept=".mp4" placeholder="File" name="video" value="${les.video_url}">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-3">
                                                                    <div class="offset-md-2 col-md-10">
                                                                        <div class="custom-control custom-checkbox">
                                                                            <button type="submit" id="submit" class="btn btn-info offset-5">Lưu</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div> <!-- end col -->
                                                        </div> <!-- end row -->
                                                    </div> <!-- end tab pane -->
                                                </div> <!-- tab-content -->
                                            </div> <!-- end-->
                                            <div style="color: red;">${success}</div>
                                        </form>
                                    </div>
                                </div><!-- end row-->
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div>
                </div>

                <script type="text/javascript">
                    $(document).ready(function () {
                        initSummerNote(['#description']);
                    });
                </script>

                <script type="text/javascript">
                    var blank_outcome = jQuery('#blank_outcome_field').html();
                    var blank_requirement = jQuery('#blank_requirement_field').html();
                    jQuery(document).ready(function () {
                        jQuery('#blank_outcome_field').hide();
                        jQuery('#blank_requirement_field').hide();
                    });
                    function appendOutcome() {
                        jQuery('#outcomes_area').append(blank_outcome);
                    }
                    function removeOutcome(outcomeElem) {
                        jQuery(outcomeElem).parent().parent().remove();
                    }

                    function appendRequirement() {
                        jQuery('#requirement_area').append(blank_requirement);
                    }
                    function removeRequirement(requirementElem) {
                        jQuery(requirementElem).parent().parent().remove();
                    }

                    function priceChecked(elem) {
                        if (jQuery('#discountCheckbox').is(':checked')) {

                            jQuery('#discountCheckbox').prop("checked", false);
                        } else {

                            jQuery('#discountCheckbox').prop("checked", true);
                        }
                    }
                </script>

                <style media="screen">
                    body {
                        overflow-x: hidden;
                    }
                </style>
                <!-- END PLACE PAGE CONTENT HERE -->
            </div>
        </div>
        <!-- END CONTENT -->
        <%@include file="AdminMenu_floor.jsp" %>
    </body>
</html>