Bá Tuấn
<%@page import="dao.BlogDAO"%>
<%@page import="model.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js" integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    </head>
    <body>
        <header>
            <jsp:include page="Header.jsp"></jsp:include>
            </header>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <section class="jumbotron text-center">
                <div class="container">
                    <h1 class="jumbotron-heading">Blogs</h1>
                    <p class="lead text-muted mb-0">Một vài bài viết cho mọi người giúp tìm hiểu và chọn khóa học phù hợp</p>
                </div>
                <section class="search-section ss-other-page">
                    <div class="container">
                        <div class="search-warp">
                            <div class="section-title text-white">
                                <h2><span>Search your course</span></h2>
                            </div>
                            <div class="row">
                                <div class="col-lg-10 offset-lg-1">
                                    <form class="course-search-form" action="blogsearch" method="POST">
                                        <input type="text" placeholder="Input name title of course" name="txtCourse">
                                        <button class="site-btn btn-dark">Search Couse</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section><br/>
                <section class="blog-page spad pb-0">
                    <div class="container">
                        <div class="row">
                        <c:forEach items="${blogs}" var="b">
                            <div class="col-lg-9">
                                <div class="blog-post" style="border: solid">
                                    <img src="<%=request.getContextPath()%>/images/${b.thumbnail}" width="200" height="200" alt="">
                                    <p>${b.thumbnail}</p>
                                    <h5>${b.title}</h5>
                                    <div class="blog-metas">
                                        <div class="blog-meta">
                                            Write By:${b.author}
                                        </div>
                                        <div class="blog-meta">
                                            Type:${b.title_blog}
                                        </div>
                                        <div class="blog-meta">
                                            Date:${b.added_date}
                                        </div>
                                    </div>
                                    <p>Summary:${b.brief_infor}</p>
                                    <a href="blogdetail?blog_id=${b.blog_id}" class="site-btn readmore">Read More</a>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </section>
            <nar aria-lable="...">
                <ul class="pagination pagination-lg">
                    <c:forEach begin="1" end="${numberPage}" var="i">
                        <li class="page-item"><a class="page-link" href="BlogController?index=${i}">${i}</a></li>
                        </c:forEach>
                </ul>
            </nar>
        </section>
        <script type="text/javascript">
            $(document).ready(function () {
                initSummerNote(['#description']);
            });
        </script>

        <script type="text/javascript">
            var blank_outcome = jQuery('#blank_outcome_field').html();
            var blank_requirement = jQuery('#blank_requirement_field').html();
            jQuery(document).ready(function () {
                jQuery('#blank_outcome_field').hide();
                jQuery('#blank_requirement_field').hide();
            });
            function appendOutcome() {
                jQuery('#outcomes_area').append(blank_outcome);
            }
            function removeOutcome(outcomeElem) {
                jQuery(outcomeElem).parent().parent().remove();
            }

            function appendRequirement() {
                jQuery('#requirement_area').append(blank_requirement);
            }
            function removeRequirement(requirementElem) {
                jQuery(requirementElem).parent().parent().remove();
            }

            function priceChecked(elem) {
                if (jQuery('#discountCheckbox').is(':checked')) {

                    jQuery('#discountCheckbox').prop("checked", false);
                } else {

                    jQuery('#discountCheckbox').prop("checked", true);
                }
            }

            function topCourseChecked(elem) {
                if (jQuery('#isTopCourseCheckbox').is(':checked')) {

                    jQuery('#isTopCourseCheckbox').prop("checked", false);
                } else {

                    jQuery('#isTopCourseCheckbox').prop("checked", true);
                }
            }

            function isFreeCourseChecked(elem) {

                if (jQuery('#' + elem.id).is(':checked')) {
                    $('#price').prop('required', false);
                } else {
                    $('#price').prop('required', true);
                }
            }

            function calculateDiscountPercentage(discounted_price) {
                if (discounted_price > 0) {
                    var actualPrice = jQuery('#price').val();
                    if (actualPrice > 0) {
                        var reducedPrice = actualPrice - discounted_price;
                        var discountedPercentage = (reducedPrice / actualPrice) * 100;
                        if (discountedPercentage > 0) {
                            jQuery('#discounted_percentage').text(discountedPercentage.toFixed(2) + '%');

                        } else {
                            jQuery('#discounted_percentage').text('0%');
                        }
                    }
                }
            }

        </script>

        <style media="screen">
            body {
                overflow-x: hidden;
            }
        </style>
        <footer>
            <jsp:include page="Footer.jsp"></jsp:include>
        </footer>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>
        <script src="../js/script.js" defer></script>
    </body>
</html>