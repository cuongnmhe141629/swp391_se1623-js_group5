<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Thêm mới khóa học</title>
        <!-- all the meta tags -->
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="shortcut icon" href="/images/favicon.png">
    </head>
    <body>
        <%@include file="AdminMenu_top.jsp" %>
        <!-- PAGE CONTAINER-->
        <div class="content-page">
            <div class="content">
                <!-- BEGIN PlACE PAGE CONTENT HERE -->
                <div class="row ">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6"><h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i>Thêm khóa học mới</h4></div>
                                    <div class="col-md-6"><a href="/AdminCourse" class="alignToTitle btn btn-outline-secondary btn-rounded btn-sm my-1"> <i class=" mdi mdi-keyboard-backspace"></i> Back to course list</a></div>                               
                                </div> <!-- end card body-->
                            </div>
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div>

                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="header-title my-1">Thêm khóa học </h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xl-12">
                                        <form class="required-form" action="/AdminAddCourse" method="post" enctype="multipart/form-data">
                                            <div id="basicwizard">

                                                <ul class="nav nav-pills nav-justified form-wizard-header">
                                                    <li class="nav-item">
                                                        <a href="#basic" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                                            <i class="mdi mdi-fountain-pen-tip"></i>
                                                            <span class="d-none d-sm-inline">T</span>
                                                        </a>
                                                    </li>



                                                    <li class="w-100 bg-white pb-3">
                                                        <div class="ajax_loader w-100">
                                                            <div class="ajax_loaderBar"></div>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <div class="tab-content b-0 mb-0">
                                                    <div class="tab-pane" id="basic">
                                                        <div class="row justify-content-center">
                                                            <div class="col-xl-8">
                                                                <input type="hidden" name = "course_type" value="general">
                                                                <div class="form-group row mb-3">
                                                                    <label class="col-md-2 col-form-label" for="course_title">Course title <span class="required">*</span> </label>
                                                                    <div class="col-md-10">
                                                                        <input type="text" class="form-control" placeholder="Name" name="txtType" required>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-3">
                                                                    <label class="col-md-2 col-form-label" for="description">Description</label>
                                                                    <div class="col-md-10">
                                                                        <textarea class="form-control" rows="5" name="description"></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-3">
                                                                    <label class="col-md-2 col-form-label">thumbnail </label>
                                                                    <div class="col-md-10">
                                                                        <input type="file" class="form-control" placeholder="File" name="image" required>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-3">
                                                                    <label class="col-md-2 col-form-label" for="category">Category <span class="required">*</span> </label>
                                                                    <div class="col-md-10">
                                                                        <select class="form-select" aria-label="Default select example" name="txtcategory">
                                                                            <c:forEach items="${categoris}" var="category">
                                                                                <option value="${category.id}">${category.name}</option>
                                                                            </c:forEach>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-3">
                                                                    <label class="col-md-2 col-form-label" for="owner">Owner <span class="required">*</span> </label>
                                                                    <div class="col-md-10">
                                                                        <select class="form-select" aria-label="Default select example" name="txtuser">
                                                                            <c:forEach items="${users}" var="user">
                                                                                <option value="${user.user_id}">${user.fullname}</option>
                                                                            </c:forEach>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-3">
                                                                    <label class="col-md-2 col-form-label" for="status">Status</label>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" name="status" value="true">ON
                                                                    </div>
                                                                    <div class="form-check mx-3">
                                                                        <input class="form-check-input" type="radio" name="status" value="false">OFF                                                  
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-3">
                                                                    <div class="offset-md-2 col-md-10">
                                                                        <div class="custom-control custom-checkbox">
                                                                            <button type="submit" id="submit" class="btn btn-info offset-5">Submit</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div> <!-- end col -->
                                                        </div> <!-- end row -->
                                                    </div> <!-- end tab pane -->
                                                </div> <!-- tab-content -->
                                            </div> <!-- end-->
                                        </form>
                                    </div>
                                </div><!-- end row-->
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div>
                </div>

                <script type="text/javascript">
                    $(document).ready(function () {
                        initSummerNote(['#description']);
                    });
                </script>

                <script type="text/javascript">
                    var blank_outcome = jQuery('#blank_outcome_field').html();
                    var blank_requirement = jQuery('#blank_requirement_field').html();
                    jQuery(document).ready(function () {
                        jQuery('#blank_outcome_field').hide();
                        jQuery('#blank_requirement_field').hide();
                    });
                    function appendOutcome() {
                        jQuery('#outcomes_area').append(blank_outcome);
                    }
                    function removeOutcome(outcomeElem) {
                        jQuery(outcomeElem).parent().parent().remove();
                    }

                    function appendRequirement() {
                        jQuery('#requirement_area').append(blank_requirement);
                    }
                    function removeRequirement(requirementElem) {
                        jQuery(requirementElem).parent().parent().remove();
                    }

                    function priceChecked(elem) {
                        if (jQuery('#discountCheckbox').is(':checked')) {

                            jQuery('#discountCheckbox').prop("checked", false);
                        } else {

                            jQuery('#discountCheckbox').prop("checked", true);
                        }
                    }

                    function topCourseChecked(elem) {
                        if (jQuery('#isTopCourseCheckbox').is(':checked')) {

                            jQuery('#isTopCourseCheckbox').prop("checked", false);
                        } else {

                            jQuery('#isTopCourseCheckbox').prop("checked", true);
                        }
                    }

                    function isFreeCourseChecked(elem) {

                        if (jQuery('#' + elem.id).is(':checked')) {
                            $('#price').prop('required', false);
                        } else {
                            $('#price').prop('required', true);
                        }
                    }

                    function calculateDiscountPercentage(discounted_price) {
                        if (discounted_price > 0) {
                            var actualPrice = jQuery('#price').val();
                            if (actualPrice > 0) {
                                var reducedPrice = actualPrice - discounted_price;
                                var discountedPercentage = (reducedPrice / actualPrice) * 100;
                                if (discountedPercentage > 0) {
                                    jQuery('#discounted_percentage').text(discountedPercentage.toFixed(2) + '%');

                                } else {
                                    jQuery('#discounted_percentage').text('0%');
                                }
                            }
                        }
                    }

                </script>

                <style media="screen">
                    body {
                        overflow-x: hidden;
                    }
                </style>
                <!-- END PLACE PAGE CONTENT HERE -->
            </div>
        </div>
        <!-- END CONTENT -->
        <%@include file="AdminMenu_floor.jsp" %>
    </body>
</html>