<%-- 
    Document   : UserProfile
    Created on : Aug 16, 2022, 12:37:26 AM
    Author     : Admin
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Hồ sơ cá nhân</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
              crossorigin="anonymous" />
        <link rel="stylesheet" type="text/css"
              href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css" />
        <link rel="stylesheet" type="text/css"
              href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css" />
        <link href="/css/unicons/css/unicons.css" type="text/css"
              rel="stylesheet" />
        <link href="/css/semantic/semantic.min.css" type="text/css"
              rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
        <!-- Difference CSS  -->
        <link rel="stylesheet" type="text/css" href="/css/home.css" />
        <link rel="stylesheet" type="text/css" href="/css/demo.css" />
        <meta name="csrf-token" content="tTXT53Dve8x0Vxi1Y81JSlQBsolCQyhVFDWCJqph" />
    </head>

    <body class="tcnvcd">
        <header>
            <jsp:include page="Header.jsp"></jsp:include>
            </header>

            <div class="tcnvcd breadcrumb-ctn">

            </div>
            <section class="tcnvcd ctn-sn ctn-sn-1">
                <div class="tcnvcd nav nav-tabs" id="nav-tab" role="tablist">
                    <a style="text-align: center" class="tcnvcd nav-link active" id="nav-hscn-tab" data-bs-target="#hscn" role="tab"
                       href="/UserProfile">
                        HỒ SƠ CÁ NHÂN
                    </a>
                    <a style="text-align: center" class="tcnvcd nav-link" id="nav-khdm-tab" data-bs-target="#khdm" role="tab"
                       href="/mycourse">
                        KHÓA HỌC CỦA TÔI
                    </a>
                    <a style="text-align: center" class="tcnvcd nav-link" id="nav-khdm-tab" data-bs-target="#khdm" role="tab"
                       href="#">
                        KHÓA HỌC YÊU THÍCH
                    </a>
                </div>

                <div class="tcnvcd sn tab-content" id="nav-tabContent">
                    <div class="tcnvcd tab-pane fade" id="khdm" role="tabpanel" aria-labelledby="nav-khdm-tab">
                        <div class="tcnvcd khdm">
                            <div class="tcnvcd slider slider-content1" style="color: black">
                            </div>
                        </div>
                    </div>
                    <div class="tcnvcd tab-pane fade show active" id="hscn" role="tabpanel" aria-labelledby="nav-hscn-tab">
                        <form action="/UserProfile?user_id=${user.user_id}" method="post" enctype="multipart/form-data">
                            <input hidden="true" type="email" name="email" value="${user.email}"/>
                        <input type="hidden" name="_token" value="tTXT53Dve8x0Vxi1Y81JSlQBsolCQyhVFDWCJqph">
                        <div class="tcnvcd hscn">
                            <div>
                                <div>
                                    <!--<img src="https://davidfc.vn/img/avatar/default.jpg" alt="" />-->
                                </div>
                                <div style="margin-left: 5%">
                                    <c:if test="${user.image == null || user.image.isEmpty()}">
                                        <p><img style="margin-left: 25%; border: 2px inset thistle" class="circular--portrait" id="avar" src="../images/user-profile.jpg"/></p></br>
                                        </c:if>
                                        <c:if test="${user.image != null && !user.image.isEmpty()}">
                                        <p><img style="margin-left: 25%; border: 2px inset thistle" class="circular--portrait" id="avar" src="${user.image}"/></p></br>
                                        </c:if>
                                    <div class="input-group custom-file-button">
                                        <input type="file" class="form-control" id="inputGroupFile" name="photo" accept="image/*" onchange="loadFile(event)" value="${user.image}"/>
                                        <label hidden="true" class="form-label" for="inputGroupFile">${user.image}</label>
                                    </div>
                                    <!--<input style="background-color: #a9d5de; color: maroon" type="file" name="photo" placeholder="Upload photo" accept="image/*" onchange="loadFile(event)"></br>-->
                                    <script>
                                        var loadFile = function (event) {
                                            var image = document.getElementById('avar');
                                            image.src = URL.createObjectURL(event.target.files[0]);
                                        };
                                    </script>

                                    <label class="gioi-tinh" style="margin-bottom: 15px">Giới tính</label>&emsp;&emsp;
                                    <div style="display: inline-block" class="gender-radio">
                                        <c:if test="${user.gender}">
                                            <input class="form-check-input" type='radio' id='male' checked='checked' name='gender' value="0">
                                            <label for='male'>Nam</label>&nbsp;&ensp;&ensp;&nbsp;&nbsp;
                                            <input class="form-check-input" type='radio' id='female' name='gender' value="1">
                                            <label for='female'>Nữ</label>
                                        </c:if>
                                        <c:if test="${!user.gender}">
                                            <input class="form-check-input" type='radio' id='male' name='gender' value="0">
                                            <label for='male'>Nam</label>&nbsp;&ensp;&ensp;&nbsp;&nbsp;
                                            <input class="form-check-input" type='radio' id='female' checked='checked' name='gender' value="1">
                                            <label for='female'>Nữ</label>
                                        </c:if>
                                    </div>
                                </div>
                                <div style="margin-right: 5%">
                                    <label for="hovaten" class="tcnvcd form-label">Họ và tên</label>
                                    <input type="text" class="tcnvcd form-control" id="hovaten" name="fullname"
                                           value="${user.fullname}" maxlength="30" required />
                                    <label for="email" class="tcnvcd form-label" >Email</label><br>
                                    <div class="pass-container">
                                        <input type="text" class="text-box" name="email" id="email" value="${user.email}"
                                               disabled /><hr style="border-top: 1px solid black">
                                    </div>
                                    <label for="password" class="tcnvcd form-label">Mật khẩu</label>
                                    <style>
                                        .pass-container {
                                            position: relative;
                                        }
                                        .text-box {
                                            background-color: transparent;
                                            outline: none;
                                            height: 25px;
                                            width: 300px;
                                            border: 0;
                                        }
                                        .changePass {
                                            position: absolute;
                                            right: 0;
                                            top: 0;
                                            background-color: #C00;
                                            border: 0;
                                            color: #FFF;
                                            width: 130px;
                                            height: 35px;
                                            outline: 0;
                                        }
                                    </style>
                                    <div class="pass-container">
                                        <input type="password" class="text-box" id="password" name="password" value="12345678" disabled/><hr style="border-top: 1px solid black">
                                        <a href="/ChangePassword"><input type="button" value="Thay đổi mật khẩu" class="changePass"/></a>
                                    </div>
                                    <label for="sdt" class="tcnvcd form-label">Số điện thoại</label>
                                    <input type="text" name="phone" class="tcnvcd form-control" id="sdt"
                                           value="${user.phone}" pattern="^[0]\d{8,11}$" title="Please enter correct format phone number!" required/>
                                </div>
                            </div>
                            <button style="margin-right: 10%" type="submit" class="tcnvcd btn btn-primary">
                                Lưu thông tin
                            </button>
                            <div class="form-group row w-75 ml-3 mt-2">
                                <div style="color: red;">${success}</div> 
                            </div>
                            <div style="clear: both"></div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <footer>
            <jsp:include page="Footer.jsp"></jsp:include>
        </footer>
        <c:if test="${requestScope.success ne null}">
            <script type="text/javascript"> window.onload = function success() {
                    alert("Chỉnh sửa thông tin cá nhân thành công!");
                };</script>
        </c:if>
        <c:if test="${requestScope.error ne null}">
            <script type="text/javascript"> window.onload = function error() {
                    alert("Có lỗi. Vui lòng nhập lại!");
                };</script>
        </c:if>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-migrate-3.0.1.min.js"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous">
        </script>
        <script src="/js/index.js" defer></script>
    </body>
</html>
