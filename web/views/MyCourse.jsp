<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Khoa hoc da mua</title>
        <link href="/css/user.css" rel="stylesheet" />
        <link href="/css/style1.css" rel="stylesheet" />
        <style>
            body {
                background-color: #FFFFFF !important;
            }
        </style>
    </head>
    <body>
        <header>
            <jsp:include page="Header.jsp"></jsp:include>
            </header>
            <div>
                <div id="wapper-mycourse">
                    <section class="tcnvcd ctn-sn ctn-sn-1" style="margin-top:110px">
                        <div class="tcnvcd nav nav-tabs" id="nav-tab" role="tablist">
                            <a style="text-align: center" class="tcnvcd nav-link" id="nav-hscn-tab" data-bs-target="#hscn" role="tab"
                               href="/UserProfile">
                                HỒ SƠ CÁ NHÂN
                            </a>
                            <a style="text-align: center" class="tcnvcd nav-link active" id="nav-khdm-tab" data-bs-target="hscn" role="tab"
                               href="/mycourse">
                                KHÓA HỌC CỦA TÔI
                            </a>
                            <a style="text-align: center" class="tcnvcd nav-link" id="nav-khdm-tab" data-bs-target="#khdm" role="tab"
                               href="#">
                                KHÓA HỌC YÊU THÍCH
                            </a>
                        </div>
                        <div class="khoahoc ctn-sn ctn-sn-1">
                            <div id="sidebar" class="khoahoc sidebar">
                                <div class="khoahoc bar bar-2">
                                    <div class="khoahoc bar-head">
                                        <h2 class="khoahoc bar-head__title">KHÓA HỌC ĐỀ XUẤT NÊN MUA</h2>
                                    </div>
                                    <div class="khoahoc bar-content">
                                        <ul>
                                            <li>
                                                <div>
                                                    <div class="img-div">
                                                        <a href="/detailcourse?CourseID=DP01">
                                                            <img src="/Content/images/kh9.jpg" alt="" />
                                                        </a>
                                                    </div>
                                                    <a href="/detailcourse?CourseID=DP01">
                                                        <div>Dựng Phim Chuy&#234;n Nghiệp</div>
                                                        <div>Xem chi tiết</div>
                                                    </a>
                                                </div>
                                            </li>
                                            <li>
                                                <div>
                                                    <div class="img-div">
                                                        <a href="/detailcourse?CourseID=NN02">
                                                            <img src="/Content/images/nn3.jpg" alt="" />
                                                        </a>
                                                    </div>
                                                    <a href="/detailcourse?CourseID=NN02">
                                                        <div>Học IELTS cho từ số 0</div>
                                                        <div>Xem chi tiết</div>
                                                    </a>
                                                </div>
                                            </li>
                                            <li>
                                                <div>
                                                    <div class="img-div">
                                                        <a href="/detailcourse?CourseID=KNM03">
                                                            <img src="/Content/images/kn2.jpg" alt="" />
                                                        </a>
                                                    </div>
                                                    <a href="/detailcourse?CourseID=KNM03">
                                                        <div>Kỹ năng viết</div>
                                                        <div>Xem chi tiết</div>
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="khoahoc lst-crse">
                                <div class="khoahoc owl-item active">
                                    <div class="khoahoc item">
                                        <div class="khoahoc fcrse_1">
                                            <a href="/detailcourse?CourseID=LTC01"
                                               class="khoahoc fcrse_img">
                                                <img src="/Content/images/kh1.jpg" alt="" />
                                            </a>
                                            <div class="khoahoc fcrse_content">
                                                <h3 class="khoahoc bl-title">
                                                    <a href="/detailcourse?CourseID=LTC01">Lập tr&#236;nh C/C++</a>
                                                </h3>
                                                <div>
                                                    <p style="margin-left: 19px">GV: Sir Alex  Ferguson</p>
                                                    <div>
                                                        <div class="khoahoc auth1lnkprce">
                                                            <div class="khoahoc education_block_author">
                                                                <h5><a href="/Study?courseID=LTC01&&videoID=1" tabindex="-1">Vào Học</a></h5>


                                                            </div>
                                                            <a href="/detailcourse?CourseID=LTC01">
                                                                <div class="khoahoc shrt-cart-btn">
                                                                    <i class="khoahoc"></i>
                                                                    <span>Xem chi tiết</span>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="khoahoc owl-item active">
                                    <div class="khoahoc item">
                                        <div class="khoahoc fcrse_1">
                                            <a href="/detailcourse?CourseID=LTCS"
                                               class="khoahoc fcrse_img">
                                                <img src="/Content/images/kh2.jpg" alt="" />
                                            </a>
                                            <div class="khoahoc fcrse_content">
                                                <h3 class="khoahoc bl-title">
                                                    <a href="/detailcourse?CourseID=LTCS">Lập tr&#236;nh C#</a>
                                                </h3>
                                                <div>
                                                    <p style="margin-left: 19px">GV: Jeff Bezos</p>
                                                    <div>
                                                        <div class="khoahoc auth1lnkprce">
                                                            <div class="khoahoc education_block_author">
                                                                <h5><a href="/Study?courseID=LTCS&&videoID=22" tabindex="-1">Vào Học</a></h5>


                                                            </div>
                                                            <a href="/detailcourse?CourseID=LTCS">
                                                                <div class="khoahoc shrt-cart-btn">
                                                                    <i class="khoahoc"></i>
                                                                    <span>Xem chi tiết</span>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="khoahoc owl-item active">
                                    <div class="khoahoc item">
                                        <div class="khoahoc fcrse_1">
                                            <a href="/detailcourse?CourseID=LTWeb"
                                               class="khoahoc fcrse_img">
                                                <img src="/Content/images/kh8.jpg" alt="" />
                                            </a>
                                            <div class="khoahoc fcrse_content">
                                                <h3 class="khoahoc bl-title">
                                                    <a href="/detailcourse?CourseID=LTWeb">Lập tr&#236;nh Web</a>
                                                </h3>
                                                <div>
                                                    <p style="margin-left: 19px">GV: Jeff Bezos</p>
                                                    <div>
                                                        <div class="khoahoc auth1lnkprce">
                                                            <div class="khoahoc education_block_author">
                                                                <h5><a href="/Study?courseID=LTWeb&&videoID=53" tabindex="-1">Vào Học</a></h5>


                                                            </div>
                                                            <a href="/detailcourse?CourseID=LTWeb">
                                                                <div class="khoahoc shrt-cart-btn">
                                                                    <i class="khoahoc"></i>
                                                                    <span>Xem chi tiết</span>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="khoahoc owl-item active">
                                    <div class="khoahoc item">
                                        <div class="khoahoc fcrse_1">
                                            <a href="/detailcourse?CourseID=LTJ02"
                                               class="khoahoc fcrse_img">
                                                <img src="/Content/images/kh5.jpg" alt="" />
                                            </a>
                                            <div class="khoahoc fcrse_content">
                                                <h3 class="khoahoc bl-title">
                                                    <a href="/detailcourse?CourseID=LTJ02">Lập tr&#236;nh web với Java</a>
                                                </h3>
                                                <div>
                                                    <p style="margin-left: 19px">GV: Elon Musk</p>
                                                    <div>
                                                        <div class="khoahoc auth1lnkprce">
                                                            <div class="khoahoc education_block_author">
                                                                <h5><a href="/Study?courseID=LTJ02&&videoID=35" tabindex="-1">Vào Học</a></h5>


                                                            </div>
                                                            <a href="/detailcourse?CourseID=LTJ02">
                                                                <div class="khoahoc shrt-cart-btn">
                                                                    <i class="khoahoc"></i>
                                                                    <span>Xem chi tiết</span>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>        
                </div>
            </div>
            <footer>
            <jsp:include page="Footer.jsp"></jsp:include>
        </footer>
    </body>
</html>
