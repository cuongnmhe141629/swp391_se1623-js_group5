<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Hoc</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Sarala&display=swap" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
        <!-- MDB -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.css" rel="stylesheet" />
        <!-- MDB -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.js"></script>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet">
        <link href="/css/Study.css" rel="stylesheet" />
        <link href="/css/Registration.css" rel="stylesheet" />
    </head>
    <body>
        <header>
            <jsp:include page="Header.jsp"></jsp:include>
            </header>
            <div id="main">
                <div id="wapper-study">
                    <section class="hoconline ctn-sn">
                        <div class="hoconline sn sn--left" style="position: relative">
                            <div>
                                <a class="hoconline btn-back" href="/CourseDetail?course_id=LTC01">Quay lại</a>
                                <p>Bài học: Giới thiệu kh&#243;a học</p>
                            </div>
                            <div class="hmvideo">
                                <iframe src="https://www.youtube.com/embed/ILr_rU-lISk" height="600" width="1150"
                                        title="Iframe Example"></iframe>
                            </div>
                            <div class="hoconline ctl-lesson">
                                <div class="hoconline mb mode-auto">
                                    <label class="hoconline switch">
                                        <input type="checkbox" checked />
                                        <span class="hoconline slider round"></span>
                                    </label>
                                    <span>tự động chuyển</span>
                                </div>
                                <div>
                                    <a href="/Study?courseID=LTC01&&videoID=1">Bài trước</a>
                                    <a href="/Study?courseID=LTC01&&videoID=2">Bài tiếp</a>
                                    <div style="clear: both"></div>
                                </div>
                            </div>
                            <div class="hoconline mode-auto" style="position: absolute; top: 7px; right: 7px; padding: 0">
                                <label class="hoconline switch">
                                    <input type="checkbox" checked />
                                    <span class="hoconline slider round"></span>
                                </label>
                                <span>Tự động chuyển bài</span>
                            </div>
                        </div>
                        <div class="hoconline sn sn--right">
                            <div id="ContentCourse">
                                <div id="cover-title-content2">
                                    <div class="title-Content-CourseView"> nội dung khoá học </div>
                                </div>
                                <div class="accordion">
                                    <div class="contentBx">
                                        <div class="accordion-item-header Leaned">
                                            Chương 1: Giới thiệu về ng&#244;n ngữ lập tr&#236;nh C
                                        </div>
                                        <div class="accordion-item-body">
                                            <div class="accordion-item-body-content current-video">
                                                <div class="content-item"> Bài 1. Giới thiệu kh&#243;a học</div>
                                                <div>
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/Study?courseID=LTC01&&videoID=1">Học</a>
                                                </div>
                                            </div>
                                            <div class="accordion-item-body-content">
                                                <div class="content-item"> Bài 2. Biến v&#224; hằng trong C</div>
                                                <div>
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/Study?courseID=LTC01&&videoID=2">Học</a>
                                                </div>
                                            </div>
                                            <div class="accordion-item-body-content">
                                                <div class="content-item"> Bài 3. C&#225;c kiểu dữ liệu trong C</div>
                                                <div>
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/Study?courseID=LTC01&&videoID=3">Học</a>
                                                </div>
                                            </div>
                                            <div class="accordion-item-body-content">
                                                <div class="content-item"> Bài 4. Hiện tượng tr&#224;n số trong C</div>
                                                <div>
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/Study?courseID=LTC01&&videoID=4">Học</a>
                                                </div>
                                            </div>
                                            <div class="accordion-item-body-content">
                                                <div class="content-item"> Bài 5. Nhập xuất trong C</div>
                                                <div>
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/Study?courseID=LTC01&&videoID=5">Học</a>
                                                </div>
                                            </div>
                                            <div class="accordion-item-body-content">
                                                <div class="content-item"> Bài 6. To&#225;n tử trong C</div>
                                                <div>
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/Study?courseID=LTC01&&videoID=6">Học</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item-header">
                                            Chương 2: Cấu tr&#250;c điều khiển &amp; rẽ nh&#225;nh
                                        </div>
                                        <div class="accordion-item-body">
                                            <div class="accordion-item-body-content">
                                                <div class="content-item"> Bài 7. Cấu tr&#250;c điều khiển if else</div>
                                                <div>
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/Study?courseID=LTC01&&videoID=7">Học</a>
                                                </div>
                                            </div>
                                            <div class="accordion-item-body-content">
                                                <div class="content-item"> Bài 8. V&#242;ng lặp for trong C</div>
                                                <div>
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/Study?courseID=LTC01&&videoID=8">Học</a>
                                                </div>
                                            </div>
                                            <div class="accordion-item-body-content">
                                                <div class="content-item"> Bài 9. V&#242;ng lặp while v&#224; do … while</div>
                                                <div>
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/Study?courseID=LTC01&&videoID=9">Học</a>
                                                </div>
                                            </div>
                                            <div class="accordion-item-body-content">
                                                <div class="content-item"> Bài 10. Sự linh hoạt trong sử dụng v&#242;ng lặp</div>
                                                <div>
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/Study?courseID=LTC01&&videoID=10">Học</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item-header">
                                            Chương 3: H&#224;m trong C
                                        </div>
                                        <div class="accordion-item-body">
                                            <div class="accordion-item-body-content">
                                                <div class="content-item"> Bài 11. Một số h&#224;m trong thư viện to&#225;n học math.h</div>
                                                <div>
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/Study?courseID=LTC01&&videoID=11">Học</a>
                                                </div>
                                            </div>
                                            <div class="accordion-item-body-content">
                                                <div class="content-item"> Bài 12. H&#224;m người d&#249;ng định nghĩa</div>
                                                <div>
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/Study?courseID=LTC01&&videoID=12">Học</a>
                                                </div>
                                            </div>
                                            <div class="accordion-item-body-content">
                                                <div class="content-item"> Bài 13. H&#224;m kh&#244;ng trả về gi&#225; trị</div>
                                                <div>
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/Study?courseID=LTC01&&videoID=13">Học</a>
                                                </div>
                                            </div>
                                            <div class="accordion-item-body-content">
                                                <div class="content-item"> Bài 14. H&#224;m đệ quy</div>
                                                <div>
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/Study?courseID=LTC01&&videoID=14">Học</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item-header">
                                            Chương 4: Mảng trong C
                                        </div>
                                        <div class="accordion-item-body">
                                            <div class="accordion-item-body-content">
                                                <div class="content-item"> Bài 15. Nhập xuất mảng 1 chiều trong C</div>
                                                <div>
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/Study?courseID=LTC01&&videoID=15">Học</a>
                                                </div>
                                            </div>
                                            <div class="accordion-item-body-content">
                                                <div class="content-item"> Bài 16. Th&#234;m, x&#243;a phần tử trong mảng 1 chiều</div>
                                                <div>
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/Study?courseID=LTC01&&videoID=16">Học</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item-header">
                                            Chương 5: Chuỗi trong C
                                        </div>
                                        <div class="accordion-item-body">
                                            <div class="accordion-item-body-content">
                                                <div class="content-item"> Bài 17. Nhập xuất chuỗi trong C</div>
                                                <div>
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/Study?courseID=LTC01&&videoID=17">Học</a>
                                                </div>
                                            </div>
                                            <div class="accordion-item-body-content">
                                                <div class="content-item"> Bài 18. C&#225;c h&#224;m trong thư viện string.h</div>
                                                <div>
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/Study?courseID=LTC01&&videoID=18">Học</a>
                                                </div>
                                            </div>
                                            <div class="accordion-item-body-content">
                                                <div class="content-item"> Bài 19. Đảo ngược chuỗi trong C</div>
                                                <div>
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/Study?courseID=LTC01&&videoID=19">Học</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item-header">
                                            Chương 6: Con trỏ trong C
                                        </div>
                                        <div class="accordion-item-body">
                                            <div class="accordion-item-body-content">
                                                <div class="content-item"> Bài 20. Con trỏ trong C</div>
                                                <div>
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/Study?courseID=LTC01&&videoID=20">Học</a>
                                                </div>
                                            </div>
                                            <div class="accordion-item-body-content">
                                                <div class="content-item"> Bài 21. Mối li&#234;n hệ giữa con trỏ v&#224; mảng trong C</div>
                                                <div>
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/Study?courseID=LTC01&&videoID=21">Học</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <script>
                        const accordionItemHeaders = document.querySelectorAll(".accordion-item-header");
                        accordionItemHeaders.forEach(accordionItemHeader => {
                            accordionItemHeader.addEventListener("click", event => {
                                accordionItemHeader.classList.toggle("active");
                                const accordionItemBody = accordionItemHeader.nextElementSibling;
                                if (accordionItemHeader.classList.contains("active")) {
                                    accordionItemBody.style.maxHeight = accordionItemBody.scrollHeight + "px";
                                } else {
                                    accordionItemBody.style.maxHeight = 0;
                                }
                            });
                        });

                        accordionItemHeaders.forEach(accordionItemHeader => {
                            const accordionItemBody = accordionItemHeader.nextElementSibling;
                            if (accordionItemHeader.classList.contains("Leaned")) {
                                accordionItemBody.style.maxHeight = accordionItemBody.scrollHeight + "px";
                            } else {
                                accordionItemBody.style.maxHeight = 0;
                            }
                        });
                    </script>
                </div>
            </div>
            <footer>
            <jsp:include page="Footer.jsp"></jsp:include>
        </footer>
    </body>
</html>

