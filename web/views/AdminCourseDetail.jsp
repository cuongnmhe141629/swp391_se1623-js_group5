<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Chi tiết khóa học</title>
        <!-- all the meta tags -->
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="shortcut icon" href="/images/favicon.png">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    </head>
    <body>
        <%@include file="AdminMenu_top.jsp" %>

        <!-- PAGE CONTAINER-->
        <div class="content-page">
            <div class="content">
                <!-- BEGIN PlACE PAGE CONTENT HERE -->

                <div class="row ">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6"><h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> Chi tiết khóa học</h4></div>
                                    <div class="col-md-6"><a href="/AdminCourse" class="alignToTitle btn btn-outline-secondary btn-rounded btn-sm my-1"> <i class=" mdi mdi-keyboard-backspace"></i> Trở lại danh sách khóa học </a></div>                               
                                </div> <!-- end card body-->
                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div>

                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="/AdminCourse" method="get" id="registration ">
                                    <input hidden="true" type="id" name="id" value="${id}"/>
                                    <nav>
                                        <div class="nav nav-pills nav-fill" id="nav-tab" role="tablist">
                                            <a class="nav-link active" href="/AdminCourseDetail?id=${c.id}">Tổng quan</a>
                                            <a class="nav-link "  href="/AdminSubjectDimension?id=${c.id}">Subject Dimension</a>
                                            <a class="nav-link "  href="/AdminPricePackage?id=${c.id}">Các gói giá</a>
                                        </div>
                                    </nav>
                                    <div class="tab-content py-4 ">
                                        <div class="tab-pane fade show active" id="step1">
                                            <!-- PAGE CONTAINER-->
                                            <div class="content-page">
                                                <div class="content">
                                                    <!-- BEGIN PlACE PAGE CONTENT HERE -->
                                                    <div class="row">
                                                        <div class="col-xl-12">
                                                            <div class="card">
                                                                <div class="card-body">

                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <h4 class="header-title my-1">Cập nhật thông tin khóa học</h4>
                                                                        </div>
                                                                    </div>.

                                                                    <div class="row">
                                                                        <div class="col-xl-12">
                                                                            <form class="required-form" action="/AdminUpdateCourse" method="post">
                                                                                <div id="basicwizard">

                                                                                    <ul class="nav nav-pills nav-justified form-wizard-header">
                                                                                        <li class="nav-item">
                                                                                            <a href="#basic" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                                                                                <i class="mdi mdi-fountain-pen-tip"></i>
                                                                                                <span class="d-none d-sm-inline">Course ${c.id} </span>
                                                                                            </a>
                                                                                        </li>



                                                                                        <li class="w-100 bg-white pb-3">
                                                                                            <!--ajax page loader-->
                                                                                            <div class="ajax_loader w-100">
                                                                                                <div class="ajax_loaderBar"></div>
                                                                                            </div>
                                                                                            <!--end ajax page loader-->
                                                                                        </li>
                                                                                    </ul>

                                                                                    <div class="tab-content b-0 mb-0">
                                                                                        <div class="tab-pane" id="basic">
                                                                                            <div class="row justify-content-center">
                                                                                                <div class="col-xl-8">
                                                                                                    <input type="hidden" name = "course_type" value="general">
                                                                                                    <div class="form-group row mb-3">
                                                                                                        <label class="col-md-3 col-form-label" for="course_id">Mã khóa học <span class="required">*</span> </label>
                                                                                                        <div class="col-md-9">
                                                                                                            <input type="text" class="form-control" id="course_id" name = "id" value="${c.id}" disabled readonly>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group row mb-3">
                                                                                                        <label class="col-md-3 col-form-label" for="course_title">Tiêu đề khóa học </label>
                                                                                                        <div class="col-md-9">
                                                                                                            <input type="text" class="form-control" id="course_title" name = "title"  value="${c.title}" required>

                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="form-group row mb-3">
                                                                                                        <label class="col-md-3 col-form-label" for="short_description">Miêu tả ngắn</label>
                                                                                                        <div class="col-md-9">
                                                                                                            <textarea name="short_description" id = "short_description" class="form-control" >${c.short_description}</textarea>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="form-group row mb-3">
                                                                                                        <label class="col-md-3 col-form-label" for="description">Miêu tả</label>
                                                                                                        <div class="col-md-9">
                                                                                                            <textarea name="description" id = "description" class="form-control">${c.descripton}</textarea>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="form-group row mb-3">

                                                                                                        <label class="col-md-3 col-form-label" for="category_id">Thể loại <span class="required">*</span> </label>
                                                                                                        <div class="col-md-9">
                                                                                                            <input type="text" class="form-control" id="course_price" name = "category_id" 
                                                                                                                   value="${c.category_id==listCategory.id}" required>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="form-group row mb-3">

                                                                                                        <label class="col-md-3 col-form-label" for="is_free_course">Khóa học miễn phí <span class="required">*</span> </label>
                                                                                                        <div class="col-md-9">
                                                                                                            <input type="text" class="form-control" id="course_price" name = "price" value="${c.is_free_course}" required>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group row mb-3">
                                                                                                        <div class="offset-md-2 col-md-10">
                                                                                                            <div class="custom-control custom-checkbox">
                                                                                                                <button type="submit" id="submit" class="btn btn-info offset-5">Lưu</button>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div> <!-- end col -->
                                                                                        </div> <!-- end row -->
                                                                                    </div> <!-- end tab pane -->
                                                                                </div> <!-- tab-content -->
                                                                        </div> <!-- end-->
                                                                        <div style="color: red;">${success}</div>
                                                                        </form>
                                                                    </div>
                                                                </div><!-- end row-->
                                                            </div> <!-- end card-body-->
                                                        </div> <!-- end card-->
                                                    </div>
                                                </div>

                                                <script type="text/javascript">
                                                    $(document).ready(function () {
                                                        initSummerNote(['#description']);
                                                    });
                                                </script>

                                                <script type="text/javascript">
                                                    var blank_outcome = jQuery('#blank_outcome_field').html();
                                                    var blank_requirement = jQuery('#blank_requirement_field').html();
                                                    jQuery(document).ready(function () {
                                                        jQuery('#blank_outcome_field').hide();
                                                        jQuery('#blank_requirement_field').hide();
                                                    });
                                                    function appendOutcome() {
                                                        jQuery('#outcomes_area').append(blank_outcome);
                                                    }
                                                    function removeOutcome(outcomeElem) {
                                                        jQuery(outcomeElem).parent().parent().remove();
                                                    }

                                                    function appendRequirement() {
                                                        jQuery('#requirement_area').append(blank_requirement);
                                                    }
                                                    function removeRequirement(requirementElem) {
                                                        jQuery(requirementElem).parent().parent().remove();
                                                    }

                                                    function priceChecked(elem) {
                                                        if (jQuery('#discountCheckbox').is(':checked')) {

                                                            jQuery('#discountCheckbox').prop("checked", false);
                                                        } else {

                                                            jQuery('#discountCheckbox').prop("checked", true);
                                                        }
                                                    }
                                                </script>

                                                <style media="screen">
                                                    body {
                                                        overflow-x: hidden;
                                                    }
                                                </style>
                                                <!-- END PLACE PAGE CONTENT HERE -->
                                            </div>
                                        </div>
                                    </div>

                            </div>
                            <div class="row justify-content-between">
                                <div class="col-auto"><button type="submit" class="btn btn-primary" data-enchanter="Submit">Lưu</button></div>
                                <!--                                                <div class="col-auto">
                                                                                    <button type="button" class="btn btn-primary" data-enchanter="next">Next</button>
                                                                                    <button type="submit" class="btn btn-primary" data-enchanter="finish">Finish</button>
                                                                                </div>-->
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <!--JavaScript and dependencies--> 
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"></script>
    <!--JavaScript for validations only--> 
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script> 
    <script src="/js/enchanter.js"></script>
    <script>
        var registrationForm = $('#registration');
        var formValidate = registrationForm.validate({
            errorClass: 'is-invalid',
            errorPlacement: () => false
        });

        const wizard = new Enchanter('registration', {}, {
            onNext: () => {
                if (!registrationForm.valid()) {
                    formValidate.focusInvalid();
                    return false;
                }
            }
        });
    </script>
    <%@include file="AdminMenu_floor.jsp" %>
</body>
</html>