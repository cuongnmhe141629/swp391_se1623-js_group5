<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Lesson</title>
        <!-- all the meta tags -->
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="shortcut icon" href="/images/favicon.png">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    </head>
    <body>
        <%@include file="AdminMenu_top.jsp" %>
        <!-- PAGE CONTAINER-->
        <div class="content-page">
            <div class="content">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive-sm mt-4">
                                    <h4 class="mb-3 header-title">LESSON OF COURSE</h4>
                                    <a href="AdminAddLesson?id=${id}" class="btn btn-outline-primary">Thêm mới Lesson</a>
                                    <table style="text-align: center" id="basic-datatable" class="table table-striped table-centered mb-0">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>title</th>
                                                <th>status</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${lessons}" var="les">
                                                <tr>
                                                    <td>${les.id}</td>
                                                    <td>${les.title}</td>
                                                    <td>${les.status}</td>
                                                    <td>
                                                        <div class="dropright dropright">
                                                            <button type="button" class="btn btn-info"><a style="color: black" href="/AdminLessonDetail?l_id=${les.id}&id=${les.course_id}"> <i class="fas fa-edit "></i>Detail</a></button> 
                                                        </div>                                                  
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
        <!-- JavaScript for validations only -->
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script> 
        <script src="/js/enchanter.js"></script>
        <script>
            var registrationForm = $('#registration');
            var formValidate = registrationForm.validate({
                errorClass: 'is-invalid',
                errorPlacement: () => false
            });

            const wizard = new Enchanter('registration', {}, {
                onNext: () => {
                    if (!registrationForm.valid()) {
                        formValidate.focusInvalid();
                        return false;
                    }
                }
            });
        </script>
        <%@include file="AdminMenu_floor.jsp" %>
    </body>
</html>
