<!DOCTYPE html>  


<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>
  <title>lesson</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <style>
  .fakeimg {
    height: 200px;
    background: #aaa;
  }
  </style>
  
</head>
<body>
      <header>
          
            <jsp:include page="Header.jsp"></jsp:include>
            </header>
          
 
<div class="container">
 
<!-- Phần header website -->
<div class="jumbotron text-center" style="margin-bottom:0">
  <h1>leson</h1>
  <p>Phóng to/thu nhỏ trang web để xem sự thay đổi bố cục</p> 
</div>
<!-- Kết thúc phần header website -->
 
<!-- Phần menu chính -->
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <a class="navbar-brand" href="#">leson</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="#">Mục 1</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Mục 2</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Mục 3</a>
      </li>    
    </ul>
  </div>  
</nav>
<!-- Kết thúc phần menu chính -->
 
<!-- Phần nội dung chính -->
<div class="row">
 
    <!-- Cột trái -->
    <div class="col-sm-3">
      
   
     
    
   
      <ul class="nav nav-pills flex-column">
        <li class="nav-item">
          <a class="nav-link active" href="#">Phần1</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Phần2</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Phần3</a>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled" href="#">kiểm tra</a>
        </li>
      </ul>
      <hr class="d-sm-none">
    </div>
    <!-- Kết thúc cột trái -->
     
    <!-- Cột giữa -->
    <div class="col-sm-6">
      <h2>TIÊU ĐỀ 1</h2>
      <h5>Mô tả tiêu đề, Ngày 01 tháng 01, 2020</h5>
      <div class="fakeimg"></div>
      <p>Một số văn bản...</p>
      <p>Một số văn bản hiển thị ở website <a href="https://www.dammio.com">DAMMIO.COM</a>. Nội dung website là lập trình, thiết kế Web và học ngoại ngữ.</p>
      <br>
      <h2>TIÊU ĐỀ 2</h2>
      <h5>Mô tả tiêu đề, Ngày 01 tháng 01, 2020</h5>
      <div class="fakeimg"></div>
      <p>Một số văn bản...</p>
      <p>Một số văn bản hiển thị ở website <a href="https://www.dammio.com">DAMMIO.COM</a>. Nội dung website là lập trình, thiết kế Web và học ngoại ngữ.</p>
    </div>
    <!-- Kết thúc cột giữa -->
     
    <!-- Cột phải-->
   
    <!-- Kết thúc cột phải -->
     
  </div>
</div>
<!-- Kết thúc phần nội dung chính -->
 
<!-- Phần Footer -->
<div class="jumbotron text-center" style="margin-bottom:0">
  <p></p>
</div>
<!-- Kết thúc phần Footer -->
 


</body>
</html>
