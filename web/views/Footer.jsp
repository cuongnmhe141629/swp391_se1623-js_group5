<%-- 
    Document   : footer
    Created on : Aug 14, 2022, 2:38:46 PM
    Author     : quang
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    </head>
    <body>
        <footer class="footer">
            <section class="section section--8th" style="background-color: #60894d">
                <div class="footer-body">
                    <div class="row1">
                        <div class="col col-2">
                            <div class="cty">
                                <h1>NHÓM 5 SWP391</h1>
                                <p><img src="../images/logo-dark.png" alt="" />
                                </p>
                                <p>
                                    <i class=""></i>
                                    <span>Đại học FPT</span>
                                    <br />
                                    <i class=""></i>
                                    <span>Hotline: 0123.56.789</span>
                                </p>
                                <p><i class=""></i>Email: group5@edu.vn</p>

                            </div>
                        </div>
                        <div class="col col-2">
                            <h1 data-language="43">KHOÁ HỌC</h1>
                            <p>
                                <a style="color: white" href="#">Khoá học 1</a>
                            </p>
                            <p>
                                <a style="color: white" href="#">Khoá học 2</a>
                            </p>
                            <p>
                                <a style="color: white" href="#">Khoá học 3</a>
                            </p>
                            <p>
                                <a style="color: white" href="#">Khoá học 4</a>
                            </p>
                            <p>
                                <a style="color: white" href="#">Khoá học 5</a>
                            </p>
                            <p>
                                <a style="color: white" href="#">Khoá học 6</a>
                            </p>
                            <p>
                                <a style="color: white" href="#">Khoá học 7</a>
                            </p>
                        </div>
                        <div class="col col-3">
                            <h1 data-language="40">TIN TỨC</h1>
                            <a data-language="41" href="#">Xem lịch khai giảng</a>
                            <p>Khoá học 1</p>
                            <p>Khoá học 2</p>
                            <p>Khoá học 3</p>
                            <p>Khoá học 4</p>

                        </div>

                    </div>
                    <div class="div1" style="border-bottom: none">
                        <div id="div1__collaspe" class="div1__collaspe" style="text-align: center;
                             padding-top: 15px;
                             margin-bottom: -20px;">
                            <a class="div1__a">© 2022 SWP391</a>
                        </div>
                    </div>

                </div>
            </section>
        </footer>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>
        <script src="/js/index.js" defer></script>
    </body>
</html>
