
</div>
</div>
<!-- all the js files -->
<!-- bundle -->
<script src="/js/backend/app.min.js"></script>

<!-- third party js -->
<script src="/js/vendor/Chart.bundle.min.js"></script>
<script src="/js/vendor/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/js/vendor/jquery-jvectormap-world-mill-en.js"></script>
<script src="/js/vendor/jquery.dataTables.min.js"></script>
<script src="/js/vendor/dataTables.bootstrap4.js"></script>
<script src="/js/vendor/dataTables.responsive.min.js"></script>
<script src="/js/vendor/responsive.bootstrap4.min.js"></script>
<script src="/js/vendor/dataTables.buttons.min.js"></script>
<script src="/js/vendor/buttons.bootstrap4.min.js"></script>
<script src="/js/vendor/buttons.html5.min.js"></script>
<script src="/js/vendor/buttons.flash.min.js"></script>
<script src="/js/vendor/buttons.print.min.js"></script>
<script src="/js/vendor/dataTables.keyTable.min.js"></script>
<script src="/js/vendor/dataTables.select.min.js"></script>
<script src="/js/vendor/summernote-bs4.min.js"></script>
<script src="/js/vendor/fullcalendar.min.js"></script>
<script src="/js/page/demo.summernote.js"></script>
<script src="/js/vendor/dropzone.js"></script>
<script src="/js/page/datatable-initializer.js"></script>
<script src="/js/backend/font-awesome-icon-picker/fontawesome-iconpicker.min.js" charset="utf-8"></script>
<script src="/js/vendor/bootstrap-tagsinput.min.js" charset="utf-8"></script>
<script src="/js/backend/bootstrap-tagsinput.min.js"></script>
<script src="/js/vendor/dropzone.min.js" charset="utf-8"></script>
<script src="/js/ui/component.fileupload.js" charset="utf-8"></script>
<script src="/js/page/demo.form-wizard.js"></script>
<!-- dragula js-->
<script src="/js/vendor/dragula.min.js"></script>
<!-- component js -->
<script src="/js/backend/component.dragula.js"></script>

<script src="/js/backend/custom.js"></script>

<!-- Dashboard chart's data is coming from this file -->




<script type="text/javascript">
    $(document).ready(function () {
        $(function () {
            $('.icon-picker').iconpicker();
        });
    });
</script>

<!-- Toastr and alert notifications scripts -->
<script type="text/javascript">
    function notify(message) {
        $.NotificationApp.send("Heads up!", message, "top-right", "rgba(0,0,0,0.2)", "info");
    }

    function success_notify(message) {
        $.NotificationApp.send("Congratulations!", message, "top-right", "rgba(0,0,0,0.2)", "success");
    }

    function error_notify(message) {
        $.NotificationApp.send("Oh snap!", message, "top-right", "rgba(0,0,0,0.2)", "error");
    }

    function error_required_field() {
        $.NotificationApp.send("Oh snap!", "Please fill all the required fields", "top-right", "rgba(0,0,0,0.2)", "error");
    }
</script>



<script type="text/javascript">
    function showAjaxModal(url, header)
    {
        // SHOWING AJAX PRELOADER IMAGE
        jQuery('#scrollable-modal .modal-body').html('<div style="width: 100px; height: 100px; line-height: 100px; padding: 0px; text-align: center; margin-left: auto; margin-right: auto;"><div class="spinner-border text-secondary" role="status"></div></div>');
        jQuery('#scrollable-modal .modal-title').html('loading...');
        // LOADING THE AJAX MODAL
        jQuery('#scrollable-modal').modal('show', {backdrop: 'true'});

        // SHOW AJAX RESPONSE ON REQUEST SUCCESS
        $.ajax({
            url: url,
            success: function (response)
            {
                jQuery('#scrollable-modal .modal-body').html(response);
                jQuery('#scrollable-modal .modal-title').html(header);
            }
        });
    }
    function showLargeModal(url, header)
    {
        // SHOWING AJAX PRELOADER IMAGE
        jQuery('#large-modal .modal-body').html('<div style="width: 100px; height: 100px; line-height: 100px; padding: 0px; text-align: center; margin-left: auto; margin-right: auto;"><div class="spinner-border text-secondary" role="status"></div></div>');
        jQuery('#large-modal .modal-title').html('...');
        // LOADING THE AJAX MODAL
        jQuery('#large-modal').modal('show', {backdrop: 'true'});

        // SHOW AJAX RESPONSE ON REQUEST SUCCESS
        $.ajax({
            url: url,
            success: function (response)
            {
                jQuery('#large-modal .modal-body').html(response);
                jQuery('#large-modal .modal-title').html(header);
            }
        });
    }
</script>

<!-- (Large Modal)-->
<div class="modal fade" id="large-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">Large modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            </div>
            <div class="modal-body">
                ...
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<!-- Scrollable modal -->
<div class="modal fade" id="scrollable-modal" tabindex="-1" role="dialog" aria-labelledby="scrollableModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="scrollableModalTitle">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body ml-2 mr-2">

            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script type="text/javascript">
    function confirm_modal(delete_url)
    {
        jQuery('#alert-modal').modal('show', {backdrop: 'static'});
        document.getElementById('update_link').setAttribute('href', delete_url);
    }
</script>

<!-- Info Alert Modal -->
<div id="alert-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body p-4">
                <div class="text-center">
                    <i class="dripicons-information h1 text-info"></i>
                    <h4 class="mt-2">Heads up!</h4>
                    <p class="mt-3">Are you sure?</p>
                    <button type="button" class="btn btn-info my-2" data-dismiss="modal">Cancel</button>
                    <a href="#" id="update_link" class="btn btn-danger my-2">Continue</a>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">

    function ajax_confirm_modal(delete_url, elem_id)
    {
        jQuery('#ajax-alert-modal').modal('show', {backdrop: 'static'});
        $("#appent_link_a").bind("click", function () {
            delete_by_ajax_calling(delete_url, elem_id);
        });
    }

    function delete_by_ajax_calling(delete_url, elem_id) {
        $.ajax({
            url: delete_url,
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success') {
                    $('#' + elem_id).fadeOut(600);
                    $.NotificationApp.send("Success!", response.message, "top-right", "rgba(0,0,0,0.2)", "success");
                } else {
                    $.NotificationApp.send("Oh snap!", response.message, "top-right", "rgba(0,0,0,0.2)", "error");
                }
            }
        });
    }
</script>

<!-- Info Alert Modal -->
<div id="ajax-alert-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body p-4">
                <div class="text-center" id="appent_link">
                    <i class="dripicons-information h1 text-info"></i>
                    <h4 class="mt-2">Heads up!</h4>
                    <p class="mt-3">Are you sure?</p>
                    <button type="button" class="btn btn-info my-2" data-dismiss="modal">Cancel</button>
                    <a id="appent_link_a" href="javascript:;" class="btn btn-danger my-2" data-dismiss="modal">Continue</a>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->  
<!-- Our script! :) -->

</body>
</html>