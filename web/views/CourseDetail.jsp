<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.*"%>
<%@page import="dao.*"%>
<%@page import="model.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Chi tiết khóa học</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" >
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" ></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Sarala&display=swap" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
        <!-- MDB -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.css" rel="stylesheet" />
        <!-- MDB -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.js"></script>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet">
        <link href="/css/Registration.css" rel="stylesheet" />
        <style>
            div.stars {
                width: 270px;
                display: inline-block;
            }

            input.star {
                display: none;
            }

            label.star {
                float: right;
                padding: 10px;
                font-size: 18px;
                color: #444;
                transition: all .2s;
            }

            input.star:checked ~ label.star:before {
                content: '\f005';
                color: #FD4;
                transition: all .25s;
            }

            input.star-5:checked ~ label.star:before {
                color: #FE7;
                text-shadow: 0 0 20px #952;
            }

            input.star-1:checked ~ label.star:before {
                color: #F62;
            }

            label.star:hover {
                transform: rotate(-15deg) scale(1.3);
            }

            label.star:before {
                content: '\f006';
                font-family: FontAwesome;
            }

            #wapper-start {
                margin: 0 auto;
                width: 66%;
                margin-bottom: 50px;
                padding-bottom: 20px;
                background-color: #fff;
                border-radius: 10px 10px 10px 10px;
                border: 1px solid #efefef;
                transition: all 0.4s ease;
                box-shadow: 0 1px 5px 0 rgb(0 0 0 / 25%);
            }

            #total-rating {
                width: 100%;
                margin: 0px auto;
                text-align: center;
            }

            #total-rating p {
                font-size: 18px;
                font-weight: 700;
            }
        </style>
    </head>
    <body>
        <header>
            <jsp:include page="Header.jsp"></jsp:include>
            </header>
            <div id="main">
                <div id="InformationCourse">
                    <div class="LeftInformationCourse">
                        <div class="NameOfCourse">${course.title}</div>

                    <div class="InforCourse">
                        <div>
                            ${course.short_description}
                        </div>    
                    </div>
                    <div class="IntroTeacher" style="width: 100%; height: 30%; display: flex; margin-bottom: 10px; margin-top: 10px;">
                        <div style=" width: 20%; height: 100%;"><img src="${course.user_photo}" style="width: 120px; height: 120px; border-radius: 50%; padding: auto;"></div>
                        <div style=" width: 80%; height: 100%; padding-top: 20px; padding-left: 10px;  overflow: auto; color: white;">
                            <P style=" font-size: 20px;"><strong>GV: ${course.owner}</strong></P>
                            <p style=" font-size: 15px;">
                                ${course.user_description}
                        </div>
                    </div>
                    <c:if test="${course.is_free_course == 0}">
                        <c:if test="${checkURC == null}">
                            <div class="Button">
                                <a href="/Order?course_id=1" class="buttonClickBuy">MUA NGAY CHỈ VỚI ${course.price} VNĐ</a>
                            </div>
                        </c:if>
                    </c:if>
                    <c:if test="${course.is_free_course == 1}">
                    </c:if>
                </div>

                <div class="RightInformationCourse">
                    <iframe src="https://www.youtube.com/embed/CFD9EFcNZTQ"></iframe>
                    <!-- <img src="https://www.g24i.org/wp-content/uploads/2020/03/kissanime.jpg"> -->
                </div>
            </div>
            <div class="title-Content-CourseView">
                đánh giá khóa học
            </div>
            <hr>
            <div id="wapper-start">
                <form action="/CourseDetail/Vote" method="post"><div class="stars" style="width: 60%;">
                        <input class="star star-5" id="star-5" type="radio" name="star5" disabled />
                        <label class="star star-5" for="star-5"></label>
                        <input class="star star-4" id="star-4" type="radio" name="star4" checked />
                        <label class="star star-4" for="star-4"></label>
                        <input class="star star-3" id="star-3" type="radio" name="star3" disabled />
                        <label class="star star-3" for="star-3"></label>
                        <input class="star star-2" id="star-2" type="radio" name="star2" disabled />
                        <label class="star star-2" for="star-2"></label>
                        <input class="star star-1" id="star-1" type="radio" name="star1" disabled />
                        <label class="star star-1" for="star-1"></label>
                    </div> 
                </form>
                <div id="total-rating">
                    <p>Điểm đánh giá: 4</p>
                    <p>Số lượt đánh giá: 6</p>
                </div>
            </div>
            <hr>
            <div id="IntroduceCourse">
                <div class="title-Content-CourseView">
                    giới thiệu khoá học
                </div>
                <hr>
                <div id="cover-title-content">
                    <div class="content">
                        <p>
                            👉 ${course.descripton}
                        </p>
                    </div>
                </div>

                <div id="cover-title-content2">
                    <div class="title-Content-CourseView"> nội dung khoá học </div>
                </div>
                <hr>

                <div id="ContentCourse">
                    <div class="accordion">
                        <div class="contentBx">
                            <c:forEach varStatus="index" items="${listTopic}" var="t">
                            <div class="accordion-item-header Leaned">
                                Chương ${index.count}: ${t.title}
                            </div>    
                            <div class="accordion-item-body">
                                <c:forEach varStatus="index1" items="${listLesson}" var="l">
                                    <c:if test="${l.topic_id == t.topic_id}">
                                        <div class="accordion-item-body-content">
                                            <div class="content-item"> Bài ${index1.count}. ${l.title}</div>
                                            <c:if test="${course.is_free_course == 1}">       
                                                <a style=" margin-left: 80px;" class="btn btn-success" href="/CourseLesson?course_id=${course.id}&lesson_id=${l.id}">Học</a> 
                                            </c:if>
                                            <c:if test="${course.is_free_course == 0}">
                                                <c:if test="${checkURC != null && checkURC.getUid() != 0}">       
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/CourseLesson?course_id=${course.id}&lesson_id=${l.id}">Học</a>
                                                </c:if>
                                                <c:if test="${(checkURC == null || checkURC.getUid() == 0) && index1.count<=2}">
                                                    <a style=" margin-left: 80px;" class="btn btn-success" href="/CourseLesson?course_id=${course.id}&lesson_id=${l.id}">Học thử </a> 
                                                </c:if>
                                            </c:if>
                                        </div>   
                                    </c:if>
                                </c:forEach>
                            </div>
                            </c:forEach>          
                        </div>
                    </div>
                </div>

                <!-- JS - Accordtion effect -->
                <script>
                    const accordionItemHeaders = document.querySelectorAll(".accordion-item-header");
                    accordionItemHeaders.forEach(accordionItemHeader => {
                        accordionItemHeader.addEventListener("click", event => {
                            accordionItemHeader.classList.toggle("active");
                            const accordionItemBody = accordionItemHeader.nextElementSibling;
                            if (accordionItemHeader.classList.contains("active")) {
                                accordionItemBody.style.maxHeight = accordionItemBody.scrollHeight + "px";
                            } else {
                                accordionItemBody.style.maxHeight = 0;
                            }

                        });
                    });

                    accordionItemHeaders.forEach(accordionItemHeader => {
                        const accordionItemBody = accordionItemHeader.nextElementSibling;
                        if (accordionItemHeader.classList.contains("Leaned")) {
                            accordionItemBody.style.maxHeight = accordionItemBody.scrollHeight + "px";
                        } else {
                            accordionItemBody.style.maxHeight = 0;
                        }
                    });
                </script>
            </div>
            <footer>
                <jsp:include page="Footer.jsp"></jsp:include>
            </footer>

    </body>
</html>
