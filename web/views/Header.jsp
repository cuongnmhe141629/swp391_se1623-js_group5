<%-- 
    Document   : header
    Created on : Aug 14, 2022, 2:38:57 PM
    Author     : quang
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <meta name="csrf-token" content="pWKYsTnljZgm1p0UVE49TcaqrKHop1jCQv3bw7bS" />
        <link rel="shortcut icon" href="/images/favicon.png" type="image/x-icon">
        <link rel="icon" href="/images/favicon.png" type="image/x-icon">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
              crossorigin="anonymous" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css" />
        <link href="/css/unicons/css/unicons.css" type="text/css" rel="stylesheet" />
        <link href="/css/semantic/semantic.min.css" type="text/css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
        
        <!-- Difference CSS  -->
        <link rel="stylesheet" type="text/css" href="../css/demo.css" />
        <link rel="stylesheet" type="text/css" href="../css/home.css" />
        <link href="/slideshow/assets/owl.theme.default.min.css" rel="stylesheet" />
        <link href="/content/slideshow/assets/owl.carousel.min.css" rel="stylesheet" />
        <link rel='stylesheet prefetch' href='https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css'>
        
        <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
        <script src="/slideshow/jquery.min.js"></script>
        <script src="/slideshow/owl.carousel.min.js"></script>
    </head>
    <body>
        <nav id="nav">
            <div class="nav-top" style="background-color: #60894d">
                <div>
                    <div class="contact">
                        <span><i style="color: #f7c24b"></i>0123.456.789</span>
                        <span style="margin: 0 5px 0 10px; color: rgb(255, 255, 255)">|</span>
                        <span>
                            <i style="color: #f7c24b"></i>
                            hoconline@gmail.com</span>
                    </div>

                    <c:if test="${sessionScope.acc ne null}">
                        <c:if test="${sessionScope.user.role_id != 5 && sessionScope.user.role_id != 6}">
                            <div class="dn-dx">
                                <a class="dn-dx__admin" href="/AdminDashboard">Trang quản trị</a>&nbsp;
                            </div>
                            <span>|</span>&nbsp;&nbsp;
                        </c:if>
                        <c:if test="${sessionScope.user.role_id == 5 && sessionScope.user.role_id == 6}">

                        </c:if>
                        <c:if test="${user.image == null || user.image.isEmpty()}">
                            <a href="<%=request.getContextPath()%>/UserProfile"><img class="u-avatar" id="s_avar" src="../images/user-profile.jpg"/><a>&nbsp;&nbsp;
                                </c:if>
                                <c:if test="${user.image != null && !user.image.isEmpty()}">
                                    <a href="<%=request.getContextPath()%>/UserProfile"><img class="u-avatar" id="s_avar" src="${user.image}"/><a>&nbsp;&nbsp;
                                        </c:if>
                                        <div class="dn-dx">
                                            <a class="dn-dx__login" href="<%=request.getContextPath()%>/UserProfile">${user.fullname}</a>
                                            <span>|</span>
                                            <a class="dn-dx__signup" href="/LogOutServlet">Đăng xuất</a>
                                        </div>
                                    </c:if>
                                    <c:if test="${sessionScope.acc eq null}">
                                        <div class="dn-dx">
                                            <a class="dn-dx__login" href="/Login" >Đăng nhập</a>
                                            <span>|</span>
                                            <a class="dn-dx__signup" href="/Register">Đăng ký</a>
                                        </div>
                                    </c:if>
                                    </div>
                                    </div>
                                    <ul id="nav-ul" class="wb ul">
                                        <li class="btn-cll">
                                            <a id="nav-collapse" class="mb nav-collapse" href="javascript:void(0);"><span>☰</span></a>
                                        </li>
                                        <li class="logo">
                                            <a href="/Home"><img src="/images/logo-dark.png" alt="" /></a>
                                        </li>
                                        <form class="col-md-3" action="/SearchCourse" method="get">
                                            <li class="search">
                                                <div>
                                                    <!-- <i class="uil uil-search" id="search_icon"></i> -->
                                                    <input id="search_value" class="search__input" name="search_course" type="text" placeholder="Bạn đang cần tìm khóa học..." style="width: 100%" />
                                                </div>
                                            </li>
                                        </form>

                                        <li class="links">
                                            <!--                                            <a class="homepage" href="/views/Home.jsp">Trang chủ</a>-->
                                            <a class="training" href="/CourseList?caId=1">Các khóa học</a>
                                            <a class="blogs" href="/BlogController">Blogs</a>
                                            <a class="ct" href="#">Liên hệ</a>
                                        </li>
                                    </ul>
                                    <!-- MOBILE -->
                                    <ul class="mb ul">
                                        <h1>MENU</h1>
                                        <li class="course"><a href="/CourseList">KHÓA HỌC</a></li>
                                        <li class="homepage"><a href="#">TIN TỨC</a></li>
                                        <li class="training"><a href="#">ĐÀO TẠO</a></li>
                                        <li class="blogs"><a href="#">BLOGS</a></li>
                                        <li class="ct"><a href="#">LIÊN HỆ</a></li>
                                        <div>
                                            <a href="javascript:void(0);">ĐĂNG NHẬP</a>
                                            <div>Tôi chưa có tài khoản?</div>
                                        </div>
                                    </ul>
                                    <!-- MOBILE END -->
                                    </nav>
                                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
                                    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>
                                    <script src="../js/script.js" defer></script>

                                    </body>
                                    </html>
