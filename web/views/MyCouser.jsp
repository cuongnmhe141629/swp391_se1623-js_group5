<%-- 
    Document   : MyCouser
    Created on : Nov 1, 2022, 10:10:01 PM
    Author     : ASUS
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Khóa học đã mua</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
              crossorigin="anonymous" />
        <link rel="stylesheet" type="text/css"
              href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css" />
        <link rel="stylesheet" type="text/css"
              href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css" />
        <link href="/css/unicons/css/unicons.css" type="text/css"
              rel="stylesheet" />
        <link href="/css/semantic/semantic.min.css" type="text/css"
              rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
        <!-- Difference CSS  -->
        <link rel="stylesheet" type="text/css" href="/css/home.css" />
        <link rel="stylesheet" type="text/css" href="/css/demo.css" />
        <meta name="csrf-token" content="tTXT53Dve8x0Vxi1Y81JSlQBsolCQyhVFDWCJqph" />            
    </head>

    <body class="tcnvcd">
        <header>    
            <jsp:include page="Header.jsp"></jsp:include>
            </header>
          
            <div class="tcnvcd breadcrumb-ctn">

            </div>
            <section class="tcnvcd ctn-sn ctn-sn-1">
                <div class="tcnvcd nav nav-tabs" id="nav-tab" role="tablist">
                    <a style="text-align: center" class="tcnvcd nav-link" id="nav-hscn-tab" data-bs-target="#hscn" role="tab"
                       href="/UserProfile">
                        HỒ SƠ CÁ NHÂN
                    </a>
                    <a style="text-align: center" class="tcnvcd nav-link active" id="nav-khdm-tab" data-bs-target="hscn" role="tab"
                       href="/mycourse">
                        KHÓA HỌC CỦA TÔI
                    </a>
                    <a style="text-align: center" class="tcnvcd nav-link" id="nav-khdm-tab" data-bs-target="#khdm" role="tab"
                       href="#">
                        KHÓA HỌC YÊU THÍCH
                    </a>
                </div>
                <div class="tcnvcd tab-pane fade show active" id="hscn" role="tabpanel" aria-labelledby="nav-hscn-tab"> 
                    <form action="/mycourse" method="post" enctype="multipart/form-data">   
                        <input hidden="true" type="email" name="email" value="${sessionScope.email}"/>
                    <input type="hidden" name="_token" value="tTXT53Dve8x0Vxi1Y81JSlQBsolCQyhVFDWCJqph">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="mb-3 header-title">Danh sách khóa học</h4>

                                    <div class="table-responsive-sm mt-4">                                       
                                        <table style="text-align: center;" id="basic-datatable" class="table table-striped table-centered mb-0">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Tên khóa học</th>
                                                    <th>Thể loại</th>   
                                                 
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${listCourseOfUser}" var="u">
                                                    <tr>
                                                        <td>${u.id}</td>
                                                        <td>${u.title}</td>

                                                        <td>${u.category_name }</td>
                                                       
                                                        <td>
                                                            <div class="dropright dropright">
                                                                <a class="btn btn-outline-primary btn-rounded" href="/CourseLesson?course_id=${u.id}"> 
                                                                    Khóa Học
                                                                    <i class="bi bi-book" ></i>
                                                                </a>
                                                                    
<!--                                                                    viết servlet cho lesson như thế này là bấm vào sẽ ra lesson-->
                                                                <button type="button" class="btn btn-outline-primary btn-rounded"  onclick="doDelete"  >
                                                                    Xóa khóa học
                                                                    <i class="bi bi-trash" onclick="doDelete"  href="/DeleteMycouse=${u.id}"  ></i>
                                                                  
                                                                </button>

                                                               <script type="text/javascript">
                                                                      function doDelete(course_id){
//                                                                            $ajax().del(mycourse,
//                                                                                 {course_id:u.id });       
                                                                                  $.ajax({
                                                                              url: '/mycourse?course_id=${u.id} ',
                                                                                 type: 'DELETE',
                                                                               success: function(result) {
                                                                               // Do something with the result
                                                                                       }
                                                                                   });
                                                                   }
                                                               </script>


                                                                <ul class="dropdown-menu">
                                                                    <li><a class="btn btn-sm btn-outline-primary btn-rounded btn-icon" href="AdminCourseDetail?id=${o.id}">
                                                                            <i class="mdi mdi-dots-vertical">Khoa hoc</i>
                                                                        </a></li>
                                                                    <li>Cái này thay bằng button, viết function js rồi gán vào onCLick. trong function đó gọi ajax().del('/mycourse')</li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </form>
            </div>
        </section>
        <footer>
            <%@include file="AdminMenu_floor.jsp" %>
        </footer>                
    </body>

</html>
