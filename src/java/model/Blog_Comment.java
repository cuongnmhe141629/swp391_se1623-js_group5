/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author User
 */
public class Blog_Comment {
    private int id;
    private int blog_id;
    private int user_id;
    private String comment;
    private int status;
    private String name;

    public Blog_Comment() {
    }

    public Blog_Comment(int blog_id, int user_id, String comment, int status) {
        this.blog_id = blog_id;
        this.user_id = user_id;
        this.comment = comment;
        this.status = status;
    }
    
    public Blog_Comment(int id, int blog_id, int user_id, String comment, int status) {
        this.id = id;
        this.blog_id = blog_id;
        this.user_id = user_id;
        this.comment = comment;
        this.status = status;
    }

    public Blog_Comment(int id, int blog_id, int user_id, String comment, int status, String name) {
        this.id = id;
        this.blog_id = blog_id;
        this.user_id = user_id;
        this.comment = comment;
        this.status = status;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBlog_id() {
        return blog_id;
    }

    public void setBlog_id(int blog_id) {
        this.blog_id = blog_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
}
