/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author User
 */
public class Price_Package {

    private int id;
    private String name;
    private int access_duration;
    private boolean status;
    private float list_price;
    private float sale_price;
    private String description;
    private int course_id;

    public Price_Package() {
    }

    public Price_Package(int id, String name, int access_duration, boolean status, float list_price, float sale_price, String description, int course_id) {
        this.id = id;
        this.name = name;
        this.access_duration = access_duration;
        this.status = status;
        this.list_price = list_price;
        this.sale_price = sale_price;
        this.description = description;
        this.course_id = course_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAccess_duration() {
        return access_duration;
    }

    public void setAccess_duration(int access_duration) {
        this.access_duration = access_duration;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public float getList_price() {
        return list_price;
    }

    public void setList_price(float list_price) {
        this.list_price = list_price;
    }

    public float getSale_price() {
        return sale_price;
    }

    public void setSale_price(float sale_price) {
        this.sale_price = sale_price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCourse_id() {
        return course_id;
    }

    public void setCourse_id(int course_id) {
        this.course_id = course_id;
    }

}
