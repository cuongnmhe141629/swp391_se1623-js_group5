/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author ASUS
 */
public class Lession {

    private int id;
    private String title;
    private int course_ID;
    private int topic_ID;
    private String video_URL;
    private String lestion_type;
    private String attachment;
    private String sumary;
    private int status;

    public Lession(int id, String title, int course_ID, int topic_ID, String video_URL, String lestion_type, String attachment, String sumary, int status) {
        this.id = id;
        this.title = title;
        this.course_ID = course_ID;
        this.topic_ID = topic_ID;
        this.video_URL = video_URL;
        this.lestion_type = lestion_type;
        this.attachment = attachment;
        this.sumary = sumary;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCourse_ID() {
        return course_ID;
    }

    public void setCourse_ID(int course_ID) {
        this.course_ID = course_ID;
    }

    public int getTopic_ID() {
        return topic_ID;
    }

    public void setTopic_ID(int topic_ID) {
        this.topic_ID = topic_ID;
    }

    public String getVideo_URL() {
        return video_URL;
    }

    public void setVideo_URL(String video_URL) {
        this.video_URL = video_URL;
    }

    public String getLestion_type() {
        return lestion_type;
    }

    public void setLestion_type(String lestion_type) {
        this.lestion_type = lestion_type;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getSumary() {
        return sumary;
    }

    public void setSumary(String sumary) {
        this.sumary = sumary;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Lession{" + "id=" + id + ", title=" + title + ", course_ID=" + course_ID + ", topic_ID=" + topic_ID + ", video_URL=" + video_URL + ", lestion_type=" + lestion_type + ", attachment=" + attachment + ", sumary=" + sumary + ", status=" + status + '}';
    }

}
