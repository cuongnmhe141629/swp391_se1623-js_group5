/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;

/**
 *
 * @author ASUS
 */
public class Course {

    private int id;
    private String title;
    private String short_description;
    private String descripton;
    private int category_id;
    private String category_name;
    private String tag_line;
    private String thumbnail;
    private int is_free_course;
    private Date updated_date;
    private boolean status;
    private String featured_flag;
    private String owner;
    private User lecturer;
    private int lesson_count;
    private float price;
    private String user_description;
    private String user_photo;

    public Course() {
    }

    public Course(int id, String title, String short_description, String descripton, int category_id, String category_name, String tag_line, String thumbnail, int is_free_course, Date updated_date, boolean status, String featured_flag, User lecturer) {
        this.id = id;
        this.title = title;
        this.short_description = short_description;
        this.descripton = descripton;
        this.category_id = category_id;
        this.category_name = category_name;
        this.tag_line = tag_line;
        this.thumbnail = thumbnail;
        this.is_free_course = is_free_course;
        this.updated_date = updated_date;
        this.status = status;
        this.featured_flag = featured_flag;
        this.lecturer = lecturer;
       
    }

    public Course(int id, String title, String short_description, String descripton, int category_id, String tag_line, String thumbnail, int is_free_course, Date updated_date, boolean status, String featured_flag, String owner) {
        this.id = id;
        this.title = title;
        this.short_description = short_description;
        this.descripton = descripton;
        this.category_id = category_id;
        this.tag_line = tag_line;
        this.thumbnail = thumbnail;
        this.is_free_course = is_free_course;
        this.updated_date = updated_date;
        this.status = status;
        this.featured_flag = featured_flag;
        this.owner = owner;
    }



    public Course(int id, String title, String short_description, String descripton, int category_id, String tag_line, String thumbnail, int is_free_course, Date updated_date, boolean status, String featured_flag, String owner, int lesson_count) {
        this.id = id;
        this.title = title;
        this.short_description = short_description;
        this.descripton = descripton;
        this.category_id = category_id;
        this.tag_line = tag_line;
        this.thumbnail = thumbnail;
        this.is_free_course = is_free_course;
        this.updated_date = updated_date;
        this.status = status;
        this.featured_flag = featured_flag;
        this.owner = owner;
         this.lesson_count = lesson_count;
    }

    public Course(int id, String title, String short_description, String descripton, int category_id, String tag_line, String thumbnail, int is_free_course, Date updated_date, boolean status, String featured_flag, String owner, float price) {
        this.id = id;
        this.title = title;
        this.short_description = short_description;
        this.descripton = descripton;
        this.category_id = category_id;
        this.tag_line = tag_line;
        this.thumbnail = thumbnail;
        this.is_free_course = is_free_course;
        this.updated_date = updated_date;
        this.status = status;
        this.featured_flag = featured_flag;
        this.owner = owner;
        this.price = price;
    }

    public Course(int id, String title, String short_description, String descripton, int category_id, String tag_line, String thumbnail, int is_free_course, Date updated_date, boolean status, String featured_flag, String owner, int lesson_count, float price) {
        this.id = id;
        this.title = title;
        this.short_description = short_description;
        this.descripton = descripton;
        this.category_id = category_id;
        this.tag_line = tag_line;
        this.thumbnail = thumbnail;
        this.is_free_course = is_free_course;
        this.updated_date = updated_date;
        this.status = status;
        this.featured_flag = featured_flag;
        this.owner = owner;
        this.lesson_count = lesson_count;
        this.price = price;
    }

    public Course(int id, String title, String short_description, String descripton, int category_id, String tag_line, String thumbnail, int is_free_course, Date updated_date, boolean status, String featured_flag, String owner, float price, String user_description, String user_photo) {
        this.id = id;
        this.title = title;
        this.short_description = short_description;
        this.descripton = descripton;
        this.category_id = category_id;
        this.tag_line = tag_line;
        this.thumbnail = thumbnail;
        this.is_free_course = is_free_course;
        this.updated_date = updated_date;
        this.status = status;
        this.featured_flag = featured_flag;
        this.owner = owner;
        this.price = price;
        this.user_description = user_description;
        this.user_photo = user_photo;
    }

    public String getUser_photo() {
        return user_photo;
    }

    public void setUser_photo(String user_photo) {
        this.user_photo = user_photo;
    }

    public String getUser_description() {
        return user_description;
    }

    public void setUser_description(String user_description) {
        this.user_description = user_description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getLesson_count() {
        return lesson_count;
    }

    public void setLesson_count(int lesson_count) {
        this.lesson_count = lesson_count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getLecturer() {
        return lecturer;
    }

    public void setLecturer(User lecturer) {
        this.lecturer = lecturer;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    public String getDescripton() {
        return descripton;
    }

    public void setDescripton(String descripton) {
        this.descripton = descripton;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getTag_line() {
        return tag_line;
    }

    public void setTag_line(String tag_line) {
        this.tag_line = tag_line;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getIs_free_course() {
        return is_free_course;
    }

    public void setIs_free_course(int is_free_course) {
        this.is_free_course = is_free_course;
    }

    public Date getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(Date updated_date) {
        this.updated_date = updated_date;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getFeatured_flag() {
        return featured_flag;
    }

    public void setFeatured_flag(String featured_flag) {
        this.featured_flag = featured_flag;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    @Override
    public String toString() {
        return "Course{" + "id=" + id + ", title=" + title + ", short_description=" + short_description + ", descripton=" + descripton + ", category_id=" + category_id + ", tag_line=" + tag_line + ", thumbnail=" + thumbnail + ", is_free_course=" + is_free_course + ", updated_date=" + updated_date + ", status=" + status + ", featured_flag=" + featured_flag + ", owner=" + owner + '}';
    }

}
