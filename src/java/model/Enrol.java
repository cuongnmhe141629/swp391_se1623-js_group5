/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;

/**
 *
 * @author ASUS
 */
public class Enrol {

    private int id;
    private int uid;
    private int cid;
    private Date date_addeDate;
    private int status;

    public Enrol(int id, int uid, int cid, Date date_addeDate, int status) {
        this.id = id;
        this.uid = uid;
        this.cid = cid;
        this.date_addeDate = date_addeDate;
        this.status = status;
    }

    public Enrol(int id, int uid, int cid) {
        this.id = id;
        this.uid = uid;
        this.cid = cid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public Date getDate_addeDate() {
        return date_addeDate;
    }

    public void setDate_addeDate(Date date_addeDate) {
        this.date_addeDate = date_addeDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
