/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author User
 */
public class Lesson {
    private int id;
    private String title;
    private int course_id;
    private int topic_id;
    private String video_url;
    private String lesson_type;
    private String attachment;
    private String summary;
    private boolean status;
    private String topic_title;

    public Lesson() {
    }

    public Lesson(String title, int course_id, int topic_id, String video_url, String lesson_type, String summary, boolean status) {
        this.title = title;
        this.course_id = course_id;
        this.topic_id = topic_id;
        this.video_url = video_url;
        this.lesson_type = lesson_type;
        this.summary = summary;
        this.status = status;
    }
    
    public Lesson(String title, int topic_id, String video_url, String lesson_type, String summary) {
        this.title = title;
        this.topic_id = topic_id;
        this.video_url = video_url;
        this.lesson_type = lesson_type;
        this.summary = summary;
    }

    public Lesson(int id, String title, int course_id, int topic_id, String lesson_type, String summary) {
        this.id = id;
        this.title = title;
        this.course_id = course_id;
        this.topic_id = topic_id;
        this.lesson_type = lesson_type;
        this.summary = summary;
    }

    public Lesson(int id, String title, int course_id, int topic_id, String video_url, String lesson_type, String attachment, String summary, boolean status) {
        this.id = id;
        this.title = title;
        this.course_id = course_id;
        this.topic_id = topic_id;
        this.video_url = video_url;
        this.lesson_type = lesson_type;
        this.attachment = attachment;
        this.summary = summary;
        this.status = status;
    }

    public Lesson(int id, String title, int course_id, int topic_id, String video_url, String lesson_type, String attachment, String summary, boolean status, String topic_title) {
        this.id = id;
        this.title = title;
        this.course_id = course_id;
        this.topic_id = topic_id;
        this.video_url = video_url;
        this.lesson_type = lesson_type;
        this.attachment = attachment;
        this.summary = summary;
        this.status = status;
        this.topic_title = topic_title;
    }

    public String getTopic_title() {
        return topic_title;
    }

    public void setTopic_title(String topic_title) {
        this.topic_title = topic_title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCourse_id() {
        return course_id;
    }

    public void setCourse_id(int course_id) {
        this.course_id = course_id;
    }

    public int getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(int topic_id) {
        this.topic_id = topic_id;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getLesson_type() {
        return lesson_type;
    }

    public void setLesson_type(String lesson_type) {
        this.lesson_type = lesson_type;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
    
}
