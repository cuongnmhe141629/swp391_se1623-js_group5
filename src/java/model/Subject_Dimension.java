/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Admin
 */
public class Subject_Dimension {

    private int dimension_id;
    private String type;
    private String name;
    private String description;
    private int course_id;

    public Subject_Dimension() {
    }

    public Subject_Dimension(int dimension_id, String type, String name, String description, int course_id) {
        this.dimension_id = dimension_id;
        this.type = type;
        this.name = name;
        this.description = description;
        this.course_id = course_id;
    }

    public Subject_Dimension(String type, String name, String description, int course_id) {
        this.type = type;
        this.name = name;
        this.description = description;
        this.course_id = course_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDimension_id() {
        return dimension_id;
    }

    public void setDimension_id(int dimension_id) {
        this.dimension_id = dimension_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCourse_id() {
        return course_id;
    }

    public void setCourse_id(int course_id) {
        this.course_id = course_id;
    }

}
