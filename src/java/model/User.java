/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Admin
 */
public class User {

    private int user_id;
    private String fullname;
    private boolean gender;
    private String email;
    private String phone;
    private String password;
    private int role_id;
    private String image;
    private int status;
    private boolean feature;
    private String myHash;
    private int active;
    private String address;
    private String description;

    public User() {
    }


    public User(String email) {
        this.email = email;
    }

    public User(int user_id, String fullname) {
        this.user_id = user_id;
        this.fullname = fullname;
    }

    public User(int user_id, int role_id, int status) {
        this.user_id = user_id;
        this.role_id = role_id;
        this.status = status;
    }

    public User(int user_id, String fullname, boolean gender, String email, String phone, String password, int role_id, String image, int status, String address) {
        this.user_id = user_id;
        this.fullname = fullname;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.role_id = role_id;
        this.image = image;
        this.status = status;
        this.address = address;
    }

    public User(String fullname, boolean gender, String email, String phone, String password, int role_id, int status, String address) {
        this.fullname = fullname;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.role_id = role_id;
        this.status = status;
        this.address = address;
    }

    public User(int user_id, String fullname, boolean gender, String email, String phone, String password, int role_id, String image, int status, boolean feature, String myHash, int active) {
        this.user_id = user_id;
        this.fullname = fullname;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.role_id = role_id;
        this.image = image;
        this.status = status;
        this.feature = feature;
        this.myHash = myHash;
        this.active = active;
    }

    public User(int user_id, String fullname, boolean gender, String email, String phone, String password, int role_id, String image, int status) {
        this.user_id = user_id;
        this.fullname = fullname;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.role_id = role_id;
        this.image = image;
        this.status = status;
    }

    public User(String fullname, boolean gender, String email, String phone, String password, int role_id, String image, int status, boolean feature, String myHash, int active) {
        this.fullname = fullname;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.role_id = role_id;
        this.image = image;
        this.status = status;
        this.feature = feature;
        this.myHash = myHash;
        this.active = active;
    }

    public User(String fullname, boolean gender, String email, String phone, String password, int role_id, String image, int status) {
        this.fullname = fullname;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.role_id = role_id;
        this.image = image;
        this.status = status;
    }

    public User(String email, String new_password) {
        this.email = email;
        this.password = new_password;
    }

    public User(int user_id, String fullname, boolean gender, String email, String phone, String password, String image) {
        this.user_id = user_id;
        this.fullname = fullname;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.image = image;
    }

    public User(String fullname, boolean gender, String email, String phone, String password, int role_id, int status) {
        this.fullname = fullname;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.role_id = role_id;
        this.status = status;
    }

    public boolean isFeature() {
        return feature;
    }

    public void setFeature(boolean feature) {
        this.feature = feature;
    }

    public String getMyHash() {
        return myHash;
    }

    public void setMyHash(String myHash) {
        this.myHash = myHash;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    @Override
    public String toString() {
        return "User{" + "user_id=" + user_id + ", fullname=" + fullname + ", gender=" + gender + ", email=" + email + ", phone=" + phone + ", password=" + password + ", role_id=" + role_id + ", image=" + image + ", status=" + status + ", feature=" + feature + ", myHash=" + myHash + ", active=" + active + '}';
    }

}
