/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.impl.UserDAOImpl;
import dao.UserDAO;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import model.User;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author Admin
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 100 // 100 MB
)
public class UserProfileServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String email = (String) session.getAttribute("email").toString();
        try {
            UserDAO uD = new UserDAOImpl();
            User user = uD.getUserByEmail(email);
            session.setAttribute("user", user);
            request.getRequestDispatcher("/views/UserProfile.jsp").forward(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
            request.getRequestDispatcher("/views/UserProfile.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");

        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            HttpSession session = request.getSession();
//            String filename = null;
            response.setContentType("text/html;charset=UTF-8");
            request.setCharacterEncoding("UTF-8");

            String email = request.getParameter("email").trim();
            String fullname = request.getParameter("fullname").trim();

            boolean gender = false;
            String check = request.getParameter("gender");
            int statis = Integer.parseInt(check);
            if (statis == 0) {
                gender = true;
            } else if (statis == 1) {
                gender = false;
            }

            String phone = request.getParameter("phone").trim();

            Part filePart = request.getPart("photo");
            String photo = "";
            String path = request.getContextPath() + "E:\\Hoctap\\SWP391_Sangnv\\SWP391_OnlineLearnning_G5\\swp391_se1623-js_group5\\web\\images\\avatar";

            File file = new File(path);
            file.mkdir();
            String fileName = getFileName(filePart);
            OutputStream out = null;
            InputStream filecontent = null;
            PrintWriter writer = response.getWriter();
            try {
                out = new FileOutputStream(new File(path + File.separator
                        + fileName));

                filecontent = filePart.getInputStream();

                int read = 0;
                final byte[] bytes = new byte[1024];

                while ((read = filecontent.read(bytes)) != -1) {
                    out.write(bytes, 0, read);

                    photo = "../images/avatar" + "/" + fileName;

                }
            } catch (IOException e) {
            }
//cc..
//            Part filePart = request.getPart("photo");
//            String fileName = filePart.getSubmittedFileName();
//            for (Part part : request.getParts()) {
//                part.write("E:\\Hoctap\\SWP391_Sangnv\\SWP391_OnlineLearnning_G5\\swp391_se1623-js_group5\\web\\images\\avatar\\" + fileName);
//            }
//            response.getWriter().print("The file uploaded sucessfully.");

            User u = new User();
            u.setFullname(fullname);
            u.setGender(gender);
            u.setEmail(email);
            u.setPhone(phone);
            u.setImage(photo);
//            u.setImage("../images/avatar/" + fileName);

            UserDAO uD = new UserDAOImpl();
            uD.updateUserProfile(u, email);

            User user = uD.getUserByEmail(email);

            request.setAttribute("lsUser", uD.getListUser());
            session.setAttribute("user", user);
            request.setAttribute("success", "<p>Chỉnh sửa thông tin cá nhân thành công!</p>");
//            response.sendRedirect("/UserProfile?user_id=" + user.getUser_id());
            request.getRequestDispatcher("/views/UserProfile.jsp").forward(request, response);

        } catch (IOException | NumberFormatException | SQLException | ServletException ex) {
            request.setAttribute("error", "Yes");
            request.getRequestDispatcher("/views/UserProfile.jsp").forward(request, response);
            Logger.getLogger(UserProfileServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
