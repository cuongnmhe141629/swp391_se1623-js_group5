/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.RoleDAO;
import dao.UserDAO;
import dao.impl.RoleDAOImpl;
import dao.impl.UserDAOImpl;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Role;
import model.User;

/**
 *
 * @author Admin
 */
public class AdminUpdateUserServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        int user_id = Integer.parseInt(request.getParameter("user_id").trim());
        try {
            UserDAO userDAO = new UserDAOImpl();
            User user = userDAO.getUserByID(user_id);

            RoleDAO role = new RoleDAOImpl();
            List<Role> listRole = role.getAllRole();

            request.setAttribute("user", user);
            request.setAttribute("listRole", listRole);
            request.getRequestDispatcher("/views/AdminUpdateUser.jsp").forward(request, response);
        } catch (SQLException e) {
            log(e.getMessage());
            request.getRequestDispatcher("/views/AdminUpdateUser.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        int user_id = Integer.parseInt(request.getParameter("user_id").trim());
        int role_id = Integer.parseInt(request.getParameter("role").trim());
        int status = Integer.parseInt(request.getParameter("status").trim());

        try {
            RoleDAO role = new RoleDAOImpl();
            UserDAO userDAO = new UserDAOImpl();
            List<Role> listRole = role.getAllRole();

            HttpSession session = request.getSession();
            User u = new User(user_id, role_id, status);

            userDAO.editUser(u);

            User user = userDAO.getUserByID(user_id);

            request.setAttribute("listRole", listRole);
            request.setAttribute("user", user);
            request.setAttribute("success", "Yes");
//            response.sendRedirect("/AdminUpdateUser?user_id=" + user_id);
            request.getRequestDispatcher("/views/AdminUpdateUser.jsp").forward(request, response);
        } catch (SQLException e) {
            log(e.getMessage());
            request.getRequestDispatcher("/views/AdminUpdateUser.jsp").forward(request, response);
            request.getRequestDispatcher("/views/AdminUpdateUser.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
