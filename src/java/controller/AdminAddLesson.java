/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.impl.LessonDAOImpl;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author Admin
 */
@MultipartConfig
public class AdminAddLesson extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int course_id = Integer.parseInt(request.getParameter("id"));
        request.setAttribute("id", course_id);
        request.getRequestDispatcher("/views/AdminAddLesson.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try{
            String title = request.getParameter("title");
            String lesson_type = request.getParameter("lesson_type");
            int Topic_id = Integer.parseInt(request.getParameter("topic_id"));
            String summary = request.getParameter("summary");
            Part part = request.getPart("video");
            String realPath = request.getServletContext().getRealPath("/videos");
            String filename = Paths.get(part.getSubmittedFileName()).getFileName().toString();
            if (!Files.exists(Paths.get(realPath))) {
                Files.createDirectories(Paths.get(realPath));
            }
//            part.write("C:\\Users\\User\\Desktop\\swp391_se1623-js_group5-core\\web\\videos\\" + filename);
//            part.write(request.getContextPath()+"\\videos\\" + filename);
            int course_id = Integer.parseInt(request.getParameter("id"));
            boolean status =Boolean.parseBoolean(request.getParameter("status"));
            LessonDAOImpl LDAO = new LessonDAOImpl();
            LDAO.insert(title, course_id, Topic_id, filename,lesson_type, summary, status);
//            response.sendRedirect("/AdminDashboard");
            response.sendRedirect("/AdminLessonServlet?id=" + course_id);
        } catch (Exception e) {
            request.getRequestDispatcher("/views/AdminDashboard.jsp").forward(request, response);        
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
