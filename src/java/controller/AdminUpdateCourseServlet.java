/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.impl.CourseDAOImpl;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ASUS
 */
public class AdminUpdateCourseServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AdminUpdateCourseServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AdminUpdateCourseServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            request.getRequestDispatcher("/views/AdminSubjectDetail.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String title = request.getParameter("title");
        String short_description = request.getParameter("short_description");
        String descripton = request.getParameter("descripton");
        String catecory_id =request.getParameter("catecory_id");
        Date updated_date =null;
        int status = 1;
        String img = "null";
        int tag_line =200;
        int is_free_course =1;
        String featured_flag =null;
        int owner = 5 ;
        
        CourseDAOImpl dao = new CourseDAOImpl();    
        boolean aa = dao.updateCourse(title, short_description, descripton, owner, title, img, is_free_course, updated_date, status, featured_flag, img, tag_line, tag_line);
        response.sendRedirect("AdminCourse");  
        
        
        
        /*
         pr.setString(1, title);
            pr.setString(2, short_description);
            pr.setString(3, descripton);
            pr.setInt(4, category_id);
            pr.setString(5, tag_line);   
            pr.setString(6, thumbnail);
            pr.setInt(7, is_free_course);
            pr.setDate(8, updated_date);
            pr.setInt(9, status);
            pr.setString(10, featured_flag);
            pr.setString(11, owner);
            pr.setInt(12, dimension_id);
            pr.setInt(13, pricePackage_id);
        */
    }
    

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
