/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.UserDAO;
import dao.impl.CourseDAOImpl;
import dao.impl.Course_CategoryDAOImpl;
import dao.impl.UserDAOImpl;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import model.Course_Category;
import model.User;

/**
 *
 * @author Admin
 */
@MultipartConfig
public class AdminAddCourseServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AdminAddCourseServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AdminAddCourseServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            Course_CategoryDAOImpl CDAO = new Course_CategoryDAOImpl();
            List<Course_Category> categoris = CDAO.getAllCourseCategories();
            request.setAttribute("categoris", categoris);
            UserDAO UDAO = new UserDAOImpl();
            ArrayList<User> users = UDAO.getAlluserid();
            request.setAttribute("users", users);
            request.getRequestDispatcher("/views/AdminAddCourse.jsp").forward(request, response);
        } catch (Exception e) {
        }
    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");

        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String title = request.getParameter("txtType");
            String description = request.getParameter("description");
            int category = Integer.parseInt(request.getParameter("txtcategory"));
            Part part = request.getPart("image");
            String realPath = request.getServletContext().getRealPath("/images");
            String filename = Paths.get(part.getSubmittedFileName()).getFileName().toString();
//    if (!Files.exists(Paths.get(realPath))) {
//            Files.createDirectories(Paths.get(realPath));
//        }    
//        Person acc = (Person)req.getSession().getAttribute("accSession");
//        if(acc.getType()==1){
//            part.write("C:\\Users\\User\\Desktop\\swp391_se1623-js_group5-core\\web\\images\\" + filename);
            boolean status = Boolean.parseBoolean(request.getParameter("status"));
            int onwer = Integer.parseInt(request.getParameter("txtuser"));
            CourseDAOImpl dao = new CourseDAOImpl();
            dao.insertCourse(title, description, category, filename, status, onwer);
            response.sendRedirect("/AdminDashboard");
//            response.sendRedirect("/AdminAddCourse");
        } catch (Exception e) {
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
