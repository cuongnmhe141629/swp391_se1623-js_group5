/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.impl.UserDAOImpl;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.User;
import service.EmailService;
import service.EmailServiceIml;

/**
 *
 * @author Admin
 */
public class ForgotPasswordServlet extends HttpServlet {

    private final EmailService emailService = new EmailServiceIml();
    User a = new User();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        String regex = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$";
        String email = request.getParameter("email");
        String myHash = "";
        Random random = new Random();
        UserDAOImpl db = new UserDAOImpl();
        a = db.getAccountByEmail(email);

        if (!email.matches(regex)) {
            request.setAttribute("mess", "Email chưa chính xác");
            request.getRequestDispatcher("views/ResetPassword.jsp").forward(request, response);
        } else if (a == null || a.getActive() != 1) {
            request.setAttribute("mess", "Email chưa được đăng ký");
            request.getRequestDispatcher("views/ResetPassword.jsp").forward(request, response);
        } else {
            if (a != null) {
                random.nextInt(9999999);
                myHash = "" + getMd5("" + random);
                a.setMyHash(myHash);
                db.updateHashCode(a);
                User user = new User();

                db.updateFeatured(a,"false");

                emailService.sendEmail(getServletContext(), a, "forgot", "http://localhost:8080/ResetPasswordServlet?key1=" + a.getEmail() + "&key2=" + a.getMyHash());
                request.setAttribute("mess", "Kiểm tra e-mail của bạn!");
                request.setAttribute("title", "Đặt lại mật khẩu thành công");
                request.getRequestDispatcher("views/Verify.jsp").forward(request, response);

            }
        }

    }

    public static String getMd5(String input) {
        try {

            // Static getInstance method is called with hashing MD5
            MessageDigest md = MessageDigest.getInstance("MD5");

            // digest() method is called to calculate message digest
            //  of an input digest() return array of byte
            byte[] messageDigest = md.digest(input.getBytes());

            // Convert byte array into signum representation
            BigInteger no = new BigInteger(1, messageDigest);

            // Convert message digest into hex value
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } // For specifying wrong message digest algorithms
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect("views/ResetPassword.jsp");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ForgotPasswordServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
