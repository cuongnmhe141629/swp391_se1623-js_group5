/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.SubjectDimensionDAO;
import dao.impl.SubjectDimensionDAOImpl;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Subject_Dimension;

/**
 *
 * @author Admin
 */
public class AdminAddSubjectDimensionSevlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int course_id = Integer.parseInt(request.getParameter("id"));
        request.setAttribute("id", course_id);
        request.getRequestDispatcher("/views/AdminAddSubjectDimension.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String type = request.getParameter("type").trim();
        String name = request.getParameter("name").trim();
        String description = request.getParameter("description").trim();
        int course_id = Integer.parseInt(request.getParameter("id"));

        try {
            HttpSession session = request.getSession();
            Subject_Dimension dimension = new Subject_Dimension(type, name, description, course_id);
            SubjectDimensionDAO dimensionDAO = new SubjectDimensionDAOImpl();

            dimensionDAO.addSubjectDimension(dimension);
            
            request.setAttribute("id", course_id);
            request.setAttribute("success", "Yes");
            request.getRequestDispatcher("/views/AdminAddSubjectDimension.jsp").forward(request, response);
//            response.sendRedirect("/AdminAddSubjectDimension?id=" + course_id);
//            response.getWriter().print("The file uploaded sucessfully.");
//            response.sendRedirect("/AdminSubjectDimension?id=" + course_id);
        } catch (SQLException e) {
            e.printStackTrace();
            request.getRequestDispatcher("/views/AdminAddSubjectDimension.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
