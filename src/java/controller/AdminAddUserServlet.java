/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.RoleDAO;
import dao.UserDAO;
import dao.impl.RoleDAOImpl;
import dao.impl.UserDAOImpl;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Role;
import model.User;
import service.EmailService;
import service.EmailServiceIml;

/**
 *
 * @author Admin
 */
public class AdminAddUserServlet extends HttpServlet {

    private final EmailService emailService = new EmailServiceIml();
    User u = new User();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            RoleDAO role = new RoleDAOImpl();
            List<Role> listRole = role.getAllRole();
            request.setAttribute("listRole", listRole);
            request.getRequestDispatcher("/views/AdminAddUser.jsp").forward(request, response);
        } catch (SQLException e) {
            log(e.getMessage());
            request.getRequestDispatcher("/views/AdminAddUser.jsp").forward(request, response);
        }
    }

    private static char[] generatePassword(int length) {
        String capitalCaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowerCaseLetters = "abcdefghijklmnopqrstuvwxyz";
        String specialCharacters = "!@#$";
        String numbers = "1234567890";
        String combinedChars = capitalCaseLetters + lowerCaseLetters + specialCharacters + numbers;
        Random random = new Random();
        char[] password = new char[length];

        password[0] = lowerCaseLetters.charAt(random.nextInt(lowerCaseLetters.length()));
        password[1] = capitalCaseLetters.charAt(random.nextInt(capitalCaseLetters.length()));
        password[2] = specialCharacters.charAt(random.nextInt(specialCharacters.length()));
        password[3] = numbers.charAt(random.nextInt(numbers.length()));

        for (int i = 4; i < length; i++) {
            password[i] = combinedChars.charAt(random.nextInt(combinedChars.length()));
        }
        return password;
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static boolean isExistingEmail(String email) throws SQLException {
        UserDAO userDAO = new UserDAOImpl();
        List<User> users = userDAO.getListEmail();
        // Iterates all the users
        for (User user : users) {
            // Checks if the user email is equal to the email parameter
            if (user.getEmail().equals(email)) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String fullname = request.getParameter("fullname").trim();
        boolean gender = false;
        String check = request.getParameter("gender");
        if ("male".equals(check)) {
            gender = true;
        } else if ("female".equals(check)) {
            gender = false;
        }

        String email = request.getParameter("email").trim();
        String phone = request.getParameter("phone").trim();
        String address = request.getParameter("address").trim();
        int role_id = Integer.parseInt(request.getParameter("role").trim());
        int status = Integer.parseInt(request.getParameter("status").trim());
        String password = String.valueOf(generatePassword(6));

        try {
            RoleDAO role = new RoleDAOImpl();
            List<Role> listRole = role.getAllRole();
            request.setAttribute("listRole", listRole);

            HttpSession session = request.getSession();
            if (isExistingEmail(email)) {
//                request.setAttribute("error", "<p>Email đã tồn tại. Vui lòng nhập lại!</p>");
                request.setAttribute("error", "Yes");
                request.getRequestDispatcher("/views/AdminAddUser.jsp").forward(request, response);
//                response.sendRedirect("/AdminAddUser");
            } else {
                UserDAO userDAO = new UserDAOImpl();
                User user = new User(fullname, gender, email, phone, password, role_id, status, address);
                try {
                    u = userDAO.getAccountByEmail(email);
                    emailService.sendEmail(getServletContext(), u, "newPass", password);
                } catch (SQLException e) {
                    log(e.getMessage());
                }
                userDAO.addUser(user);
                request.setAttribute("success", "Yes");
                request.getRequestDispatcher("/views/AdminAddUser.jsp").forward(request, response);
//                response.sendRedirect("/AdminUserList");
            }
        } catch (SQLException e) {
            log(e.getMessage());
            request.getRequestDispatcher("/views/AdminAddUser.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
