/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.LessonDAO;
import dao.impl.CourseDAOImpl;
import dao.impl.LessonDAOImpl;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import model.Lesson;

/**
 *
 * @author User
 */
@MultipartConfig
public class AdminLessonDetailServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        int l_id = Integer.parseInt(request.getParameter("l_id"));
        try {
            LessonDAO lessonDAO = new LessonDAOImpl();
            Lesson lesson = lessonDAO.getLessonById(l_id);
            request.setAttribute("les", lesson);
            request.setAttribute("id", lesson.getId());
            request.getRequestDispatcher("/views/AdminLessonDetail.jsp").forward(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            String title = request.getParameter("title");
            String lesson_type = request.getParameter("lesson_type");
            int Topic_id = Integer.parseInt(request.getParameter("Topic_id"));
            String summary = request.getParameter("summary");
            Part part = request.getPart("video");
            String realPath = request.getServletContext().getRealPath("/videos");
            String filename = Paths.get(part.getSubmittedFileName()).getFileName().toString();
            if (!Files.exists(Paths.get(realPath))) {
                Files.createDirectories(Paths.get(realPath));
            }    
//            part.write("C:\\Users\\User\\Desktop\\swp391_se1623-js_group5-core\\web\\images" + filename);
            int lesson_id = Integer.parseInt(request.getParameter("l_id"));
            int course_id = Integer.parseInt(request.getParameter("id"));
        try {
            LessonDAO lessonDAO = new LessonDAOImpl();
            lessonDAO.update(title, Topic_id, filename, lesson_type, summary, lesson_id, course_id);
            response.sendRedirect("/AdminLessonServlet?id=" + course_id);
        } catch (SQLException e) {
            e.printStackTrace();
            request.getRequestDispatcher("/views/Dashboard.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
