/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.CourseDAO;
import dao.Course_CategoryDAO;
import dao.impl.CourseDAOImpl;
import dao.impl.Course_CategoryDAOImpl;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Course;
import model.Course_Category;

/**
 *
 * @author Admin
 */
public class CourseListServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("search", false);
        int caId = Integer.parseInt(request.getParameter("caId").trim());
        try {
            Course_CategoryDAO category = new Course_CategoryDAOImpl();
            CourseDAO course = new CourseDAOImpl();
            List<Course_Category> listCategory = category.getAllCourseCategories();
            List<Course> courses = course.getListCourseByCategory(caId);

            int page, numperpage = 6;
            String page_raw = request.getParameter("page");
            if (page_raw == null) {
                page = 1;
            } else {
                page = Integer.parseInt(page_raw);
            }
            int start, end;
            start = (page - 1) * numperpage;
            if (page * numperpage > courses.size()) {
                end = courses.size();
            } else {
                end = page * numperpage;
            }
            List<Course> listCourse = course.getCourseByPage(courses, start, end);
            int size = courses.size();
            int pagenum = (size % 6 == 0) ? (size / 6) : (size / 6 + 1);

            request.setAttribute("num", pagenum);
            request.setAttribute("page", page);
            request.setAttribute("caId", caId);
            request.setAttribute("listCourse", listCourse);
            request.setAttribute("listCategory", listCategory);
            request.getRequestDispatcher("/views/CourseList.jsp").forward(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
            request.getRequestDispatcher("/views/CourseList.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
