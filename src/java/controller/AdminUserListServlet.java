/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.RoleDAO;
import dao.UserDAO;
import dao.impl.RoleDAOImpl;
import dao.impl.UserDAOImpl;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Role;
import model.User;

/**
 *
 * @author Admin
 */
public class AdminUserListServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            UserDAO user = new UserDAOImpl();
            List<User> listUser = user.getListUser();

            RoleDAO role = new RoleDAOImpl();
            List<Role> listRole = role.getAllRole();

            request.setAttribute("listUser", listUser);
            request.setAttribute("listRole", listRole);
            request.getRequestDispatcher("/views/AdminUserList.jsp").forward(request, response);
        } catch (IOException | SQLException | ServletException e) {
            log(e.getMessage());
            request.getRequestDispatcher("/views/AdminUserList.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String gender = request.getParameter("gender").trim();
        String role_id = request.getParameter("role").trim();
        String status = request.getParameter("status").trim();
        try {
            UserDAO user = new UserDAOImpl();
            List<User> listUser = user.searchUser(gender, role_id, status);

            RoleDAO role = new RoleDAOImpl();
            List<Role> listRole = role.getAllRole();

            request.setAttribute("listUser", listUser);
            request.setAttribute("listRole", listRole);
            request.getRequestDispatcher("/views/AdminUserList.jsp").forward(request, response);
        } catch (IOException | SQLException | ServletException e) {
            log(e.getMessage());
            request.getRequestDispatcher("/views/AdminUserList.jsp").forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
