/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.impl.BlogDAOImpl;
import dao.CommentDAO;
import dao.impl.CommentDAOImpl;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Blog;
import model.Blog_Comment;

/**
 *
 * @author User
 */
public class BlogDetailServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        int blog_id = Integer.parseInt(request.getParameter("blog_id"));
        Blog blog = new BlogDAOImpl().getBlogByID(blog_id);
        CommentDAO daoComment = new CommentDAOImpl();
        ArrayList<Blog_Comment> blog_comments = daoComment.getAllCommentByBlog(blog_id);
        request.setAttribute("blog", blog);
        request.setAttribute("blog_comments", blog_comments);
        request.getRequestDispatcher("/views/BlogDetail.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int blogid = Integer.parseInt(request.getParameter("txtblogid"));
        int userid = Integer.parseInt(request.getParameter("txtuserid"));
        String comment = request.getParameter("txtcomment");
        int status = Integer.parseInt(request.getParameter("txtstatus"));
        
        CommentDAOImpl dao = new CommentDAOImpl();
        dao.comment(blogid, userid, comment, status);
            response.sendRedirect("blogdetail?blog_id="+blogid);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
