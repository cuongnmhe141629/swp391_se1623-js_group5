/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.impl.UserDAOImpl;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.User;

/**
 *
 * @author PV
 */
public class ChangePasswordServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/views/ChangePassword.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {

            response.setContentType("text/html;charset=UTF-8");
            request.setCharacterEncoding("UTF-8");

            HttpSession session = request.getSession();
            String email = (String) session.getAttribute("email").toString();

            String cur_password = request.getParameter("cur_password").trim();
            String new_password = request.getParameter("new_password").trim();
            String re_password = request.getParameter("re_password").trim();

            UserDAOImpl dao = new UserDAOImpl();
            String password = dao.getPassword(email);
//        System.out.println(password);
            if (cur_password.equals(password)) {
                if (new_password.equals(re_password)) {
                    User user = new User(email, new_password);
                    dao.changePassword(user, email);
                    request.getRequestDispatcher("/views/Login.jsp").forward(request, response);
                } else {
//                response.sendRedirect(request.getContextPath() + "/ChangePassword");
                    request.setAttribute("mess", "Nhập lại mật khẩu không đúng!");
                    request.getRequestDispatcher(request.getContextPath() + "/views/ChangePassword.jsp").forward(request, response);
                }
            } else {
//            response.sendRedirect(request.getContextPath() + "/ChangePassword");
                request.setAttribute("mess1", "Bạn đã nhập sai mật khẩu!");
                request.getRequestDispatcher(request.getContextPath() + "/views/ChangePassword.jsp").forward(request, response);
            }
        } catch (SQLException e) {
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
