/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.CourseDAO;
import dao.impl.CourseDAOImpl;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Course;
import model.User;

/**
 *
 * @author nhiep
 */
public class MyCourseServlet extends HttpServlet {

    private CourseDAO courseDao = new CourseDAOImpl();
    private User userLoggedIn;

    private void setUserLoggedIn(HttpServletRequest request) {
        // kêt nối người dùng 
        User user = (User) request.getSession().getAttribute("user");
        System.out.println(user);
        //logiin
        userLoggedIn = user; // ket noi nguoi dung ra 
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         setUserLoggedIn(request);
        if (userLoggedIn == null) {
            // phai đăng nhập để xem thông tin 
            response.sendRedirect(request.getContextPath() + "/views/Login.jsp");
        } else {
            try {
                //đã đăng nhập 
                String path = "/views/MyCouser.jsp";
                // hiện list danh sách couser đã đăng kí của người dùng đã đăng nhập  
                ArrayList<Course> listCourseOfUserLoggedIn = courseDao.GetCoureOfUser(userLoggedIn);
                request.setAttribute("listCourseOfUser", listCourseOfUserLoggedIn);
                request.getRequestDispatcher(path).forward(request, response);
            } catch (SQLException ex) {
                //login người dùng phù hợp 
                Logger.getLogger(MyCourseServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       setUserLoggedIn(request);
        if (userLoggedIn == null) {
            // kiểu tra đăng nhập pha daang nhap
            doGet(request, response);
        } else {
            try {
                //lay dữ liệu theo cousrid 
                String courseId = request.getParameter("course_id");
                Course course = new Course();
                course.setId(Integer.parseInt(courseId));
                //thêm couser cho người dùng 
                courseDao.insertCourseForUser(userLoggedIn, course);
                doGet(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(MyCourseServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setUserLoggedIn(req);
        if (userLoggedIn == null) {
            doGet(req, resp);
        } else {
            try {
                // chọn couser theo id 
                String courseId = req.getParameter("course_id");
                Course course = new Course();
                course.setId(Integer.parseInt(courseId));
                //xóa couser đã chọn theo id 
                courseDao.deleteCourseOfUser(userLoggedIn, course);
            } catch (SQLException ex) {
                Logger.getLogger(MyCourseServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
