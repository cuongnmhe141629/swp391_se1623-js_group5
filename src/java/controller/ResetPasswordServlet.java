/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import context.DBContext;
import dao.impl.UserDAOImpl;
import dao.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.User;

/**
 *
 * @author Admin
 */
public class ResetPasswordServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
    public static String getMd5(String input) {
        try {

            // Static getInstance method is called with hashing MD5
            MessageDigest md = MessageDigest.getInstance("MD5");

            // digest() method is called to calculate message digest
            //  of an input digest() return array of byte
            byte[] messageDigest = md.digest(input.getBytes());

            // Convert byte array into signum representation
            BigInteger no = new BigInteger(1, messageDigest);

            // Convert message digest into hex value
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } // For specifying wrong message digest algorithms
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String email = request.getParameter("key1");
            String hash = request.getParameter("key2");
            
            Connection con = DBContext.getConnection();
            
            try {
                String sql = "SELECT [email]\n"
                        + "      ,[hash]\n"
                        + "      ,[active]\n"
                        + "  FROM [User]\n"
                        + "  WHERE [email]=? AND [hash]=?";
                PreparedStatement stm = con.prepareStatement(sql);
                stm.setString(1, email);
                stm.setString(2, hash);
                ResultSet rs = stm.executeQuery();
               
                if (!rs.next()) {
                    request.setAttribute("title", "Rất tiếc!");
                    request.setAttribute("mess", "Đường dẫn không còn hiệu lực");
                    request.getRequestDispatcher("views/Verify.jsp").forward(request, response);
                } else {
                    PreparedStatement pst1 = con.prepareStatement("UPDATE [User]\n"
                            + "   SET [featured] = 1\n"
                            + " WHERE [email] = ? AND [hash] = ?");
                    pst1.setString(1, email);
                    pst1.setString(2, hash);
                    int i = pst1.executeUpdate();
                    if (i == 1) {
                        request.setAttribute("mail", email);
                        request.getRequestDispatcher("views/CreateNewPassword.jsp").forward(request, response);
                    } else {
                        response.sendRedirect("HomeServlet");
                    }
                }
            } catch (Exception ex) {
                System.out.println("ActivateAccount File :: " + ex);
            }
        } catch (Exception ex) {
            Logger.getLogger(ResetPasswordServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String email = request.getParameter("email");
            String pass = request.getParameter("pass");
            String npass = request.getParameter("npass");
            String re_pass = request.getParameter("repass");
            
            String choise = request.getParameter("choise");
            
            UserDAO dao = new UserDAOImpl();
            User user = new User();
            user = dao.getAccountByEmail(email);
            
            switch (choise) {
                case "forgot":
                    if (!npass.equals(re_pass)) {
                        request.setAttribute("mess", "Mật khẩu không khớp!");
                        request.getRequestDispatcher("views/createnewpassword.jsp").forward(request, response);
                    } else {
                        dao.updatePassword(email, npass);
                        request.setAttribute("mess", "Cảm ơn!");
                        request.setAttribute("title", "Đổi mật khẩu thành công !");
                        request.getRequestDispatcher("views/Verify.jsp").forward(request, response);
                    }
                    break;
                case "reset":
                    if (user.getPassword().equals(re_pass) == false) {
                        request.setAttribute("mess", "Mật khẩu cũ không đúng!");
                        request.getRequestDispatcher("views/CreateNewPassword.jsp").forward(request, response);
                    } else if (!npass.equals(re_pass)) {
                        request.setAttribute("mess", "Mật khẩu không khớp!");
                        request.getRequestDispatcher("views/CreateNewPassword.jsp").forward(request, response);
                    } else {
                        dao.updatePassword(email, npass);
                        request.setAttribute("mess", "Cảm ơn!");
                        request.setAttribute("title", "Đổi mật khẩu thành công !");
                        request.getRequestDispatcher("views/Verify.jsp").forward(request, response);
                    }
                    break;
            }
        } catch (Exception ex) {
            Logger.getLogger(ResetPasswordServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
