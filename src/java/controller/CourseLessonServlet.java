/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.CourseDAO;
import dao.EnrolDAO;
import dao.LessonDAO;
import dao.impl.CourseDAOImpl;
import dao.impl.EnrolDAOImpl;
import dao.impl.LessonDAOImpl;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Course;
import model.Enrol;
import model.Lesson;
import model.Topic;
import model.User;

/**
 *
 * @author Admin
 */
public class CourseLessonServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        int course_id = Integer.parseInt(request.getParameter("course_id").trim());
        User user = (User) session.getAttribute("user");
        int user_id = 0;
        if (user == null) {
            user_id = 0;
        } else {
            user_id = user.getUser_id();
        }
        try {
            CourseDAO courseDAO = new CourseDAOImpl();
            Course course = courseDAO.getCourseByID(course_id);
            LessonDAO lessonDAO = new LessonDAOImpl();
            EnrolDAO edao = new EnrolDAOImpl();
            Enrol checkUserEnrolCourse = edao.checkUserEnrolCourse(user_id, course_id);

            ArrayList<Lesson> listLesson = lessonDAO.getLessonByCourse(course_id);
            ArrayList<Topic> listTopic = lessonDAO.getTopicByCourse(course_id);

            request.setAttribute("course", course);
            request.setAttribute("listLesson", listLesson);
            request.setAttribute("listTopic", listTopic);
            request.setAttribute("checkURC", checkUserEnrolCourse);
            request.getRequestDispatcher("/views/CourseLesson.jsp").forward(request, response);
        } catch (IOException | SQLException | ServletException e) {
            e.printStackTrace();
            request.getRequestDispatcher("/views/CourseLesson.jsp").forward(request, response);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
