/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.impl.Price_PackageDAOImpl;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Price_Package;

/**
 *
 * @author Admin
 */
public class AdminUpdatePricePackageServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            int course_id = Integer.parseInt(request.getParameter("id"));
            request.setAttribute("id", course_id);
            int pId = Integer.parseInt(request.getParameter("pid"));
            Price_Package price = new Price_PackageDAOImpl().getPriceById(pId);
            request.setAttribute("price", price);
            request.getRequestDispatcher("/views/AdminUpdatePricePackage.jsp").forward(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(AdminUpdatePricePackageServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            int id = Integer.parseInt(request.getParameter("Id"));
            String name = request.getParameter("name");
            int access = Integer.parseInt(request.getParameter("access_duration"));
            boolean status = Boolean.parseBoolean(request.getParameter("status"));
            float listprice = Float.parseFloat(request.getParameter("listprice"));
            float saleprice = Float.parseFloat(request.getParameter("saleprice"));
            String description = request.getParameter("description");
            int course_id = Integer.parseInt(request.getParameter("course_id"));
            Price_PackageDAOImpl Pdao = new Price_PackageDAOImpl();
            Pdao.update(name, access, status, listprice, saleprice, description, course_id, id);
            response.sendRedirect("/AdminPricePackage?id="+course_id);
        } catch (SQLException ex) {
            Logger.getLogger(AdminUpdatePricePackageServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
