/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.CourseDAO;
import dao.SubjectDimensionDAO;
import dao.impl.CourseDAOImpl;
import java.io.IOException;
import java.sql.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Course;
import model.Subject_Dimension;

/**
 *
 * @author ASUS
 */
public class AdminCourseDetailServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id");
        int param_id = Integer.parseInt(id);
        try {
            CourseDAO dao = new CourseDAOImpl();
            Course courseDetail = dao.getCourseByID(param_id);
            request.setAttribute("c", courseDetail);
            request.getRequestDispatcher("/views/AdminCourseDetail.jsp").forward(request, response);
        } catch (Exception e) {
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String title = request.getParameter("title");
        String short_description = request.getParameter("short_description");
        String descripton = request.getParameter("descripton");
        int category_id = Integer.parseInt(request.getParameter("category_id"));
        Date updated_date = null;
        int status = 1;
        String img = "null";
        int tag_line = 200;
        int is_free_course = 1;
        String featured_flag = null;
        int owner = 5;

        CourseDAOImpl dao = new CourseDAOImpl();
        boolean aa = dao.updateCourse(title, short_description, descripton, owner, title, img, is_free_course, updated_date, status, featured_flag, img, tag_line, tag_line);

        response.sendRedirect("AdminCourse");

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
