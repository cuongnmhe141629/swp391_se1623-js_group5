/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import javax.servlet.ServletContext;
import model.User;
import util.SendingEmailUtil;

/**
 *
 * @author Admin
 */
public class EmailServiceIml implements EmailService {

    private static final String EMAIL_WELCOME_SUBJECT = "Welcome to OnlineLearning";
    private static final String EMAIL_REGISTER_ACTIVE = "OnlineLearning - Active Account!";
    private static final String EMAIL_FORGOT_PASSWORD = "OnlineLearning - New Password";
    private static final String EMAIL_NEW_PASSWORD = "OnlineLearning - New Password";

    @Override
    public void sendEmail(ServletContext context, User recipient, String type, String text) {
        String host = context.getInitParameter("host");
        String port = context.getInitParameter("port");
        String user = context.getInitParameter("user");
        String pass = context.getInitParameter("pass");

        try {
            String content = null;
            String subject = null;
            switch (type) {
                case "active":
                    subject = EMAIL_REGISTER_ACTIVE;
                    content = "Dear " + recipient.getFullname() + " ,your account need to be active!\n"
                            + "Click the link below to active your account:\n"
                            + text + "\n"
                            + "Thank you very much!\n";
                    break;
                case "welcome":
                    subject = EMAIL_WELCOME_SUBJECT;
                    content = "Dear " + recipient.getFullname() + " , hope you have a good time!";
                    break;
                case "forgot":
                    subject = EMAIL_FORGOT_PASSWORD;
                    content = "Dear " + recipient.getFullname() + " , we send you the reset password link!\n"
                            + "Click the link below to come to reset password:\n"
                            + text + "\n"
                            + "Thank you very much!\n";
                    break;
                case "newPass":
                    subject = EMAIL_NEW_PASSWORD;
                    content = "Dear " + recipient.getFullname() + " , we send you the password to login website!\n"
                            + "Your password is:\n"
                            + text + "\n"
                            + "Thank you very much!\n";
                    break;
            }
            SendingEmailUtil.sendEmail(host, port, user, pass, recipient.getEmail(), subject, content);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
