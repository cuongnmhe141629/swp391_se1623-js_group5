/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import javax.servlet.ServletContext;
import model.User;

/**
 *
 * @author Admin
 */
public interface EmailService {
    void sendEmail(ServletContext context, User recipient, String type, String text);
}
