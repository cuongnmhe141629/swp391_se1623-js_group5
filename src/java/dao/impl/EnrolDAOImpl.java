/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.impl;

import context.DBContext;
import dao.EnrolDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Enrol;

/**
 *
 * @author ASUS
 */
public class EnrolDAOImpl implements EnrolDAO {

    Connection conn;
    PreparedStatement ps;
    ResultSet rs;

//     public List<Enrol> getListEnrollByID(int uid){
//         List<Enrol> list = new ArrayList<>();
//         ResultSet resultSet = DBContext.querySet("SELECT * FROM [Enrol] where [enrol].[user_id] = ?",uid);
//        if (resultSet != null) {
//            try {
//                while (resultSet.next()) {
//                    list.add(new Enrol(resultSet.getInt(1), resultSet.getInt(2), resultSet.getDate(3),resultSet.getInt(4)));
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return list;
//     }
    @Override
    public Enrol checkUserEnrolCourse(int user_id, int course_id) throws SQLException {
//        String sql = "SELECT * FROM Enrol WHERE [user_id] = ? AND [course_id] = ?";
        ResultSet resultSet = DBContext.querySet("SELECT * FROM Enrol WHERE [user_id] = ? AND [course_id] = ?", user_id, course_id);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new Enrol(
                            resultSet.getInt("enrol_id"),
                            resultSet.getInt("user_id"),
                            resultSet.getInt("course_id"),
                            resultSet.getDate("date_added"),
                            resultSet.getInt("status")
                    );
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
