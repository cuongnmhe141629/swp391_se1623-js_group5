/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.impl;

import context.DBContext;
import dao.Course_CategoryDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Course_Category;

/**
 *
 * @author ASUS
 */
public class Course_CategoryDAOImpl implements Course_CategoryDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    @Override
    public List<Course_Category> getAllCourseCategories() throws SQLException {
        List<Course_Category> list = new ArrayList<>();
        ResultSet resultSet = DBContext.querySet("SELECT * from [Course_Category]");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    list.add(new Course_Category(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getString(3)
                    ));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }
}
