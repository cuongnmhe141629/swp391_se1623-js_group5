/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.impl;

import context.DBContext;
import dao.CommentDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Blog_Comment;

/**
 *
 * @author User
 */
public class CommentDAOImpl implements CommentDAO{
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    @Override
    public ArrayList<Blog_Comment> getAllCommentByBlog(int id) {
        ArrayList<Blog_Comment> comments = new ArrayList<>();
        ResultSet resultSet = DBContext.querySet("select *,(select fullname from [User] where [User].user_id = Blog_Comment.user_id) as name from Blog_Comment where Blog_Comment.blog_id = ? ",id);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    comments.add(new Blog_Comment(
                            resultSet.getInt(1),
                            resultSet.getInt(2),
                            resultSet.getInt(3),
                            resultSet.getString(4),
                            resultSet.getInt(5),
                            resultSet.getNString(6)
                    ));
                }
            } catch (Exception e) {
            }
        }
        return comments;
    }
    
    @Override
    public void comment(int blogid,int userid,String comment,int status){
        int count = 0;
        String query =("insert into Blog_Comment values(?,?,?,?)");
            try {
                conn = DBContext.getConnection();
                ps = conn.prepareStatement(query);
                ps.setInt(1, blogid);
                ps.setInt(2, userid);
                ps.setString(3, comment);
                ps.setInt(4, status);
                ps.executeUpdate();
            } catch (Exception e) {
            }
    }
    public static void main(String[] args) {
        CommentDAOImpl dao = new CommentDAOImpl();
        System.out.println(dao.getAllCommentByBlog(26));
    }
}
