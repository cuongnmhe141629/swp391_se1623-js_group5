/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.impl;

import context.DBContext;
import dao.LessonDAO;
import dao.Price_PackageDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Lesson;
import model.Price_Package;
import model.Topic;

/**
 *
 * @author User
 */
public class LessonDAOImpl implements LessonDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    @Override
    public boolean insert(String title, int course_id, int topic_id, String video_url, String lesson_type, String summary, boolean status) throws SQLException {
        try {
            String query = ("INSERT INTO [dbo].[Lesson]\n"
                    + "           ([title]\n"
                    + "           ,[course_id]\n"
                    + "           ,[topic_id]\n"
                    + "           ,[video_url]\n"
                    + "           ,[lesson_type]\n"
                    + "           ,[summary]\n"
                    + "           ,[status])\n"
                    + "            VALUES\n"
                    + "           (?,?,?,?,?,?,?)");
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, title);
            ps.setInt(2, course_id);
            ps.setInt(3, topic_id);
            ps.setString(4, video_url);
            ps.setString(5, lesson_type);
            ps.setString(6, summary);
            ps.setBoolean(7, status);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(String title, int topic_id, String video_url, String lesson_type, String summary, int lesson_id, int course_id) throws SQLException {
        try {
            String query = ("UPDATE [dbo].[Lesson]\n"
                    + "SET         [title] = ? \n"
                    + "           ,[topic_id]= ?\n"
                    + "           ,[video_url]= ?\n"
                    + "           ,[lesson_type]= ?\n"
                    + "           ,[summary]= ?\n"
                    + "WHERE      [lesson_id]=? AND [course_id] = ?");
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, title);
            ps.setInt(2, topic_id);
            ps.setString(3, video_url);
            ps.setString(4, lesson_type);
            ps.setString(5, summary);
            ps.setInt(6, lesson_id);
            ps.setInt(7, course_id);
            ps.executeUpdate();
        } catch (Exception ex) {
        }
        return false;
    }

    @Override
    public Lesson getLessonById(int lesson_id) throws SQLException {
        ResultSet resultSet = DBContext.querySet("select * from Lesson where lesson_id = ?", lesson_id);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new Lesson(
                            resultSet.getInt("lesson_id"),
                            resultSet.getString("title"),
                            resultSet.getInt("course_id"),
                            resultSet.getInt("topic_id"),
                            resultSet.getString("video_url"),
                            resultSet.getString("lesson_type"),
                            resultSet.getString("attachment"),
                            resultSet.getString("summary"),
                            resultSet.getBoolean("status")
                    );
                }
            } catch (SQLException e) {
            }
        }
        return null;
    }

    @Override
    public ArrayList<Topic> getTopicByCourse(int course_id) throws SQLException {
        ArrayList<Topic> topics = new ArrayList<>();
        ResultSet resultSet = context.DBContext.querySet("SELECT * FROM Topic WHERE course_id = ?", course_id);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    topics.add(new Topic(
                            resultSet.getInt("topic_id"),
                            resultSet.getString("title"),
                            resultSet.getInt("course_id")));
                }
            } catch (SQLException e) {
            }
        }
        return topics;
    }

    @Override
    public ArrayList<Lesson> getLessonByCourse(int course_id) throws SQLException {
        ArrayList<Lesson> lessons = new ArrayList<>();
        ResultSet resultSet = context.DBContext.querySet("SELECT * FROM Lesson WHERE course_id = ?", course_id);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    lessons.add(new Lesson(
                            resultSet.getInt("lesson_id"),
                            resultSet.getString("title"),
                            resultSet.getInt("course_id"),
                            resultSet.getInt("topic_id"),
                            resultSet.getString("video_url"),
                            resultSet.getString("lesson_type"),
                            resultSet.getString("attachment"),
                            resultSet.getString("summary"),
                            resultSet.getBoolean("status")));
                }
            } catch (SQLException e) {
            }
        }
        return lessons;
    }

    @Override
    public ArrayList<Lesson> getLessonByTopic(int topic_id) throws SQLException {
        ArrayList<Lesson> lessons = new ArrayList<>();
        ResultSet resultSet = context.DBContext.querySet("SELECT * FROM Lesson WHERE  topic_id = ?", topic_id);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    lessons.add(new Lesson(
                            resultSet.getInt("lesson_id"),
                            resultSet.getString("title"),
                            resultSet.getInt("course_id"),
                            resultSet.getInt("topic_id"),
                            resultSet.getString("video_url"),
                            resultSet.getString("lesson_type"),
                            resultSet.getString("attachment"),
                            resultSet.getString("summary"),
                            resultSet.getBoolean("status")));
                }
            } catch (SQLException e) {
            }
        }
        return lessons;
    }

    public static void main(String[] args) throws SQLException {
        LessonDAOImpl Ldao = new LessonDAOImpl();
        ArrayList<Lesson> lessons = new ArrayList<>();
        Lesson lesson = new Lesson();
        System.out.println(Ldao.insert("hi", 1, 3, "", "hi", "hello", true));
    }
}
