/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.impl;

import context.DBContext;
import dao.UserDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 *
 * @author Admin
 */
public class UserDAOImpl implements UserDAO {

    private Connection conn;
    private PreparedStatement ps;
    private ResultSet rs;

    @Override
    public ArrayList<User> getAll() throws SQLException {
        ArrayList<User> users = new ArrayList<>();
        ResultSet resultSet = context.DBContext.querySet("select * from [User]");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    users.add(new User(
                            resultSet.getString(1),
                            resultSet.getNString(2)
                    ));
                }
            } catch (Exception e) {
            }
        }
        return users;
    }

    @Override
    public ArrayList<User> getAlluserid() throws SQLException {
        ArrayList<User> users = new ArrayList<>();
        ResultSet resultSet = context.DBContext.querySet("select * from [User]");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    users.add(new User(
                            resultSet.getInt(1),
                            resultSet.getNString(2)
                    ));
                }
            } catch (SQLException e) {
            }
        }
        return users;
    }

    @Override
    public User login(String email, String pass) throws SQLException {
        ResultSet resultSet = DBContext.querySet("select * from [User] where [User].[email] = ?  and [User].[password] = ?", email, pass);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new User(
                            resultSet.getString(2),
                            resultSet.getBoolean(3),
                            resultSet.getString(4),
                            resultSet.getString(5),
                            resultSet.getString(6),
                            resultSet.getInt(7),
                            resultSet.getString(8),
                            resultSet.getInt(9)
                    );
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public User getUserByID(int id) throws SQLException {
        rs = DBContext.querySet("select * from [User] where [User].[user_id] = ? ", id);
        if (rs != null) {
            try {
                while (rs.next()) {
                    return new User(
                            rs.getInt("user_id"),
                            rs.getString("fullname"),
                            rs.getBoolean("gender"),
                            rs.getString("email"),
                            rs.getString("phone"),
                            rs.getString("password"),
                            rs.getInt("role_id"),
                            rs.getString("image"),
                            rs.getInt("status"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                    if (conn != null) {
                        conn.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public User getUserByEmail(String email) throws SQLException {
        ResultSet resultSet = DBContext.querySet("select * from [User] where [User].[email] = ?", email);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new User(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getBoolean(3),
                            resultSet.getString(4),
                            resultSet.getString(5),
                            resultSet.getString(6),
                            resultSet.getInt(7),
                            resultSet.getString(8),
                            resultSet.getInt(9));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public List<User> getUserByID(String email) throws SQLException {
        List<User> list = new ArrayList<>();
        ResultSet resultSet = DBContext.querySet("select * from [User] where [User].[email] = ? ", email);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    list.add(new User(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getBoolean(3),
                            resultSet.getString(4),
                            resultSet.getString(5),
                            resultSet.getString(6),
                            resultSet.getInt(7),
                            resultSet.getString(8),
                            resultSet.getInt(9)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public void addUser(String fullname, boolean gender, String email, String phone, String password, int role_id, String img, int status) throws SQLException {
        try {
            String sql = "INSERT INTO [dbo].[User]\n"
                    + "           ([fullname]\n"
                    + "           ,[gender]\n"
                    + "           ,[email]\n"
                    + "           ,[phone]\n"
                    + "           ,[password]\n"
                    + "           ,[role_id]\n"
                    + "           ,[image]\n"
                    + "           ,[status])\n"
                    + "     VALUES\n"
                    + "           (?,\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";

            ps = DBContext.getConnection().prepareStatement(sql);
            ps.setString(1, fullname);
            ps.setBoolean(2, gender);
            ps.setString(3, email);
            ps.setString(4, phone);
            ps.setString(5, password);
            ps.setInt(6, role_id);
            ps.setString(7, img);
            ps.setInt(8, status);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean changePassword(User user, String email) throws SQLException {
        try {
            String sql = "UPDATE [dbo].[User]\n"
                    + "   SET\n"
                    + "      [password] = ? "
                    + " WHERE [User].[email] = ?";

            PreparedStatement pr = DBContext.getConnection().prepareStatement(sql);
            pr.setString(1, user.getPassword());
            pr.setString(2, email);
            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean updateUserProfile(User user, String email) throws SQLException {
        try {
            String sql = "UPDATE [dbo].[User]\n"
                    + "   SET\n"
                    + "      [fullname] = ? "
                    + "      ,[gender] = ?"
                    + "      ,[phone] = ?"
                    + "      ,[image] = ?"
                    + " WHERE [User].[email] = ?";

            PreparedStatement pr = DBContext.getConnection().prepareStatement(sql);
            pr.setString(1, user.getFullname());
            pr.setBoolean(2, user.isGender());
            pr.setString(3, user.getPhone());
            pr.setString(4, user.getImage());
            pr.setString(5, email);
            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean updateUserAdmin(User user, String email) throws SQLException {
        try {
            String sql = "UPDATE [dbo].[User]\n"
                    + "   SET\n"
                    + "      [fullname] = ? "
                    + "      ,[gender] = ?"
                    + "      ,[phone] = ?"
                    + "      ,[password] = ?"
                    + "      ,[role_id] = ?"
                    + "      ,[image] = ?"
                    + "      ,[status] = ?"
                    + " WHERE [User].[email] = ?";

            PreparedStatement pr = DBContext.getConnection().prepareStatement(sql);
            pr.setString(1, user.getFullname());
            pr.setBoolean(2, user.isGender());
            pr.setString(3, user.getPhone());
            pr.setString(4, user.getPassword());
            pr.setInt(5, user.getRole_id());
            pr.setString(6, user.getImage());
            pr.setInt(7, user.getStatus());
            pr.setString(8, email);
            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public String getPassword(String email) throws SQLException {
        try {
            ps = DBContext.getConnection().prepareStatement("SELECT [Password] FROM dbo.[User] WHERE email = ?");
            ps.setString(1, email);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public User getAccountByEmail(String email) throws SQLException {
        try {
            String sql = "SELECT *"
                    + "  FROM [User]\n"
                    + "  WHERE [email] = ?";
            PreparedStatement stm = DBContext.getConnection().prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                User account = new User(rs.getInt("user_id"), rs.getString("fullname"), rs.getBoolean("gender"), rs.getString("email"), rs.getString("phone"),
                        rs.getString("password"), rs.getInt("role_id"), rs.getString("image"), rs.getInt("status"), rs.getBoolean("featured"), rs.getString("hash"), rs.getInt("active"));
                return account;

            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAOImpl.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (Exception ex) {
            Logger.getLogger(UserDAOImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void updateFeatured(User user, String text) throws SQLException {
        String sql = "UPDATE [User]\n"
                + "   SET featured = '?'\n"
                + " WHERE [email] = ?";
        try {
            PreparedStatement stm = DBContext.getConnection().prepareStatement(sql);
            stm.setString(1, text);
            stm.setString(2, user.getEmail());
            ResultSet rs = stm.executeQuery();

        } catch (SQLException e) {
        } catch (Exception ex) {
            Logger.getLogger(UserDAOImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void updateHashCode(User user) throws Exception {
        String sql = "UPDATE [User]\n"
                + "   SET [hash] = ?\n"
                + " WHERE [email] = ?";
        try {
            PreparedStatement stm = DBContext.getConnection().prepareStatement(sql);
            stm.setString(1, user.getMyHash());
            stm.setString(2, user.getEmail());
            ResultSet rs = stm.executeQuery();
        } catch (SQLException e) {
        }
    }

    @Override
    public void updatePassword(String mail, String pass) throws SQLException {
        String sql = "UPDATE [User]\n"
                + "   SET [password] = ?\n"
                + " WHERE [email] = ?";
        try {
            PreparedStatement stm = DBContext.getConnection().prepareStatement(sql);
            stm.setString(1, pass);
            stm.setString(2, mail);
            ResultSet rs = stm.executeQuery();

        } catch (SQLException e) {
        } catch (Exception ex) {
            Logger.getLogger(UserDAOImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public boolean addUser(User user) throws SQLException {
        int row = 0;
        try {
            String sql = "INSERT INTO [dbo].[User]\n"
                    + "           ([fullname]\n"
                    + "           ,[gender]\n"
                    + "           ,[email]\n"
                    + "           ,[phone]\n"
                    + "           ,[password]\n"
                    + "           ,[role_id]\n"
                    + "           ,[status]\n"
                    + "           ,[address])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";

            ps = DBContext.getConnection().prepareStatement(sql);
            ps.setString(1, user.getFullname());
            ps.setBoolean(2, user.isGender());
            ps.setString(3, user.getEmail());
            ps.setString(4, user.getPhone());
            ps.setString(5, user.getPassword());
            ps.setInt(6, user.getRole_id());
            ps.setInt(7, user.getStatus());
            ps.setString(8, user.getAddress());
            row = ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return row > 0;
    }

    @Override
    public boolean editUser(User user) throws SQLException {
        int row = 0;
        try {
            String sql = "UPDATE [dbo].[User]\n"
                    + "   SET [role_id] = ?\n"
                    + "      ,[status] = ?\n"
                    + " WHERE [user_id] = ?";

            ps = DBContext.getConnection().prepareStatement(sql);
            ps.setInt(1, user.getRole_id());
            ps.setInt(2, user.getStatus());
            ps.setInt(3, user.getUser_id());
            row = ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return row > 0;
    }

    @Override
    public boolean checkEmailExit(String email) throws SQLException {
        try {
            String sql = "SELECT * FROM [User] WHERE [User].[email] = ?";

            ps = DBContext.getConnection().prepareStatement(sql);
            ps.setString(1, email);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public List<User> getListEmail() throws SQLException {
        List<User> list = new ArrayList<>();
        String sql = "SELECT email FROM [User]";
        rs = DBContext.querySet(sql);
        if (rs != null) {
            try {
                while (rs.next()) {
                    list.add(new User(
                            rs.getString("email")));
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                    if (conn != null) {
                        conn.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return list;
    }

    @Override
    public List<User> searchUser(String gender, String role_id, String status) throws SQLException {
        List<User> list = new ArrayList<>();

        String sql = "SELECT * FROM [User] WHERE 1=1";
        if (!gender.isEmpty()) {
            sql += " AND gender = '" + gender + "'";
        }
        if (!role_id.isEmpty()) {
            sql += " AND role_id = '" + role_id + "'";
        }
        if (!status.isEmpty()) {
            sql += " AND status = '" + status + "'";
        }
        rs = DBContext.querySet(sql);
        if (rs != null) {
            try {
                while (rs.next()) {
                    list.add(new User(
                            rs.getInt("user_id"),
                            rs.getString("fullname"),
                            rs.getBoolean("gender"),
                            rs.getString("email"),
                            rs.getString("phone"),
                            rs.getString("password"),
                            rs.getInt("role_id"),
                            rs.getString("image"),
                            rs.getInt("status"),
                            rs.getString("address")));
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                    if (conn != null) {
                        conn.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return list;
    }

    @Override
    public List<User> getListUser() throws SQLException {
        List<User> list = new ArrayList<>();
        rs = DBContext.querySet("SELECT * FROM [User]");
        if (rs != null) {
            try {
                while (rs.next()) {
                    list.add(new User(
                            rs.getInt("user_id"),
                            rs.getString("fullname"),
                            rs.getBoolean("gender"),
                            rs.getString("email"),
                            rs.getString("phone"),
                            rs.getString("password"),
                            rs.getInt("role_id"),
                            rs.getString("image"),
                            rs.getInt("status"),
                            rs.getString("address")));
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                    if (conn != null) {
                        conn.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return list;
    }

    public static void main(String[] args) throws SQLException {
        UserDAOImpl dao = new UserDAOImpl();
        User user = dao.login("long@gmail.com", "123");
        if (user == null) {
            System.out.println("Null roi");
        } else {
            System.out.println(user);
        }
    }
}
