/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.impl;

import context.DBContext;
import dao.SubjectDimensionDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Subject_Dimension;
import model.User;

/**
 *
 * @author Admin
 */
public class SubjectDimensionDAOImpl implements SubjectDimensionDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    @Override
    public List<Subject_Dimension> getSubjectDimensionByCourseId(int courseId) throws SQLException {
        List<Subject_Dimension> list = new ArrayList<>();
        ResultSet resultSet = DBContext.querySet("SELECT * from [Subject_Dimension] WHERE [Subject_Dimension].[course_id] = ? ", courseId);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    list.add(new Subject_Dimension(
                            resultSet.getInt("dimension_id"),
                            resultSet.getString("type"),
                            resultSet.getString("name"),
                            resultSet.getString("description"),
                            resultSet.getInt("course_id")));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public Subject_Dimension getSubjectDimensionById(int dimension_id) throws SQLException {
        ResultSet resultSet = DBContext.querySet("SELECT * from [Subject_Dimension] WHERE [Subject_Dimension].[dimension_id] = ? ", dimension_id);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new Subject_Dimension(
                            resultSet.getInt("dimension_id"),
                            resultSet.getString("type"),
                            resultSet.getString("name"),
                            resultSet.getString("description"),
                            resultSet.getInt("course_id"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public void addSubjectDimension(Subject_Dimension dimension) throws SQLException {
        try {
            String sql = "INSERT INTO [dbo].[Subject_Dimension]\n"
                    + "           ([type]\n"
                    + "           ,[name]\n"
                    + "           ,[description]\n"
                    + "           ,[course_id]) "
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";

            PreparedStatement pr = DBContext.getConnection().prepareStatement(sql);
            pr.setString(1, dimension.getType());
            pr.setString(2, dimension.getName());
            pr.setString(3, dimension.getDescription());
            pr.setInt(4, dimension.getCourse_id());

            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean updateSubjectDimension(Subject_Dimension dimension, int dimension_id, int course_id) throws SQLException {
        try {
            String sql = "UPDATE [dbo].[Subject_Dimension]\n"
                    + "   SET [type] = ? \n"
                    + "      ,[name] = ?\n"
                    + "      ,[description] = ?\n"
                    + " WHERE [Subject_Dimension].[dimension_id] = ? AND [Subject_Dimension].[course_id] = ?";

            PreparedStatement pr = DBContext.getConnection().prepareStatement(sql);
            pr.setString(1, dimension.getType());
            pr.setString(2, dimension.getName());
            pr.setString(3, dimension.getDescription());
            pr.setInt(4, dimension_id);
            pr.setInt(5, course_id);
            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteSubjectDimension(int dimension_id) throws SQLException {
        try {
            String sql = "DELETE FROM [dbo].[Subject_Dimension] WHERE [Subject_Dimension].[dimension_id] = ?";
            PreparedStatement pr = DBContext.getConnection().prepareStatement(sql);
            pr.setInt(1, dimension_id);
            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
