/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.impl;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import context.DBContext;
import dao.Price_PackageDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Price_Package;

/**
 *
 * @author User
 */
public class Price_PackageDAOImpl implements Price_PackageDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    @Override
    public boolean insert(String name, int access_duration, boolean status, Float list_price, Float sale_price, String description, int Course_id) throws SQLException {
        try {
            String query = ("INSERT INTO [dbo].[Price_Package]\n"
                    + "           ([name]\n"
                    + "           ,[access_duration]\n"
                    + "           ,[status]\n"
                    + "           ,[list_price]\n"
                    + "           ,[sale_price]\n"
                    + "           ,[description]\n"
                    + "           ,[course_id])\n"
                    + "            VALUES\n"
                    + "           (?,?,?,?,?,?,?)");
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, name);
            ps.setInt(2, access_duration);
            ps.setBoolean(3, status);
            ps.setFloat(4, list_price);
            ps.setFloat(5, sale_price);
            ps.setString(6, description);
            ps.setInt(7, Course_id);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(String name, int access_duration, boolean status, Float list_price, Float sale_price, String description, int course_id, int id) throws SQLException {
        try {
            String query = ("UPDATE [dbo].[Price_Package]\n"
                    + "SET         [name] = ? \n"
                    + "           ,[access_duration]= ?\n"
                    + "           ,[status]= ?\n"
                    + "           ,[list_price]= ?\n"
                    + "           ,[sale_price]= ?\n"
                    + "           ,[description]= ?\n"
                    + "           ,[course_id]= ?\n"
                    + "WHERE      [pricePackage_id]=?");
            conn = DBContext.getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, name);
            ps.setInt(2, access_duration);
            ps.setBoolean(3, status);
            ps.setFloat(4, list_price);
            ps.setFloat(5, sale_price);
            ps.setString(6, description);
            ps.setInt(7, course_id);
            ps.setInt(8, id);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(Price_PackageDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public ArrayList<Price_Package> ListPrice(int Id) throws SQLException {
        ArrayList<Price_Package> prices = new ArrayList<>();
        ResultSet resultSet = context.DBContext.querySet("SELECT * FROM Price_Package where course_id = ?", Id);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    prices.add(new Price_Package(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getInt(3),
                            resultSet.getBoolean(4),
                            resultSet.getFloat(5),
                            resultSet.getFloat(6),
                            resultSet.getString(7),
                            resultSet.getInt(8)));
                }
            } catch (SQLException e) {
            }
        }
        return prices;
    }

    @Override
    public Price_Package getPriceById(int pId) throws SQLException {
        ResultSet resultSet = DBContext.querySet("select *from Price_Package where pricePackage_id = ?", pId);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new Price_Package(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getInt(3),
                            resultSet.getBoolean(4),
                            resultSet.getFloat(5),
                            resultSet.getFloat(6),
                            resultSet.getString(7),
                            resultSet.getInt(8)
                    );
                }
            } catch (SQLException e) {
            }
        }
        return null;
    }

    public static void main(String[] args) throws SQLException {
        Price_PackageDAOImpl Pdao = new Price_PackageDAOImpl();
        System.out.println(Pdao.insert("hi", 4, true, (float) 49.9, (float) 29.9, "hello", 2));
    }
}
