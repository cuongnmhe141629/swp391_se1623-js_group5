/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.impl;

import context.DBContext;
import dao.BlogDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Blog;

/**
 *
 * @author User
 */
public class BlogDAOImpl implements BlogDAO{
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    @Override
    public ArrayList<Blog> getAllBlogs() {
        ArrayList<Blog> blogs = new ArrayList<>();
        ResultSet resultSet = DBContext.querySet("select *,(select fullname from [User] where [User].user_id = Blog.user_id) as author,(select title from Blog_Category where Blog_Category.blog_category_id = Blog.blog_category_id)as title_blog  from Blog");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    blogs.add(new Blog(
                            resultSet.getInt(1),
                            resultSet.getInt(2),
                            resultSet.getInt(3),
                            resultSet.getString(4),
                            resultSet.getString(5),
                            resultSet.getString(6),
                            resultSet.getString(7),
                            resultSet.getString(8),
                            resultSet.getString(9),
                            resultSet.getNString(10),
                            resultSet.getNString(11)
                    ));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return blogs;
    }
    @Override
    public ArrayList<Blog> getTop5Blog() {
        ArrayList<Blog> blogs = new ArrayList<>();
        ResultSet resultSet = DBContext.querySet("SELECT TOP 5 * FROM Blogs");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    blogs.add(new Blog(
                            resultSet.getInt(1),
                            resultSet.getInt(2),
                            resultSet.getInt(3),
                            resultSet.getString(4),
                            resultSet.getString(5),
                            resultSet.getString(6),
                            resultSet.getString(7),
                            resultSet.getString(8),
                            resultSet.getString(9)
                    ));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return blogs;
     }
     
    @Override
     public Blog getBlogByID(int id) {
        ResultSet resultSet = DBContext.querySet("select *,(select fullname from [User] where [User].user_id = Blog.user_id) as author,(select title from Blog_Category where Blog_Category.blog_category_id = Blog.blog_category_id)as title_blog  from Blog where blog_id = ?", id);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new Blog(
                            resultSet.getInt(1),
                            resultSet.getInt(2),
                            resultSet.getInt(3),
                            resultSet.getString(4),
                            resultSet.getString(5),
                            resultSet.getString(6),
                            resultSet.getString(7),
                            resultSet.getString(8),
                            resultSet.getString(9),
                            resultSet.getNString(10),
                            resultSet.getNString(11)
                            );
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
     
    @Override
     public int getNumberPage(){
          ResultSet resultSet = DBContext.querySet("select COUNT(*) from Blog");
          if(resultSet != null){
              try{
                  while(resultSet.next()){
                      int total = resultSet.getInt(1);
                      int countPage = 0 ;
                      countPage = total/2;
                      if(total % 2 !=0){
                          countPage++;
                      }
                      return countPage;
                   }
              }catch(SQLException e){
          }
        }
        return 0;
    }
     
    @Override
    public ArrayList<Blog> pagingBlog(int index){
        ArrayList<Blog> blogs = new ArrayList<>();
        String query =("select *,(select fullname from [User] where [User].user_id = Blog.user_id) as author,(select title from Blog_Category where Blog_Category.blog_category_id = Blog.blog_category_id)as title_blog  from Blog\n" +
                       "order by blog_id\n" +
                       "OFFSET ? ROWS FETCH NEXT 2 ROWS ONLY");
            try {
                conn = DBContext.getConnection();
                ps = conn.prepareStatement(query);
                ps.setInt(1,(index-1)*2);
                rs= ps.executeQuery();
                while (rs.next()) {
                    blogs.add(new Blog(
                            rs.getInt(1),
                            rs.getInt(2),
                            rs.getInt(3),
                            rs.getString(4),
                            rs.getString(5),
                            rs.getString(6),
                            rs.getString(7),
                            rs.getString(8),
                            rs.getString(9),
                            rs.getNString(10),
                            rs.getNString(11)
                            ));
                }
            } catch (Exception e) {
            }
        return blogs;
    }
    @Override
    public ArrayList<Blog> getBlogByTitle(String title) {
        ArrayList<Blog> blogs = new ArrayList<>();
        String query =("select *,(select fullname from [User] where [User].user_id = Blog.user_id) as author,(select title from Blog_Category where Blog_Category.blog_category_id = Blog.blog_category_id)as title_blog  from Blog where title like ?");
            try {
                conn = DBContext.getConnection();
                ps = conn.prepareStatement(query);
                ps.setString(1,"%"+title+"%");
                rs= ps.executeQuery();
                while (rs.next()) {
                    blogs.add(new Blog(
                            rs.getInt(1),
                            rs.getInt(2),
                            rs.getInt(3),
                            rs.getString(4),
                            rs.getString(5),
                            rs.getString(6),
                            rs.getString(7),
                            rs.getString(8),
                            rs.getString(9),
                            rs.getNString(10),
                            rs.getNString(11)
                            ));
                }
            } catch (Exception e) {
            }
        return blogs;
    }
     
     public static void main(String[] args) {
         BlogDAOImpl dao = new BlogDAOImpl();
         System.out.println(dao.getAllBlogs());
    }
}
