/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.impl;

import context.DBContext;
import dao.CourseDAO;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Course;
import model.Course_Category;
import model.Lession;
import model.Lesson;
import model.Topic;
import model.User;

/**
 *
 * @author ASUS
 */
public class CourseDAOImpl implements CourseDAO {

    Connection conn;
    PreparedStatement ps;
    ResultSet rs;

    @Override
    public List<Course> getListCourse() throws SQLException {
        List<Course> list = new ArrayList<>();
        ResultSet resultSet = DBContext.querySet("SELECT c.[course_id],c.[title], [short_description] ,c.[description],[category_id],[tag_line],[thumbnail],[is_free_course],[updated_date],c.[status],[featured_flag],u.[fullname] AS [owner],COUNT(l.lesson_id) AS lesson_count, [price]\n"
                + "  FROM [dbo].[Course] c LEFT JOIN [dbo].[User] u ON c.[owner] = u.[user_id]\n"
                + "		 LEFT JOIN [dbo].[Lesson] l ON c.course_id = l.course_id\n"
                + "GROUP BY c.course_id,c.[title], [short_description] ,c.[description],[category_id],[tag_line],[thumbnail],[is_free_course],[updated_date],c.[status],[featured_flag],u.[fullname], [price]");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    list.add(new Course(resultSet.getInt("course_id"),
                            resultSet.getString("title"),
                            resultSet.getString("short_description"),
                            resultSet.getString("description"),
                            resultSet.getInt("category_id"),
                            resultSet.getString("tag_line"),
                            resultSet.getString("thumbnail"),
                            resultSet.getInt("is_free_course"),
                            resultSet.getDate("updated_date"),
                            resultSet.getBoolean("status"),
                            resultSet.getString("featured_flag"),
                            resultSet.getString("owner"),
                            resultSet.getInt("lesson_count"),
                            resultSet.getFloat("price")));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public List<Course> getCourseByPage(List<Course> list, int start, int end) throws SQLException {
        List<Course> t = new ArrayList<>();
        for (int i = start; i < end; i++) {
            t.add(list.get(i));
        }
        return t;
    }

    @Override
    public List<Course> getListCourseByCategory(int category_id) throws SQLException {
        List<Course> list = new ArrayList<>();
        ResultSet resultSet = DBContext.querySet("SELECT c.[course_id],c.[title], [short_description] ,c.[description],[category_id],[tag_line],[thumbnail],[is_free_course],[updated_date],c.[status],[featured_flag],u.[fullname] AS [owner], [price]\n"
                + "  FROM [dbo].[Course] c LEFT JOIN [dbo].[User] u ON c.[owner] = u.[user_id]\n"
                + "WHERE c.[category_id] = ?", category_id);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    list.add(new Course(resultSet.getInt("course_id"),
                            resultSet.getString("title"),
                            resultSet.getString("short_description"),
                            resultSet.getString("description"),
                            resultSet.getInt("category_id"),
                            resultSet.getString("tag_line"),
                            resultSet.getString("thumbnail"),
                            resultSet.getInt("is_free_course"),
                            resultSet.getDate("updated_date"),
                            resultSet.getBoolean("status"),
                            resultSet.getString("featured_flag"),
                            resultSet.getString("owner"),
                            resultSet.getFloat("price")));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public Course getCourseByID(int course_id) {
        ResultSet resultSet = DBContext.querySet("SELECT c.[course_id],c.[title], [short_description] ,c.[description],[category_id],[tag_line],[thumbnail],"
                + "[is_free_course],[updated_date],c.[status],[featured_flag],u.[fullname] AS [owner], [price], u.[description] AS [user_description],u.[image] AS [user_photo]\n"
                + "FROM [dbo].[Course] c LEFT JOIN [dbo].[User] u ON c.[owner] = u.[user_id] \n"
                + "WHERE  c.[course_id] = ?", course_id);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new Course(resultSet.getInt("course_id"),
                            resultSet.getString("title"),
                            resultSet.getString("short_description"),
                            resultSet.getString("description"),
                            resultSet.getInt("category_id"),
                            resultSet.getString("tag_line"),
                            resultSet.getString("thumbnail"),
                            resultSet.getInt("is_free_course"),
                            resultSet.getDate("updated_date"),
                            resultSet.getBoolean("status"),
                            resultSet.getString("featured_flag"),
                            resultSet.getString("owner"),
                            resultSet.getFloat("price"),
                            resultSet.getString("user_description"),
                            resultSet.getString("user_photo"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public Course getCourseByOwner(int owner_id) throws SQLException {
        ResultSet resultSet = DBContext.querySet("select * from [Course] where [Course].[owner] = ?", owner_id);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    return new Course(resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getString(3),
                            resultSet.getString(4),
                            resultSet.getInt(5),
                            resultSet.getString(6),
                            resultSet.getString(7),
                            resultSet.getInt(8),
                            resultSet.getDate(9),
                            resultSet.getBoolean(10),
                            resultSet.getString(11),
                            resultSet.getString(12));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    @Override
    public boolean updateCourse(String title, String short_description, String descripton, int category_id, String tag_line, String thumbnail, int is_free_course, Date updated_date, int status, String featured_flag, String owner, int dimension_id, int pricePackage_id) {
        CourseDAOImpl dao = new CourseDAOImpl();
        try {
            String sql = "UPDATE [dbo].[Course]\n"
                    + "   SET [title] = ? \n"
                    + "      ,[short_description] = ?\n"
                    + "      ,[description] = ? \n"
                    + "      ,[category_id] = ? \n"
                    + "      ,[tag_line] = ? \n"
                    + "      ,[thumbnail] = ? \n"
                    + "      ,[is_free_course] = ? \n"
                    + "      ,[updated_date] = ? \n"
                    + "      ,[status] = ? \n"
                    + "      ,[featured_flag] = ? \n"
                    + "      ,[owner] = ? \n"
                    + " WHERE [course].[id]=?";
            PreparedStatement pr = DBContext.getConnection().prepareStatement(sql);
            pr.setString(1, title);
            pr.setString(2, short_description);
            pr.setString(3, descripton);
            pr.setInt(4, category_id);
            pr.setString(5, tag_line);
            pr.setString(6, thumbnail);
            pr.setInt(7, is_free_course);
            pr.setDate(8, updated_date);
            pr.setInt(9, status);
            pr.setString(10, featured_flag);
            pr.setString(11, owner);

            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean insertCourse(String title, String description, int category_id, String image, boolean status, int owner) {
        try {
            String query = "INSERT INTO [dbo].[Course]\n"
                    + "           ([title]\n"
                    + "           ,[description]\n"
                    + "           ,[category_id]\n"
                    + "           ,[thumbnail]\n"
                    + "           ,[status]\n"
                    + "           ,[owner])\n"
                    + "     VALUES(?,?,?,?,?,?)";
            ps = DBContext.getConnection().prepareStatement(query);
            ps.setString(1, title);
            ps.setString(2, description);
            ps.setInt(3, category_id);
            ps.setString(4, image);
            ps.setBoolean(5, status);
            ps.setInt(6, owner);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public ArrayList<Course> GetTop5Course() {
        ArrayList<Course> coures = new ArrayList<>();
        ResultSet resultSet = context.DBContext.querySet("SELECT TOP 5 * FROM Course ");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    coures.add(new Course(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getString(3),
                            resultSet.getString(4),
                            resultSet.getInt(5),
                            resultSet.getString(6),
                            resultSet.getString(7),
                            resultSet.getInt(8),
                            resultSet.getDate(9),
                            resultSet.getBoolean(10),
                            resultSet.getString(11),
                            resultSet.getString(12),
                            resultSet.getFloat(13)));
                }
            } catch (Exception e) {
            }
        }
        return coures;
    }

    @Override
    public ArrayList<Course_Category> ListCategory() {
        ArrayList<Course_Category> cc = new ArrayList<>();
        ResultSet resultSet = context.DBContext.querySet("SELECT * FROM Course_Category");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    cc.add(new Course_Category(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getString(3)));
                }
            } catch (Exception e) {
            }
        }
        return cc;
    }

    @Override
    public boolean deleteCourse(int id) {
        try {
            String sql = "DELETE FROM [dbo].[Course] where [Course].[course_id] = ?";
            PreparedStatement pr = DBContext.getConnection().prepareStatement(sql);
            pr.setInt(1, id);
            pr.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public ArrayList<Course> GetCoureOfUser(User user) {
        ArrayList<Course> list = null;
        try {
            list = new ArrayList<>();
            String sql = "SELECT c.*,category_name = ccate.name  FROM [dbo].[Course] c join [dbo].[Enrol] cu on c.course_id = cu.course_id join [dbo].[Course_Category] ccate on c.category_id = ccate.id where user_id = ?";
            PreparedStatement ps = DBContext.getConnection().prepareCall(sql);
            ps.setInt(1, user.getUser_id());
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                Course c = new Course(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getInt(5),
                        resultSet.getString(6),
                        resultSet.getString(7),
                        resultSet.getInt(8),
                        resultSet.getDate(9),
                        resultSet.getBoolean(10),
                        resultSet.getString(11),
                        resultSet.getString(12));
                c.setCategory_name(resultSet.getString("category_name"));
                list.add(c);

            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }

    @Override
    public boolean insertCourseForUser(User user, Course course) {
        boolean isSuccess = false;
        try {
            String sql = "INSERT INTO [dbo].[Enrol] values(?,?)";
            PreparedStatement ps = DBContext.getConnection().prepareStatement(sql);
            ps.setInt(1, user.getUser_id());
            ps.setInt(2, course.getId());
            isSuccess = ps.execute();

        } catch (Exception e) {
            isSuccess = false;
            System.out.println(e);
        }
        return isSuccess;
    }

    @Override
    public boolean deleteCourseOfUser(User user, Course course) {
        boolean isSuccess = false;
        try {
            String sql = "DELETE FROM [dbo].[Enrol] where user_id = ? and course_id = ?";
            PreparedStatement ps = DBContext.getConnection().prepareStatement(sql);
            ps.setInt(1, user.getUser_id());
            ps.setInt(2, course.getId());
            isSuccess = ps.execute();
        } catch (Exception e) {
            isSuccess = false;
            System.out.println(e);
        }
        return isSuccess;
    }

    @Override
    public List<Lession> getListLessionByCourseId(String id) {
        List<Lession> list = new ArrayList<>();
        ResultSet resultSet = DBContext.querySet("SELECT * FROM lesson where [lesson].[course_id] = ?", id);
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    list.add(new Lession(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getInt(4), resultSet.getString(5), resultSet.getString(6), resultSet.getString(7), resultSet.getString(8), resultSet.getInt(9)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public List<Course> searchCourse(String search) throws SQLException {
        List<Course> list = new ArrayList<>();
        ResultSet resultSet = DBContext.querySet("SELECT c.[course_id],c.[title], [short_description] ,c.[description],[category_id],[tag_line],[thumbnail],[is_free_course],[updated_date],c.[status],[featured_flag],u.[fullname] AS [owner], [price]\n"
                + "  FROM [dbo].[Course] c LEFT JOIN [dbo].[User] u ON c.[owner] = u.[user_id]\n"
                + "WHERE c.[title] LIKE '%" + search + "%'");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    list.add(new Course(resultSet.getInt("course_id"),
                            resultSet.getString("title"),
                            resultSet.getString("short_description"),
                            resultSet.getString("description"),
                            resultSet.getInt("category_id"),
                            resultSet.getString("tag_line"),
                            resultSet.getString("thumbnail"),
                            resultSet.getInt("is_free_course"),
                            resultSet.getDate("updated_date"),
                            resultSet.getBoolean("status"),
                            resultSet.getString("featured_flag"),
                            resultSet.getString("owner"),
                            resultSet.getFloat("price")));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }
}
