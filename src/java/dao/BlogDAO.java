/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.SQLException;
import java.util.ArrayList;
import model.Blog;

/**
 *
 * @author User
 */
public interface BlogDAO {
    public ArrayList<Blog> getAllBlogs() throws SQLException;
    public ArrayList<Blog> getTop5Blog() throws SQLException;

    /**
     *
     * @param id
     * @return
     * @throws SQLException
     */
    public Blog getBlogByID(int id) throws SQLException;
    
    public int getNumberPage() throws SQLException;
    
    public ArrayList<Blog> pagingBlog(int index) throws SQLException;
    
    public ArrayList<Blog> getBlogByTitle(String title) throws SQLException;
}
