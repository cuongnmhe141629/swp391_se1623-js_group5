/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;
import model.Course;
import model.Course_Category;
import model.Lession;
import model.Topic;
import model.User;

/**
 *
 * @author Admin
 */
public interface CourseDAO {

    public List<Course> getListCourse() throws SQLException;

    public Course getCourseByID(int id) throws SQLException;

    public boolean updateCourse(String title, String short_description, String descripton, int category_id, String tag_line, String thumbnail, int is_free_course, Date updated_date, int status, String featured_flag, String owner, int dimension_id, int pricePackage_id) throws SQLException;

    public boolean insertCourse(String title, String description, int category_id, String image, boolean status, int owner) throws SQLException;

    public ArrayList<Course> GetTop5Course() throws SQLException;

    public ArrayList<Course_Category> ListCategory() throws SQLException;

    public Course getCourseByOwner(int owner_id) throws SQLException;

    public boolean deleteCourse(int id) throws SQLException;

    public ArrayList<Course> GetCoureOfUser(User user) throws SQLException;

    public boolean insertCourseForUser(User user, Course course) throws SQLException;

    public boolean deleteCourseOfUser(User username, Course course) throws SQLException;

    public List<Lession> getListLessionByCourseId(String id) throws SQLException;

    public List<Course> getListCourseByCategory(int category_id) throws SQLException;

    public List<Course> searchCourse(String search) throws SQLException;

    public List<Course> getCourseByPage(List<Course> list, int start, int end) throws SQLException;

}
