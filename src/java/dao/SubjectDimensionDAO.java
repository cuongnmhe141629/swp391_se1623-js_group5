/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.SQLException;
import java.util.List;
import model.Subject_Dimension;

/**
 *
 * @author Admin
 */
public interface SubjectDimensionDAO {

    public List<Subject_Dimension> getSubjectDimensionByCourseId(int courseId) throws SQLException;

    public void addSubjectDimension(Subject_Dimension sd) throws SQLException;

    public boolean updateSubjectDimension(Subject_Dimension dimension, int dimension_id, int course_id) throws SQLException;

    public boolean deleteSubjectDimension(int dimension_id) throws SQLException;

    public Subject_Dimension getSubjectDimensionById(int dimension_id) throws SQLException;
}
