/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.User;

/**
 *
 * @author Admin
 */
public interface UserDAO {

    public User login(String email, String pass) throws SQLException;

    public List<User> getListUser() throws SQLException;

    public User getUserByID(int id) throws SQLException;

    public User getUserByEmail(String email) throws SQLException;

    public List<User> getUserByID(String email) throws SQLException;

    public boolean checkEmailExit(String email) throws SQLException;

    public boolean addUser(User user) throws SQLException;

    public boolean editUser(User user) throws SQLException;

    public void addUser(String fullname, boolean gender, String email, String phone, String password, int role_id, String img, int status) throws SQLException;

    public boolean changePassword(User user, String email) throws SQLException;

    public boolean updateUserProfile(User user, String email) throws SQLException;

    public boolean updateUserAdmin(User user, String email) throws SQLException;

    public String getPassword(String email) throws SQLException;

    public User getAccountByEmail(String email) throws SQLException;

    public void updateFeatured(User user, String text) throws SQLException;

    public void updateHashCode(User user) throws Exception;

    public void updatePassword(String mail, String pass) throws SQLException;

    public ArrayList<User> getAll() throws SQLException;

    public List<User> searchUser(String gender, String role_id, String status) throws SQLException;

    public List<User> getListEmail() throws SQLException;

     public ArrayList<User> getAlluserid() throws SQLException;

}
