/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.SQLException;
import java.util.ArrayList;
import model.Lesson;
import model.Topic;

/**
 *
 * @author User
 */
public interface LessonDAO {

    public boolean insert(String title, int course_id, int topic_id, String video_url, String lesson_type, String summary, boolean status) throws SQLException;

    public boolean update(String title, int topic_id, String video_url, String lesson_type, String summary, int lesson_id, int course_id) throws SQLException;

    public ArrayList<Lesson> getLessonByCourse(int course_id) throws SQLException;

    public Lesson getLessonById(int lId) throws SQLException;

    public ArrayList<Lesson> getLessonByTopic(int topic_id) throws SQLException;

    public ArrayList<Topic> getTopicByCourse(int course_id) throws SQLException;
}
