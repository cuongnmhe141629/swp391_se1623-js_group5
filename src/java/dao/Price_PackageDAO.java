/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.SQLException;
import java.util.ArrayList;
import model.Price_Package;

/**
 *
 * @author User
 */
public interface Price_PackageDAO {
    public boolean insert(String name,int access_duration,boolean status,Float list_price,Float sale_price,String description,int course_id) throws SQLException;
    public boolean update(String name,int access_duration,boolean status,Float list_price,Float sale_price,String description,int course_id,int id) throws SQLException;
    public ArrayList<Price_Package> ListPrice(int Id) throws SQLException;
    public Price_Package getPriceById(int pId) throws SQLException;
}
