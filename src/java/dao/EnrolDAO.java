/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;
import model.Course;
import model.Course_Category;
import model.Enrol;
import model.User;

/**
 *
 * @author Admin
 */
public interface EnrolDAO {

//    public List<Enrol> getListEnrollByID(String uid) throws SQLException;
    public Enrol checkUserEnrolCourse(int user_id, int course_id) throws SQLException;

}
