/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.SQLException;
import java.util.List;
import model.Role;

/**
 *
 * @author Admin
 */
public interface RoleDAO {

    public List<Role> getAllRole() throws SQLException;
}
