/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.ArrayList;
import model.Blog_Comment;

/**
 *
 * @author User
 */
public interface CommentDAO {
    public ArrayList<Blog_Comment> getAllCommentByBlog(int id);
    public void comment(int blogid,int userid,String comment,int status);
}
