<%-- 
    Document   : ResetPassword
    Created on : Aug 10, 2022, 9:58:12 AM
    Author     : Admin
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Đổi mật khẩu</title>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css" />
    <link href="../css/unicons/css/unicons.css" type="text/css" rel="stylesheet" />
    <link href="../css/semantic/semantic.min.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
    <!-- Difference CSS  -->
    <link rel="stylesheet" type="text/css" href="../css/home.css" />
    <link rel="stylesheet" type="text/css" href="../css/demo.css" />
    <meta name="csrf-token" content="W2gijDDalPkcKeO8wOWv8hwpSZv2sdNWEUsfphLs" />
</head>
<link rel="stylesheet" type="text/css" href="../css/login.css" />

<body>
   <header>
            <jsp:include page="Header.jsp"></jsp:include>
        </header>
    <div class="form-ctn">
        <form action="../ForgotPasswordServlet" method="post">
            <input type="hidden" name="_token" value="W2gijDDalPkcKeO8wOWv8hwpSZv2sdNWEUsfphLs">
            <h1>Quên Mật Khẩu</h1>
            <p style="color: red" class="text-danger">${requestScope.mess}</p>
            <input type="text" id="username" name="email" placeholder="Email" required />
            <button class="btn-resetPass" type="submit" style="text-transform: uppercase;">Lấy link</button>
        </form>
    </div>
   <footer>
            <jsp:include page="Footer.jsp"></jsp:include>
        </footer>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>
    <!-- <script src="css/login/includeHTML.js"></script>
    <script>
        includeHTML();
    </script> -->

</body>

</html>
