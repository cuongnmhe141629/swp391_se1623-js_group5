<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Trang chủ</title>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css" />
        <link href="../css/unicons/css/unicons.css" type="text/css" rel="stylesheet" />
        <link href="../css/semantic/semantic.min.css" type="text/css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
        <!--         Difference CSS  -->
        <link rel="stylesheet" type="text/css" href="../css/home.css" />
        <link rel="stylesheet" type="text/css" href="../css/demo.css" />
        <meta name="csrf-token" content="pWKYsTnljZgm1p0UVE49TcaqrKHop1jCQv3bw7bS" />
        <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
    </head>

    <body class="homepage"> 
        <header>
            <jsp:include page="Header.jsp"></jsp:include>
            </header>
            <header class="hd-header">
                <section class="hd-slider-header">
                    <!--                                        <div class="">
                                                                <img class="hd-slider-header__banner" src="../images/1.png" alt="" />
                                                            </div>-->
                    <div class="">
                        <img class="hd-slider-header__banner" src="../images/2.png" alt="" />
                    </div>
                    <div class="">
                        <img class="hd-slider-header__banner" src="../images/3.png" alt="" />
                    </div>
                </section>
                <menu class="hd-menu-section">
                    <div class="hd-menu-container">
                        <ul class="hd-menu">
                            <li class="hd-menu__li">
                                <a href="#">Khóa học</a>
                                <div class="hd-menu__li_dropdown" id="">
                                    <ul>
                                        <c:forEach items="${cc}" var="o">
                                        <li><a href="#">${o.name}</a></li>
                                    </c:forEach>
                                </ul>
                            </div>
                        </li>
                        <li class="hd-menu__li">
                            <a href="#">KHAI GIẢNG</a>
                            <div class="hd-menu__li_dropdown" id="categories_1">
                                <ul>
                                    <c:forEach items="${cc}" var="o">
                                        <li class="list-group-item text-white"><a href="#">${o.name}</a></li>
                                    </c:forEach>
                                </ul>
                            </div>
                        </li>
                    </ul>
                    <div id="menu-collapse" class="hd-menu-collapse" href="javascript:void(0);">
                        <a id="menu-collapse-btn" class="hd-menu-collapse__a">☰</a>
                    </div>
                </div>
                <div id="all-menu" class="hd-all-menu" aria-expanded="true">
                    <div class="hd-col-menu">
                        <div class="hd-menu-group">
                            <div class="hd-group-head" style="margin-bottom: 5px;padding-bottom: 5px;border-bottom: 3px solid #3b5b8c;">
                                <a class="hd-group-head__title-group" href="#">Tin Tức</a>
                            </div>
                            <div class="hd-row">
                                <ul>
                                    <li><a href="#">Khoá học HTML</a></li>
                                    <li><a href="#">Khoá học JavaScrip</a></li>
                                    <li><a href="#">Khoá học Python</a></li>
                                    <li><a href="#">Khoá học Tiếng Nhật</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="hd-col-menu">
                        <div class="hd-menu-group">
                            <div class="hd-group-head" style="margin-bottom: 5px;padding-bottom: 5px;border-bottom: 3px solid #3b5b8c;">
                                <a class="hd-group-head__title-group" href="#" target="_blank" rel="noopener">ỨNG DỤNG</a>
                            </div>
                            <div class="hd-row">
                                <c:forEach items="${cc}" var="o">
                                    <li><a href="#">${o.name}</a></li>
                                </c:forEach>
                            </div>

                        </div>
                    </div>

                </div>
            </menu>
        </header>
        <section class="homepage section section--1st">
            <div class="homepage section--1st-left">
                <span></span>
                <h2 class="homepage section__h2">
                    Những khóa học bán chạy nhất
                </h2>
                <hr style="margin-left: 0" />
                <p>
                    Web học online bao gồm các khóa học chất lượng cao gồm các giá cả phù hợp đã giảm giá hoặc free cho những người tham gia.Hoc online thiết lập những chuyên đề huấn luyện phù hợp với từng loại hình doanh nghiệp. Setup toàn bộ cơ chế vận hành nhằm thúc đẩy phát triển chiến lược, đo lường rủi ro... Xây dựng biểu mẫu phù hợp cho doanh nghiệp theo từng phòng ban (tài chính kế
                    toán, kinh doanh, nhân sự,...).
                </p>
            </div>
            <div class="homepage section--1st-right">
                <img src="https://hocexcel.online/caches/cc_large/cou_avatar/2017/05_04/2111f454c1b9aa6be4efe61ee908a3da.png" alt="" />
            </div>
        </section>

        <section class="homepage section section--3rd">
            <h2 class="homepage section__h2">Top 5 Khóa Học</h2>
            <hr />
            <div class="homepage slider slider-content1">
                <c:forEach items="${courses}" var="c">
                    <div class="homepage owl-item active">
                        <div class="homepage item">
                            <div class="homepage fcrse_1">
                                <a href="/CourseDetail?course_id=${c.id}" class="homepage fcrse_img">
                                    <img src="<%=request.getContextPath()%>/images/${c.thumbnail}" alt="" />
                                    <div class="homepage prce142">                                       
                                        <div style="font-size: 0.9rem; font-weight: 300; color: #ccc">
                                        </div>
                                    </div>
                                    <c:if test="${c.is_free_course == 0}">
                                        <div class="homepage makh">${c.price} VNĐ</div>
                                    </c:if>
                                    <c:if test="${c.is_free_course == 1}">
                                        <div class="homepage makh">Miễn phí</div>
                                    </c:if>
                                </a>

                                <div class="homepage fcrse_content">
                                    <h3 class="homepage bl-title">
                                        <a href="/CourseDetail?course_id=${c.id}" data-language-course-name="027">${c.title}</a>
                                    </h3>

                                    <div class="homepage auth1lnkprce">
                                        <div class="homepage education_block_author">
                                            <h5><a tabindex="-1">Đăng ký ngay</a></h5>
                                        </div>
                                        <a href="/CourseDetail?course_id=${c.id}">
                                            <div class="homepage shrt-cart-btn">

                                                <i class="homepage"></i> <span data-language-course-details>Xem chi tiết</span>

                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>

        </section>
        <section class="homepage section section--7th">
            <h2 class="homepage section__h2" data-language="32">KHÓA HỌC SẮP TỚI</h2>
            <hr />
            <div class="homepage video-container">
                <div class="homepage video-bl video-left">
                    <div class="homepage video">
                        <a href="">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/WS05AU6YYm4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                            
                        </a>
                    </div>
                </div>
                <div class="homepage video-bl video-right">
                    <div class="homepage video">
                        <a href="">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/NZj6LI5a9vc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </a>
                    </div>
                    <div class="homepage video">
                        <a href="">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/ESs78ZdCETg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </a>
                    </div>
                    <div class="homepage video">
                        <a href="">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/opmSs9qMFik" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="homepage section section--9th">
            <h2 class="homepage section__h2" data-language="33">DOANH NGHIỆP CHÚNG TÔI</h2>
            <hr />

            <div class="homepage slider slider-content7">
                <div class="homepage comment-bl">
                    <div class="homepage item" style="height: 100%">
                        <img src="../images/kh.jpg" alt="" style="height: 100%" />
                    </div>
                </div>
                <div class="homepage comment-bl">
                    <div class="homepage item" style="height: 100%">
                        <img src="../images/kh1.jpg" alt="" style="height: 100%" />
                    </div>
                </div>
                <div class="homepage comment-bl">
                    <div class="homepage item" style="height: 100%">
                        <img src="../images/kh2.jpg" alt="" style="height: 100%" />
                    </div>
                </div>
                <div class="homepage comment-bl">
                    <div class="homepage item" style="height: 100%">
                        <img src="../images/kh.jpg" alt="" style="height: 100%" />
                    </div>
                </div>
                <div class="homepage comment-bl">
                    <div class="homepage item" style="height: 100%">
                        <img src="../images/kh1.jpg" alt="" style="height: 100%" />
                    </div>
                </div>
                <div class="homepage comment-bl">
                    <div class="homepage item" style="height: 100%">
                        <img src="../images/kh2.jpg" alt="" style="height: 100%" />
                    </div>
                </div>
            </div>
        </section>
        <footer>
            <jsp:include page="Footer.jsp"></jsp:include>
        </footer>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>
        <script src="../js/script.js" defer></script>

    </body>

</html>

</html>

</html>