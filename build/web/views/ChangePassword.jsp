<%-- 
    Document   : Home
    Created on : Aug 9, 2022, 3:41:37 PM
    Author     : PV
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>THAY ĐỔI MẬT KHẨU</title>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css" />
        <link href="../css/unicons/css/unicons.css" type="text/css" rel="stylesheet" />
        <link href="../css/semantic/semantic.min.css" type="text/css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
        <!--         Difference CSS  -->
        <link rel="stylesheet" type="text/css" href="../css/home.css" />
        <link rel="stylesheet" type="text/css" href="../css/demo.css" />
        <link rel="stylesheet" type="text/css" href="../css/login.css" />
        <link rel="stylesheet" type="text/css" href="../css/register.css" />
        <meta name="csrf-token" content="pWKYsTnljZgm1p0UVE49TcaqrKHop1jCQv3bw7bS" />
        <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
    </head>

    <body> 
        <header>
            <jsp:include page="Header.jsp"></jsp:include>
            </header>
            <div class="form-ctn">
                <form action="../ChangePassword" method="post" >
                <input type="hidden" name="_token" value="W2gijDDalPkcKeO8wOWv8hwpSZv2sdNWEUsfphLs">
                <input hidden="true" type="email" name="email" value="${sessionScope.email}"/>
                <!--<h1 data-language="14">THAY ĐỔI MẬT KHẨU</h1>-->
                <h1>THAY ĐỔI MẬT KHẨU</h1>
<!--                <script type = "text/javascript">
                    function fun() {
                        alert("This is an alert dialog box");
                    }
                    function fun1() {
                        alert("1");
                    }
                </script>   -->
                <c:if test="${mess!=null}">
                    <!--                    <script>
                                            alert("This is an alert dialog box");
                                        </script>-->
                    <p style="color:red">${mess}</p>
                    <!--                    <div class="error-text"></div>-->
                </c:if>     
                <c:if test="${mess1!=null}">
                    <p style="color:red">${mess1}</p>
                    <!--                    <div class="error-text"></div>-->
                </c:if> 
                <input type="password" class="password_form" name="cur_password" placeholder="Mật khẩu hiện tại" required/>
                <input type="password" class="password_form" name="new_password" placeholder="Mật khẩu mới" required/>
                <input type="password" class="password_form" name="re_password" placeholder="Nhập lại mật khẩu" required />
                <button class="btn-dangky" type="submit" style="text-transform: uppercase;">Lưu</button>
            </form>
        </div>
        <footer>
            <jsp:include page="Footer.jsp"></jsp:include>
        </footer>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>
        <script src="../js/script.js" defer></script>

    </body>

</html>

</html>

</html>
