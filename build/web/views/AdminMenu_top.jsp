<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- all the meta tags -->
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta name="csrf-token" content="tTXT53Dve8x0Vxi1Y81JSlQBsolCQyhVFDWCJqphCD" />
        <!-- all the css files -->
        <link rel="shortcut icon" href="/images/favicon.png">
        <!-- third party css -->
        <link href="/css/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <link href="/css/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
        <link href="/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="/css/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
        <link href="/css/select.bootstrap4.css" rel="stylesheet" type="text/css" />
        <link href="/css/summernote-bs4.css" rel="stylesheet" type="text/css" />
        <link href="/css/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <link href="/css/dropzone.css" rel="stylesheet" type="text/css" />
        <!-- third party css end -->


        <!--Drips icons-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dripicons/2.0.0/webfont.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dripicons/2.0.0/webfont.min.css" />

        <!--Material Design Icon-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/2.0.46/css/materialdesignicons.min.css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/2.0.46/css/materialdesignicons.css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/3.9.97/css/materialdesignicons.min.css" integrity="sha512-PhzMnIL3KJonoPVmEDTBYz7rxxne7E3Lc5NekqcT3nxSLRTN2h2bJKStWoy0RfS31Jd6nBguC32sL6iK1k2OXw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/3.9.97/css/materialdesignicons.css" integrity="sha512-rUWddgvLngNoYen5mwmKwYV4MR6YVFeOrgj4V3mx/0IcguSrE54SSIuPtUsYNBrLCsQxx/bBbcm3GWhXVJXhxA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <!--Summer note-->
        <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

        <!-- App css -->
        <link href="/css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="/css/backend/main.css" rel="stylesheet" type="text/css" />

        <!-- font awesome 5 -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.3.1/css/all.min.css" />
        <link href="/css/backend/fontawesome-all.min.css" rel="stylesheet" type="text/css" />
        <link href="/css/fontawesome-iconpicker.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="./css/bootstrap-tagsinput.css">

        <script src="/js/jquery-3.3.1.min.js" charset="utf-8"></script>
        <script src="/js/onDomChange.js"></script></head>
    <body data-layout="detached">
        <!-- HEADER -->

        <!-- Topbar Start -->
        <div class="navbar-custom topnav-navbar topnav-navbar-dark">
            <div class="container-fluid">
                <!-- LOGO -->
                <a href="/AdminDashboard" class="topnav-logo" style = "min-width: unset;">
                    <span class="topnav-logo-lg">
                        <img src="/images/logo-light.png" alt="" height="40">
                    </span>
                    <span class="topnav-logo-sm">
                        <img src="/images/logo-light-sm.png" alt="" height="40">
                    </span>
                </a>

                <ul class="list-unstyled topbar-right-menu float-right mb-0">
                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle arrow-none" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <i class="dripicons-view-apps noti-icon"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated dropdown-lg p-0 mt-5 border-top-0" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-278px, 70px, 0px);">

                            <div class="rounded-top py-3 border-bottom bg-primary">
                                <h4 class="text-center text-white">Quick actions</h4>
                            </div>

                            <div class="row row-paddingless" style="padding-left: 15px; padding-right: 15px;">
                                <!--begin:Item-->
                                <div class="col-6 p-0 border-bottom border-right">
                                    <a href="/AdminAddCourse" class="d-block text-center py-3 bg-hover-light">
                                        <i class="dripicons-archive text-20"></i>
                                        <span class="w-100 d-block text-muted">Thêm khóa học</span>
                                    </a>
                                </div>



                                <div class="col-6 p-0 border-right">
                                    <a href="/AdminAddUser" class="d-block text-center py-3 bg-hover-light" >
                                        <i class="dripicons-user text-20"></i>
                                        <span class="w-100 d-block text-muted">Thêm người dùng</span>
                                    </a>
                                </div>


                            </div>
                        </div>
                    </li>
                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle nav-user arrow-none mr-0" data-toggle="dropdown" id="topbar-userdrop"
                           href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="account-user-avatar">
                                <c:if test="${sessionScope.user.image == null || sessionScope.user.image.isEmpty()}">
                                    <img src="/images/placeholder.png" alt="user-image"  class="rounded-circle">
                                </c:if>
                                <c:if test="${sessionScope.user.image != null}">
                                    <img src="${sessionScope.user.image}" alt="user-image"  class="rounded-circle">
                                </c:if>
                            </span>
                            <span  style="color: #fff;">
                                <span class="account-user-name">${sessionScope.acc.fullname}</span>
                                <span class="account-position"> Admin </span>
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated topbar-dropdown-menu profile-dropdown"
                             aria-labelledby="topbar-userdrop">
                            <!-- item-->
                            <div class=" dropdown-header noti-title">
                                <h6 class="text-overflow m-0">Welcome!</h6>
                            </div>

                            <!-- Logout-->
                            <a href="/LogOutServlet" class="dropdown-item notify-item">
                                <i class="mdi mdi-logout mr-1"></i>
                                <span>Đăng xuất</span>
                            </a>

                        </div>
                    </li>
                </ul>
                <a class="button-menu-mobile disable-btn">
                    <div class="lines">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <div class="visit_website">
                    <h4 style="color: #fff; float: left;" class="d-none d-md-inline-block"></h4>
                    <a href="/Home" target="" class="btn btn-outline-light ml-3 d-none d-md-inline-block">Trang chủ</a>
                </div>
            </div>
        </div>
        <!-- end Topbar -->    <div class="container-fluid">
            <div class="wrapper">
                <!-- BEGIN CONTENT -->
                <!-- SIDEBAR -->
                <!-- ========== Left Sidebar Start ========== -->
                <div class="left-side-menu left-side-menu-detached">
                    <div class="leftbar-user">
                        <a href="javascript: void(0);">
                            <c:if test="${sessionScope.user.image == null || sessionScope.user.image.isEmpty()}">
                                <img src="/images/placeholder.png" alt="user-image" height="45" width="45 "class="rounded-circle shadow-sm">
                            </c:if>
                            <c:if test="${sessionScope.user.image != null}">
                                <img src="${sessionScope.user.image}" alt="user-image" height="45" width="45" class="rounded-circle shadow-sm">
                            </c:if>
                            <span class="leftbar-user-name">${sessionScope.user.fullname}</span>
                        </a>
                    </div>

                    <!--- Sidemenu -->
                    <ul class="metismenu side-nav side-nav-light">

                        <li class="side-nav-title side-nav-item">Navigation</li>

                        <li class="side-nav-item ">
                            <a href="/AdminDashboard" class="side-nav-link">
                                <i class="dripicons-view-apps"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>

                        <li class="side-nav-item ">
                            <a href="javascript: void(0);" class="side-nav-link ">
                                <i class="dripicons-archive"></i>
                                <span>Khóa học</span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="side-nav-second-level" aria-expanded="false">
                                <li class="">
                                    <a href="/AdminCourse">Quản lý khóa học</a>
                                </li>

                                <li class="">
                                    <a href="/AdminAddCourse">Thêm khóa học</a>
                                </li>
                                <!--
                                                                <li class="side-nav-item ">
                                                                    <a href="javascript: void(0);" class="side-nav-link ">
                                                                        <i class=""></i>
                                                                        <span> Subject Detail </span>
                                                                        <span class="menu-arrow"></span>
                                                                    </a>
                                                                    <ul style="margin-left:10% " class="side-nav-second-level" aria-expanded="false">
                                                                        <li class="">
                                                                            <a href="/AdminCourseDetail">Overview</a>
                                                                        </li>
                                                                        <li class="">
                                                                            <a href="/AdminSubjectDimension">Subject Dimension</a>
                                                                        </li>
                                                                    </ul>
                                                                </li>-->
                            </ul>
                        </li>


                        <li class="side-nav-item ">

                        </li>

                        <li class="side-nav-item">

                            <ul class="side-nav-second-level" aria-expanded="false">
                                <li class=""> <a href="http://localhost/admin/admin_revenue">Admin revenue</a> </li>
                                <li class="">
                                    <a href="http://localhost/admin/instructor_revenue">
                                        Instructor revenue							</a>
                                </li>
                            </ul>
                        </li>

                        <li class="side-nav-item ">
                            <a href="javascript: void(0);" class="side-nav-link ">
                                <i class="dripicons-box"></i>
                                <span>Người dùng</span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="side-nav-second-level" aria-expanded="false">
                                <li class="side-nav-item ">
                                    <a href="/AdminUserList">Danh sách người dùng</a>
                                </li>

                                <li class="side-nav-item ">
                                    <a href="/AdminAddUser">Thêm mới </a>
                                </li>
                            </ul>
                        </li>

                        <li class="side-nav-item">
                            <a href="javascript: void(0);" class="side-nav-link">
                                <i class="dripicons-blog"></i>
                                <span> Blog </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="side-nav-second-level" aria-expanded="false">
                                <li class="">
                                    <a href="/BlogController">Tất cả blogs</a>
                                </li>
                                <li class="">
                                    <a href="/AddBlog">Thêm blogs</a>
                                </li>
                                <li class="">
                                    <a href="/AdminBlogCategory">Thể loại blog</a>
                                </li>
                                <li class="">
                                    <a href="/AdminBlogCategortAdd">Thêm thể loại</a>
                                </li>   

                            </ul>
                        </li>
                    </ul>
                </div>   
