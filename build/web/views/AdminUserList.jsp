<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Quản lý người dùng</title>
        <!-- all the meta tags -->
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="shortcut icon" href="/images/favicon.png">
    </head>
    <body>
        <%@include file="AdminMenu_top.jsp" %>
        <!-- PAGE CONTAINER-->
        <div class="content-page">
            <div class="content">
                <!-- BEGIN PlACE PAGE CONTENT HERE -->
                <div class="row ">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> Quản lý người dùng
                                </h4>
                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div>
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="mb-3 header-title">Danh sách người dùng</h4>
                                <form class="row justify-content-center" action="/AdminUserList" method="post">
                                    <!-- User Gender -->
                                    <div class="col-xl-2">
                                        <div class="form-group">
                                            <label for="gender">Giới tính</label>
                                            <select class="form-control select2" data-toggle="select2" name="gender" id="gender">
                                                <option value="" selected>Tất cả</option>
                                                <option value="1" >Nam</option>
                                                <option value="0" >Nữ</option>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- User Role -->
                                    <div class="col-xl-3">
                                        <div class="form-group">
                                            <label for="role">Vai trò</label>
                                            <select class="form-control select2" data-toggle="select2" name="role" id='role'>
                                                <option value="" selected>Tất cả</option>
                                                <c:forEach items="${listRole}" var="r">
                                                    <option value="${r.role_id}">${r.name}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- User Status -->
                                    <div class="col-xl-3">
                                        <div class="form-group">
                                            <label for="status">Trạng thái</label>
                                            <select class="form-control select2" data-toggle="select2" name="status" id='status'>
                                                <option value="" selected>Tất cả</option>
                                                <option value="1" >Đang hoạt động</option>
                                                <option value="0" >Không hoạt động</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-xl-2">
                                        <label for=".." class="text-white">..</label>
                                        <button type="submit" class="btn btn-primary btn-block" name="button">Lọc</button>
                                    </div>
                                </form>

                                <div class="table-responsive-sm mt-4">
                                    <a href="/AdminAddUser" class="btn btn-outline-primary btn-rounded alignToTitle"><i class="mdi mdi-plus"></i>Thêm mới người dùng</a>
                                    <table style="text-align: center;" id="basic-datatable" class="table table-striped table-centered mb-0">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Họ và tên</th>
                                                <th>Giới tính</th>
                                                <th>Email</th>
                                                <th>Số điện thoại</th>
                                                <th>Vai trò</th>
                                                <th>Trạng thái</th>
                                                <th>Hành động</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${listUser}" var="u">
                                                <tr>
                                                    <td>${u.user_id}</td>
                                                    <td>${u.fullname}</td>
                                                    <c:if test="${u.gender}">
                                                        <td>Nam</td>
                                                    </c:if>
                                                    <c:if test="${!u.gender}">
                                                        <td>Nữ</td>
                                                    </c:if>
                                                    <td>${u.email}</td>
                                                    <td>${u.phone}</td>
                                                    <c:forEach items="${listRole}" var="r">
                                                        <c:if test="${u.role_id == r.role_id}">
                                                            <td>${r.name}</td>
                                                        </c:if>
                                                    </c:forEach>
                                                    <td>
                                                        <c:if test="${u.status == 0}"><button type="button" class="btn btn-secondary">Không hoạt động</button></c:if>
                                                        <c:if test="${u.status == 1}"><button type="button" class="btn btn-success">Đang hoạt động</button></c:if>                                                       
                                                    </td>
                                                        <td>
                                                            <div class="dropdown dropright">
                                                                <button type="button" class="btn btn-sm btn-outline-primary btn-rounded btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="mdi mdi-dots-vertical"></i>
                                                                </button>
                                                                <ul class="dropdown-menu dropdown-menu-right">
                                                                    <li><a class="btn btn-sm btn-outline-primary btn-rounded btn-icon" href="AdminUpdateUser?user_id=${u.user_id}">
                                                                        <i class="mdi mdi-dots-vertical">Chi tiết & Tùy chỉnh</i>
                                                                    </a></li>
                                                                <!--<li><a class="dropdown-item" href="AdminDeleteCourse?id=${o.id}" onclick="return confirm('Are you sure you want to delete this Course?">Xóa</a></li>-->
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>            
            </div>
        </div>
        <%@include file="AdminMenu_floor.jsp" %>
    </body>
</html>
