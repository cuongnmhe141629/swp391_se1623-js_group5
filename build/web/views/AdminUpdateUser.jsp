<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Chi tiết & Tùy chỉnh người dùng</title>
        <!-- all the meta tags -->
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="shortcut icon" href="/images/favicon.png">
    </head>
    <body>
        <%@include file="AdminMenu_top.jsp" %>
        <!-- PAGE CONTAINER-->
        <div class="content-page">
            <div class="content">
                <!-- BEGIN PlACE PAGE CONTENT HERE -->
                <div class="row">
                </div>
                <div class="row">
                    <form class="w-100 bg-white radius-10 p-4" action="/AdminUpdateUser?user_id=${user.user_id}" method="post">
                        <div class="row">
                            <div class="col-12 border-bottom mb-3 pb-3">
                                <div class="row">
                                    <div class="col-md-6"><h4>Thông tin người dùng</h4></div>
                                    <div class="col-md-6"><a href="/AdminUserList" class="alignToTitle btn btn-outline-secondary btn-rounded btn-sm my-1"> <i class=" mdi mdi-keyboard-backspace"></i>Trở lại danh sách người dùng</a></div> 
                                </div>
                                <div class="row">
                                    <div class="w-25 h-25 mx-auto" >
                                        <c:if test="${user.image == null || user.image.isEmpty()}">
                                            <img class="center mx-auto" style="border-radius: 50%; height: 118px; width:118px" src="/images/placeholder.png">
                                        </c:if>
                                        <c:if test="${user.image != null}">
                                            <img class="center mx-auto" style="border-radius: 50%; height: 118px; width:118px" src="${user.image}">
                                        </c:if>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-8">
                                <label class="text-dark fw-600" for="fullname">Họ và tên</label>
                                <div class="input-group">
                                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                                    <input type="text" class="form-control" name = "fullname" id="FristName" value="${user.fullname}" disabled>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label class="text-dark fw-600" for="gender">Giới tính</label>
                                <div class="input-group btn-group-toggle" data-toggle="buttons">
                                    <c:if test="${user.gender}">
                                        <label class="btn btn-light active">
                                            <input type="radio" name="gender" value="male" id="option2" autocomplete="off" disabled>Nam 
                                        </label>
                                        <label class="btn btn-light">
                                            <input type="radio" name="gender" value="female" id="option3" autocomplete="off" disabled> Nữ
                                        </label>
                                    </c:if>
                                    <c:if test="${!user.gender}">
                                        <label class="btn btn-light">
                                            <input type="radio" name="gender" value="male" id="option2" autocomplete="off" disabled>Nam 
                                        </label>
                                        <label class="btn btn-light active">
                                            <input type="radio" name="gender" value="female" id="option3" autocomplete="off" disabled> Nữ
                                        </label>
                                    </c:if>
                                </div>
                            </div>

                            <div class="col-12 mt-3">
                                <hr class="my-3 bg-secondary">
                                <label class="text-dark fw-600">Email</label>
                                <div class="input-group mb-3">
                                    <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                    <input type="text" class="form-control" maxlength="60" name = "email" placeholder="" value="${user.email}" disabled>
                                </div>

                                <label class="text-dark fw-600">Số điện thoại</label>
                                <div class="input-group mb-3">
                                    <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                    <input type="text" class="form-control" maxlength="60" name = "phone" placeholder="" value="${user.phone}" disabled>
                                </div>

                                <label class="text-dark fw-600">Vai trò</label>
                                <div class="col-mb-3">
                                    <select class="form-control select2" data-toggle="select2" name="role" id="role">                                               
                                        <c:forEach items="${listRole}" var="r">
                                            <option value="${r.role_id}" ${r.role_id==user.role_id?"selected":""}>${r.getName()}</option>
                                        </c:forEach>              
                                    </select>
                                </div>
                                <br>
                                <label class="text-dark fw-600">Trạng thái</label>
                                <div class="col-mb-3">
                                    <select class="form-control select2" data-toggle="select2" name="status" id="status">                                               
                                        <c:if test="${user.status == 1}">
                                            <option value="1" selected>Đang hoạt động</option>
                                            <option value="0" >Không hoạt động</option>                                                  
                                        </c:if>
                                        <c:if test="${user.status == 0}">
                                            <option value="1">Đang hoạt động</option>
                                            <option value="0" selected >Không hoạt động</option>                                                  
                                        </c:if>
                                    </select>
                                </div>
                                <br>
                            </div>

                            <div class="custom-control custom-checkbox col-12 pt-4">
                                <button type="submit" id="submit" class="btn btn-info offset-5">Lưu</button>
                            </div>

                        </div>
                </div>
                </form>

            </div>
            <script type="text/javascript">
                $('#unpaid-instructor-revenue').mouseenter(function () {
                    $('#go-to-instructor-revenue').show();
                });
                $('#unpaid-instructor-revenue').mouseleave(function () {
                    $('#go-to-instructor-revenue').hide();
                });
            </script>
            <!-- END PLACE PAGE CONTENT HERE -->
        </div>
    </div>
    <!-- END CONTENT -->
    <c:if test="${requestScope.success ne null}">
        <script type="text/javascript"> window.onload = function success() {
                alert("Cập nhật người dùng thành công!");
            };</script>
        </c:if>
        <%@include file="AdminMenu_floor.jsp" %>
</body>
</html>