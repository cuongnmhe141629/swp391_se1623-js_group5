<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="vn">
    <head>
        <title>Thêm mới </title>
        <!-- all the meta tags -->
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="shortcut icon" href="/images/favicon.png">
    </head>
    <body>
        <%@include file="AdminMenu_top.jsp" %>
        <!-- PAGE CONTAINER-->
        <div class="content-page">
            <div class="content">
                <!-- BEGIN PlACE PAGE CONTENT HERE -->
                <div class="row ">
                    <div class="col-xl-12">

                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6"><h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon">Thêm mới người dùng</i></h4></div>
                                    <div class="col-md-6"><a href="/AdminUserList" class="alignToTitle btn btn-outline-secondary btn-rounded btn-sm my-1"> <i class=" mdi mdi-keyboard-backspace"></i>Trở lại danh sách người dùng</a></div>                               
                                </div> <!-- end card body-->
                            </div>
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div>

                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="header-title my-1">Thêm mới người dùng</h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xl-12">
                                        <form class="required-form" action="/AdminAddUser" method="post">
                                            <div id="basicwizard">
                                                <ul class="nav nav-pills nav-justified form-wizard-header">
                                                    <li class="nav-item">
                                                        <a href="#basic" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                                            <i class="mdi mdi-fountain-pen-tip"></i>
                                                            <span class="d-none d-sm-inline">Thêm mới</span>
                                                        </a>
                                                    </li>

                                                    <li class="w-100 bg-white pb-3">
                                                        <!--ajax page loader-->
                                                        <div class="ajax_loader w-100">
                                                            <div class="ajax_loaderBar"></div>
                                                        </div>
                                                        <!--end ajax page loader-->
                                                    </li>
                                                </ul>

                                                <div class="tab-content b-0 mb-0">
                                                    <div class="tab-pane" id="basic">
                                                        <div class="row justify-content-center">
                                                            <div class="col-xl-8">
                                                                <input type="hidden" name = "user_type" value="general">
                                                                <div class="form-group row mb-3">
                                                                    <label class="col-md-3 col-form-label" for="fullname">Họ và tên: <span class="required">*</span> </label>
                                                                    <div class="col-md-9">
                                                                        <input type="text" class="form-control" id="fullname" name = "fullname" maxlength="50"required>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-3">
                                                                    <label class="col-md-3 col-form-label" for="fullname">Giới tính: <span class="required">*</span> </label>
                                                                    <div class="col-md-9 btn-group-toggle" data-toggle="buttons">
                                                                        <label class="btn btn-light active">
                                                                            <input type="radio" name="gender" id="option2" value="male" checked autocomplete="off">Nam 
                                                                        </label>
                                                                        <label class="btn btn-light">
                                                                            <input type="radio" name="gender" id="option3" value="female" autocomplete="off"> Nữ
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-3">
                                                                    <label class="col-md-3 col-form-label" for="email">Email: <span class="required">*</span> </label>
                                                                    <div class="col-md-9">
                                                                        <input type="email" class="form-control" id="email" name = "email" maxlength="30" placeholder="abc@xx.xx" required>
                                                                        <!--<div style="color: red;">${requestScope.error}</div>-->
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row mb-3">
                                                                    <label class="col-md-3 col-form-label" for="phone">Số điện thoại: <span class="required">*</span> </label>
                                                                    <div class="col-md-9">
                                                                        <input type="text" class="form-control" id="phone" name = "phone" maxlength="30" placeholder="0xxxxxxxx" pattern="^[0]\d{8,11}$" title="Please enter correct format phone number!" required>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-3">
                                                                    <label class="col-md-3 col-form-label" for="role">Vai trò: <span class="required">*</span> </label>
                                                                    <div class="col-md-9">
                                                                        <select class="form-control select2" data-toggle="select2" name="role" id='role'>
                                                                            <c:forEach items="${listRole}" var="r">
                                                                                <option value="${r.role_id}">${r.name}</option>
                                                                            </c:forEach>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-3">
                                                                    <label class="col-md-3 col-form-label" for="status">Trạng thái: <span class="required">*</span> </label>
                                                                    <div class="col-md-9">
                                                                        <select class="form-control select2" data-toggle="select2" name="status" id='status'>
                                                                            <option value="1" >Đang hoạt động</option>
                                                                            <option value="0" >Không hoạt động</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-3">
                                                                    <label class="col-md-3 col-form-label" for="adress">Địa chỉ: </label>
                                                                    <div class="col-md-9">
                                                                        <input type="text" class="form-control" id="address" name = "address" maxlength="200" >
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-3">
                                                                    <div class="offset-md-2 col-md-10">
                                                                        <div class="custom-control custom-checkbox">
                                                                            <button type="submit" id="submit" class="btn btn-info offset-5">Lưu</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div> <!-- end col -->
                                                        </div> <!-- end row -->
                                                    </div> <!-- end tab pane -->
                                                </div> <!-- tab-content -->
                                            </div> <!-- end-->
                                        </form>
                                    </div>
                                </div><!-- end row-->
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div>
                </div>
             
                <script type="text/javascript">
                    var blank_outcome = jQuery('#blank_outcome_field').html();
                    var blank_requirement = jQuery('#blank_requirement_field').html();
                    jQuery(document).ready(function () {
                        jQuery('#blank_outcome_field').hide();
                        jQuery('#blank_requirement_field').hide();
                    });
                    function appendOutcome() {
                        jQuery('#outcomes_area').append(blank_outcome);
                    }
                    function removeOutcome(outcomeElem) {
                        jQuery(outcomeElem).parent().parent().remove();
                    }

                    function appendRequirement() {
                        jQuery('#requirement_area').append(blank_requirement);
                    }
                    function removeRequirement(requirementElem) {
                        jQuery(requirementElem).parent().parent().remove();
                    }

                    function priceChecked(elem) {
                        if (jQuery('#discountCheckbox').is(':checked')) {

                            jQuery('#discountCheckbox').prop("checked", false);
                        } else {

                            jQuery('#discountCheckbox').prop("checked", true);
                        }
                    }
                </script>

                <style media="screen">
                    body {
                        overflow-x: hidden;
                    }
                </style>
                <!-- END PLACE PAGE CONTENT HERE -->
            </div>
        </div>
        <c:if test="${requestScope.error ne null}">
            <script type="text/javascript"> window.onload = function error() {
                    alert("Email đã tồn tại. Vui lòng nhập lại!");
                };</script>
            </c:if>
            <c:if test="${requestScope.success ne null}">
            <script type="text/javascript"> window.onload = function success() {
                    alert("Thêm mới người dùng thành công!");
                };</script>
            </c:if>
        <!-- END CONTENT -->
        <%@include file="AdminMenu_floor.jsp" %>
    </body>
</html>