<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Quản lý khóa học</title>
        <!-- all the meta tags -->
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="shortcut icon" href="/images/favicon.png">
    </head>
    <body>
        <%@include file="AdminMenu_top.jsp" %>
        <!-- PAGE CONTAINER-->
        <div class="content-page">
            <div class="content">
                <!-- BEGIN PlACE PAGE CONTENT HERE -->
                <div class="row ">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> Quản lý khóa học
                                </h4>
                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card widget-inline">
                            <div class="card-body p-0">
                                <div class="row no-gutters">
                                    <div class="col-sm-6 col-xl-3">
                                        <a href="http://localhost/admin/courses" class="text-secondary">
                                            <div class="card shadow-none m-0">
                                                <div class="card-body text-center">
                                                    <i class="dripicons-link text-muted" style="font-size: 24px;"></i>
                                                    <h3><span>0</span></h3>
                                                    <p class="text-muted font-15 mb-0">Khóa học đang hoạt động</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="col-sm-6 col-xl-3">
                                        <a href="http://localhost/admin/courses" class="text-secondary">
                                            <div class="card shadow-none m-0 border-left">
                                                <div class="card-body text-center">
                                                    <i class="dripicons-link-broken text-muted" style="font-size: 24px;"></i>
                                                    <h3><span>0</span></h3>
                                                    <p class="text-muted font-15 mb-0">Khóa học đang hoạt động</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="col-sm-6 col-xl-3">
                                        <a href="http://localhost/admin/courses" class="text-secondary">
                                            <div class="card shadow-none m-0 border-left">
                                                <div class="card-body text-center">
                                                    <i class="dripicons-star text-muted" style="font-size: 24px;"></i>
                                                    <h3><span>0</span></h3>
                                                    <p class="text-muted font-15 mb-0">Khóa học miễn phí</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="col-sm-6 col-xl-3">
                                        <a href="http://localhost/admin/courses" class="text-secondary">
                                            <div class="card shadow-none m-0 border-left">
                                                <div class="card-body text-center">
                                                    <i class="dripicons-tags text-muted" style="font-size: 24px;"></i>
                                                    <h3><span>0</span></h3>
                                                    <p class="text-muted font-15 mb-0">Khóa học miễn phí</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                </div> <!-- end row -->
                            </div>
                        </div> <!-- end card-box-->
                    </div> <!-- end col-->
                </div>
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="mb-3 header-title">Danh sách khóa học</h4>
                                <form class="row justify-content-center" action="http://localhost/admin/courses" method="get">
                                    <!-- Course Categories -->
                                    <div class="col-xl-3">
                                        <div class="form-group">
                                            <label for="category_id">Thể loại</label>
                                            <select class="form-control select2" data-toggle="select2" name="category_id" id="category_id">
                                                <option value="all" selected>All</option>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Course Status -->
                                    <div class="col-xl-2">
                                        <div class="form-group">
                                            <label for="status">Trạng thái</label>
                                            <select class="form-control select2" data-toggle="select2" name="status" id='status'>
                                                <option value="all" selected>All</option>
                                                <option value="active" >Active</option>
                                                <option value="pending" >Pending</option>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Course Instructors -->
                                    <div class="col-xl-3">
                                        <div class="form-group">
                                            <label for="instructor_id">Giảng viên</label>
                                            <select class="form-control select2" data-toggle="select2" name="instructor_id" id='instructor_id'>
                                                <option value="all" selected>All</option>
                                                <option value="1" >Tien Nguyen</option>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Course Price -->
                                    <div class="col-xl-2">
                                        <div class="form-group">
                                            <label for="price">Giá</label>
                                            <select class="form-control select2" data-toggle="select2" name="price" id='price'>
                                                <option value="all" selected>All</option>
                                                <option value="free" >Free</option>
                                                <option value="paid" >Paid</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-xl-2">
                                        <label for=".." class="text-white">..</label>
                                        <button type="submit" class="btn btn-primary btn-block" name="button">Filter</button>
                                    </div>


                                </form>
                                <div class="table-responsive-sm mt-4">
                                    <a href="/AdminAddCourse" class="btn btn-outline-primary btn-rounded alignToTitle"><i class="mdi mdi-plus"></i>Thêm mới khóa học</a>
                                    <table style="text-align: center;" id="basic-datatable" class="table table-striped table-centered mb-0">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Tên khóa học</th>
                                                <th>Thể loại</th>
                                                <th>Số lượng bài giảng</th>
                                                <th>Người tạo</th>
                                                <th>Trạng thái</th>
                                                <th>Hành động</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${listCourse}" var="o">
                                                <tr>
                                                    <td>${o.id}</td>
                                                    <td>${o.title}</td>
                                                    <c:forEach items="${listCategory}" var="x">
                                                        <c:if test="${o.category_id == x.id}">
                                                            <td>${x.name}</td>
                                                        </c:if>
                                                    </c:forEach>
                                                    <td>${o.lesson_count}</td>
                                                    <td>${o.owner}</td>
                                                    <td>
                                                        <c:if test="${!o.status}">Unpublished</c:if>
                                                        <c:if test="${o.status}">Published</c:if>                                                       
                                                        </td>
                                                        <td>
                                                            <div class="dropdown dropright">
                                                                <button type="button" class="btn btn-sm btn-outline-primary btn-rounded btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="mdi mdi-dots-vertical"></i>
                                                                </button>
                                                                <ul class="dropdown-menu">
                                                                    <li><a class="btn btn-sm btn-outline-primary btn-rounded btn-icon" href="AdminCourseDetail?id=${o.id}">
                                                                        <i class="mdi mdi-dots-vertical">Tùy chỉnh khóa học</i>
                                                                    </a>
                                                                    <a class="btn btn-sm btn-outline-primary btn-rounded btn-icon" href="AdminLessonServlet?id=${o.id}">
                                                                        <i class="mdi mdi-dots-vertical">Tùy chỉnh bài giảng</i>
                                                                    </a>
                                                                </li>
                                                                <li><a class="dropdown-item" href="AdminDeleteCourse?id=${o.id}" onclick="return confirm('Are you sure you want to delete this Course?')">Xóa</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        <%@include file="AdminMenu_floor.jsp" %>
    </body>
</html>
