<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Chi tiết khóa học</title>
        <!-- all the meta tags -->
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="shortcut icon" href="/images/favicon.png">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    </head>
    <body>
        <%@include file="AdminMenu_top.jsp" %>

        <!-- PAGE CONTAINER-->
        <div class="content-page">
            <div class="content">
                <!-- BEGIN PlACE PAGE CONTENT HERE -->

                <div class="row ">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6"><h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> Chi tiết khóa học</h4></div>
                                    <div class="col-md-6"><a href="/AdminCourse" class="alignToTitle btn btn-outline-secondary btn-rounded btn-sm my-1"> <i class=" mdi mdi-keyboard-backspace"></i> Trở lại danh sách khóa học </a></div>                               
                                </div> <!-- end card body-->
                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div>

                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <nav>
                                    <div class="nav nav-pills nav-fill" id="nav-tab" role="tablist">
                                        <a class="nav-link "  href="/AdminCourseDetail?id=${id}">Tổng quan</a>
                                        <a class="nav-link "  href="/AdminSubjectDimension?id=${id}">Subject Dimension</a>
                                        <a class="nav-link active" href="/AdminPricePackage?id=${id}">Các gói giá</a>
                                    </div>
                                </nav>
                                <div class="tab-content py-4 ">
                                    <div class="tab-pane fade show active" id="step2">
                                        <!-- PAGE CONTAINER-->
                                        <h4 class="mb-3 header-title">Gói giá</h4> 
                                        <div class="table-responsive-sm mt-4">
                                            <a href="AdminAddPricePackage?id=${id}" class="btn btn-outline-primary btn-rounded alignToTitle"><i class="mdi mdi-plus"></i>Thêm mới gói giá </a>
                                            <table  style="text-align: center" id="basic-datatable" class="table table-striped" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>ID</th><th>Name</th><th>access_duration</th><th>status</th><th>list_price</th><th>sale_price</th><th>description</th>
                                                        <th colspan="1"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${prices}" var="p">
                                                        <tr>
                                                            <td>${p.id}</td>
                                                            <td>${p.name}</td>
                                                            <td>${p.access_duration}month</td>
                                                            <td>
                                                                <input type="checkbox" checked="${p.status==true?"true":""}" onclick="return false;">
                                                            </td>
                                                            <td>${p.list_price}$</td>
                                                            <td>${p.sale_price}$</td>
                                                            <td>${p.description}</td>
                                                            <td><a href="AdminUpdatePricePackage?pid=${p.id}&id=${p.course_id}">update</a></td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--JavaScript and dependencies--> 
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"></script>
            <!--JavaScript for validations only--> 
            <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script> 
            <script src="/js/enchanter.js"></script>
            <script>
                                        var registrationForm = $('#registration');
                                        var formValidate = registrationForm.validate({
                                            errorClass: 'is-invalid',
                                            errorPlacement: () => false
                                        });

                                        const wizard = new Enchanter('registration', {}, {
                                            onNext: () => {
                                                if (!registrationForm.valid()) {
                                                    formValidate.focusInvalid();
                                                    return false;
                                                }
                                            }
                                        });
            </script>
            <%@include file="AdminMenu_floor.jsp" %>
    </body>
</html>