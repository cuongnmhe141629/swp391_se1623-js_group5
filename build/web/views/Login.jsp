<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Đăng nhập</title>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css" />
        <link href="../css/unicons/css/unicons.css" type="text/css" rel="stylesheet" />
        <link href="../css/semantic/semantic.min.css" type="text/css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
        <!-- Difference CSS  -->
        <link rel="stylesheet" type="text/css" href="../css/home.css" />
        <link rel="stylesheet" type="text/css" href="../css/demo.css" />
        <meta name="csrf-token" content="W2gijDDalPkcKeO8wOWv8hwpSZv2sdNWEUsfphLs" />
    </head>
    <link rel="stylesheet" type="text/css" href="../css/login.css" />

    <body>
        <header>
            <jsp:include page="Header.jsp"></jsp:include>
            </header>
            <div class="form-ctn">
                <form action="../Login" method="post">
                    <input type="hidden" name="_token" value="W2gijDDalPkcKeO8wOWv8hwpSZv2sdNWEUsfphLs">
                    <h1>ĐĂNG NHẬP</h1>
                    <p class="text-14px" style="color:red">${requestScope.mess}</p>
                <input type="text" id="username" name="email" placeholder="Email" />
                <input type="password" class="password_form" name="password" placeholder="Mật khẩu" />
                <button class="btn-dangnhap" type="submit" style="text-transform: uppercase;">ĐĂNG NHẬP</button>
                <a href="/Register">Chưa có tài khoản?</a>
                <a class="forgot-pass" href="/ForgotPasswordServlet" data-language="70">Quên mật khẩu?</a>
            </form>
        </div>
        <footer>
            <jsp:include page="Footer.jsp"></jsp:include>
        </footer>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>
        <!-- <script src="css/login/includeHTML.js"></script>
        <script>
            includeHTML();
        </script> -->

    </body>

</html>