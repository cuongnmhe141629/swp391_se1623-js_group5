package org.apache.jsp.views;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class MyCouser_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(1);
    _jspx_dependants.add("/views/AdminMenu_floor.jsp");
  }

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_var_items;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_forEach_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_forEach_var_items.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"> \n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\" />\n");
      out.write("        <title>Khóa học đã mua</title>\n");
      out.write("        <link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css\" rel=\"stylesheet\"\n");
      out.write("              crossorigin=\"anonymous\" />\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\"\n");
      out.write("              href=\"https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css\" />\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\"\n");
      out.write("              href=\"https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css\" />\n");
      out.write("        <link href=\"/css/unicons/css/unicons.css\" type=\"text/css\"\n");
      out.write("              rel=\"stylesheet\" />\n");
      out.write("        <link href=\"/css/semantic/semantic.min.css\" type=\"text/css\"\n");
      out.write("              rel=\"stylesheet\" />\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css\" />\n");
      out.write("        <!-- Difference CSS  -->\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/home.css\" />\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/demo.css\" />\n");
      out.write("        <meta name=\"csrf-token\" content=\"tTXT53Dve8x0Vxi1Y81JSlQBsolCQyhVFDWCJqph\" />\n");
      out.write("             \n");
      out.write("    </head>\n");
      out.write("\n");
      out.write("    <body class=\"tcnvcd\">\n");
      out.write("        <header>\n");
      out.write("          \n");
      out.write("            ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "Header.jsp", out, false);
      out.write("\n");
      out.write("            </header>\n");
      out.write("          \n");
      out.write("            <div class=\"tcnvcd breadcrumb-ctn\">\n");
      out.write("\n");
      out.write("            </div>\n");
      out.write("            <section class=\"tcnvcd ctn-sn ctn-sn-1\">\n");
      out.write("                <div class=\"tcnvcd nav nav-tabs\" id=\"nav-tab\" role=\"tablist\">\n");
      out.write("                    <a style=\"text-align: center\" class=\"tcnvcd nav-link active\" id=\"nav-hscn-tab\" data-bs-target=\"#hscn\" role=\"tab\"\n");
      out.write("                       href=\"/UserProfile\">\n");
      out.write("                        HỒ SƠ CÁ NHÂN\n");
      out.write("                    </a>\n");
      out.write("                    <a style=\"text-align: center\" class=\"tcnvcd nav-link active\" id=\"nav-khdm-tab\" data-bs-target=\"hscn\" role=\"tab\"\n");
      out.write("                       href=\"/mycourse\">\n");
      out.write("                        KHÓA HỌC ĐÃ MUA\n");
      out.write("                    </a>\n");
      out.write("                    <a style=\"text-align: center\" class=\"tcnvcd nav-link\" id=\"nav-khdm-tab\" data-bs-target=\"#khdm\" role=\"tab\"\n");
      out.write("                       href=\"#\">\n");
      out.write("                        KHÓA HỌC YÊU THÍCH\n");
      out.write("                    </a>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"tcnvcd tab-pane fade show active\" id=\"hscn\" role=\"tabpanel\" aria-labelledby=\"nav-hscn-tab\"> \n");
      out.write("                    <form action=\"/mycourse\" method=\"post\" enctype=\"multipart/form-data\">   \n");
      out.write("                        <input hidden=\"true\" type=\"email\" name=\"email\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.email}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"/>\n");
      out.write("                    <input type=\"hidden\" name=\"_token\" value=\"tTXT53Dve8x0Vxi1Y81JSlQBsolCQyhVFDWCJqph\">\n");
      out.write("                    <div class=\"row\">\n");
      out.write("                        <div class=\"col-xl-12\">\n");
      out.write("                            <div class=\"card\">\n");
      out.write("                                <div class=\"card-body\">\n");
      out.write("                                    <h4 class=\"mb-3 header-title\">Danh sách khóa học</h4>\n");
      out.write("\n");
      out.write("                                    <div class=\"table-responsive-sm mt-4\">\n");
      out.write("                                        <a href=\" #\" class=\"btn btn-outline-primary btn-rounded alignToTitle\"><i class=\"mdi mdi-plus\"></i>Thêm mới</a>\n");
      out.write("                                        \n");
      out.write("                                        <table style=\"text-align: center;\" id=\"basic-datatable\" class=\"table table-striped table-centered mb-0\">\n");
      out.write("                                            <thead>\n");
      out.write("                                                <tr>\n");
      out.write("                                                    <th>#</th>\n");
      out.write("                                                    <th>Tên khóa học</th>\n");
      out.write("                                                    <th>Thể loại</th>   \n");
      out.write("                                                 \n");
      out.write("                                                    <th></th>\n");
      out.write("                                                </tr>\n");
      out.write("                                            </thead>\n");
      out.write("                                            <tbody>\n");
      out.write("                                                ");
      if (_jspx_meth_c_forEach_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("                                            </tbody>\n");
      out.write("                                        </table>\n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div> \n");
      out.write("                </form>\n");
      out.write("            </div>\n");
      out.write("        </section>\n");
      out.write("        <footer>\n");
      out.write("            ");
      out.write("\n");
      out.write("</div>\n");
      out.write("</div>\n");
      out.write("<!-- all the js files -->\n");
      out.write("<!-- bundle -->\n");
      out.write("<script src=\"/js/backend/app.min.js\"></script>\n");
      out.write("\n");
      out.write("<!-- third party js -->\n");
      out.write("<script src=\"/js/vendor/Chart.bundle.min.js\"></script>\n");
      out.write("<script src=\"/js/vendor/jquery-jvectormap-1.2.2.min.js\"></script>\n");
      out.write("<script src=\"/js/vendor/jquery-jvectormap-world-mill-en.js\"></script>\n");
      out.write("<script src=\"/js/vendor/jquery.dataTables.min.js\"></script>\n");
      out.write("<script src=\"/js/vendor/dataTables.bootstrap4.js\"></script>\n");
      out.write("<script src=\"/js/vendor/dataTables.responsive.min.js\"></script>\n");
      out.write("<script src=\"/js/vendor/responsive.bootstrap4.min.js\"></script>\n");
      out.write("<script src=\"/js/vendor/dataTables.buttons.min.js\"></script>\n");
      out.write("<script src=\"/js/vendor/buttons.bootstrap4.min.js\"></script>\n");
      out.write("<script src=\"/js/vendor/buttons.html5.min.js\"></script>\n");
      out.write("<script src=\"/js/vendor/buttons.flash.min.js\"></script>\n");
      out.write("<script src=\"/js/vendor/buttons.print.min.js\"></script>\n");
      out.write("<script src=\"/js/vendor/dataTables.keyTable.min.js\"></script>\n");
      out.write("<script src=\"/js/vendor/dataTables.select.min.js\"></script>\n");
      out.write("<script src=\"/js/vendor/summernote-bs4.min.js\"></script>\n");
      out.write("<script src=\"/js/vendor/fullcalendar.min.js\"></script>\n");
      out.write("<script src=\"/js/page/demo.summernote.js\"></script>\n");
      out.write("<script src=\"/js/vendor/dropzone.js\"></script>\n");
      out.write("<script src=\"/js/page/datatable-initializer.js\"></script>\n");
      out.write("<script src=\"/js/backend/font-awesome-icon-picker/fontawesome-iconpicker.min.js\" charset=\"utf-8\"></script>\n");
      out.write("<script src=\"/js/vendor/bootstrap-tagsinput.min.js\" charset=\"utf-8\"></script>\n");
      out.write("<script src=\"/js/backend/bootstrap-tagsinput.min.js\"></script>\n");
      out.write("<script src=\"/js/vendor/dropzone.min.js\" charset=\"utf-8\"></script>\n");
      out.write("<script src=\"/js/ui/component.fileupload.js\" charset=\"utf-8\"></script>\n");
      out.write("<script src=\"/js/page/demo.form-wizard.js\"></script>\n");
      out.write("<!-- dragula js-->\n");
      out.write("<script src=\"/js/vendor/dragula.min.js\"></script>\n");
      out.write("<!-- component js -->\n");
      out.write("<script src=\"/js/backend/component.dragula.js\"></script>\n");
      out.write("\n");
      out.write("<script src=\"/js/backend/custom.js\"></script>\n");
      out.write("\n");
      out.write("<!-- Dashboard chart's data is coming from this file -->\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<script type=\"text/javascript\">\n");
      out.write("    $(document).ready(function () {\n");
      out.write("        $(function () {\n");
      out.write("            $('.icon-picker').iconpicker();\n");
      out.write("        });\n");
      out.write("    });\n");
      out.write("</script>\n");
      out.write("\n");
      out.write("<!-- Toastr and alert notifications scripts -->\n");
      out.write("<script type=\"text/javascript\">\n");
      out.write("    function notify(message) {\n");
      out.write("        $.NotificationApp.send(\"Heads up!\", message, \"top-right\", \"rgba(0,0,0,0.2)\", \"info\");\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    function success_notify(message) {\n");
      out.write("        $.NotificationApp.send(\"Congratulations!\", message, \"top-right\", \"rgba(0,0,0,0.2)\", \"success\");\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    function error_notify(message) {\n");
      out.write("        $.NotificationApp.send(\"Oh snap!\", message, \"top-right\", \"rgba(0,0,0,0.2)\", \"error\");\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    function error_required_field() {\n");
      out.write("        $.NotificationApp.send(\"Oh snap!\", \"Please fill all the required fields\", \"top-right\", \"rgba(0,0,0,0.2)\", \"error\");\n");
      out.write("    }\n");
      out.write("</script>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<script type=\"text/javascript\">\n");
      out.write("    function showAjaxModal(url, header)\n");
      out.write("    {\n");
      out.write("        // SHOWING AJAX PRELOADER IMAGE\n");
      out.write("        jQuery('#scrollable-modal .modal-body').html('<div style=\"width: 100px; height: 100px; line-height: 100px; padding: 0px; text-align: center; margin-left: auto; margin-right: auto;\"><div class=\"spinner-border text-secondary\" role=\"status\"></div></div>');\n");
      out.write("        jQuery('#scrollable-modal .modal-title').html('loading...');\n");
      out.write("        // LOADING THE AJAX MODAL\n");
      out.write("        jQuery('#scrollable-modal').modal('show', {backdrop: 'true'});\n");
      out.write("\n");
      out.write("        // SHOW AJAX RESPONSE ON REQUEST SUCCESS\n");
      out.write("        $.ajax({\n");
      out.write("            url: url,\n");
      out.write("            success: function (response)\n");
      out.write("            {\n");
      out.write("                jQuery('#scrollable-modal .modal-body').html(response);\n");
      out.write("                jQuery('#scrollable-modal .modal-title').html(header);\n");
      out.write("            }\n");
      out.write("        });\n");
      out.write("    }\n");
      out.write("    function showLargeModal(url, header)\n");
      out.write("    {\n");
      out.write("        // SHOWING AJAX PRELOADER IMAGE\n");
      out.write("        jQuery('#large-modal .modal-body').html('<div style=\"width: 100px; height: 100px; line-height: 100px; padding: 0px; text-align: center; margin-left: auto; margin-right: auto;\"><div class=\"spinner-border text-secondary\" role=\"status\"></div></div>');\n");
      out.write("        jQuery('#large-modal .modal-title').html('...');\n");
      out.write("        // LOADING THE AJAX MODAL\n");
      out.write("        jQuery('#large-modal').modal('show', {backdrop: 'true'});\n");
      out.write("\n");
      out.write("        // SHOW AJAX RESPONSE ON REQUEST SUCCESS\n");
      out.write("        $.ajax({\n");
      out.write("            url: url,\n");
      out.write("            success: function (response)\n");
      out.write("            {\n");
      out.write("                jQuery('#large-modal .modal-body').html(response);\n");
      out.write("                jQuery('#large-modal .modal-title').html(header);\n");
      out.write("            }\n");
      out.write("        });\n");
      out.write("    }\n");
      out.write("</script>\n");
      out.write("\n");
      out.write("<!-- (Large Modal)-->\n");
      out.write("<div class=\"modal fade\" id=\"large-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\n");
      out.write("    <div class=\"modal-dialog modal-lg\">\n");
      out.write("        <div class=\"modal-content\">\n");
      out.write("            <div class=\"modal-header\">\n");
      out.write("                <h4 class=\"modal-title\" id=\"myLargeModalLabel\">Large modal</h4>\n");
      out.write("                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"modal-body\">\n");
      out.write("                ...\n");
      out.write("            </div>\n");
      out.write("        </div><!-- /.modal-content -->\n");
      out.write("    </div><!-- /.modal-dialog -->\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<!-- Scrollable modal -->\n");
      out.write("<div class=\"modal fade\" id=\"scrollable-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"scrollableModalTitle\" aria-hidden=\"true\">\n");
      out.write("    <div class=\"modal-dialog modal-dialog-scrollable\" role=\"document\">\n");
      out.write("        <div class=\"modal-content\">\n");
      out.write("            <div class=\"modal-header\">\n");
      out.write("                <h5 class=\"modal-title\" id=\"scrollableModalTitle\">Modal title</h5>\n");
      out.write("                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n");
      out.write("                    <span aria-hidden=\"true\">&times;</span>\n");
      out.write("                </button>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"modal-body ml-2 mr-2\">\n");
      out.write("\n");
      out.write("            </div>\n");
      out.write("            <div class=\"modal-footer\">\n");
      out.write("                <button class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n");
      out.write("            </div>\n");
      out.write("        </div><!-- /.modal-content -->\n");
      out.write("    </div><!-- /.modal-dialog -->\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<script type=\"text/javascript\">\n");
      out.write("    function confirm_modal(delete_url)\n");
      out.write("    {\n");
      out.write("        jQuery('#alert-modal').modal('show', {backdrop: 'static'});\n");
      out.write("        document.getElementById('update_link').setAttribute('href', delete_url);\n");
      out.write("    }\n");
      out.write("</script>\n");
      out.write("\n");
      out.write("<!-- Info Alert Modal -->\n");
      out.write("<div id=\"alert-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n");
      out.write("    <div class=\"modal-dialog modal-sm\">\n");
      out.write("        <div class=\"modal-content\">\n");
      out.write("            <div class=\"modal-body p-4\">\n");
      out.write("                <div class=\"text-center\">\n");
      out.write("                    <i class=\"dripicons-information h1 text-info\"></i>\n");
      out.write("                    <h4 class=\"mt-2\">Heads up!</h4>\n");
      out.write("                    <p class=\"mt-3\">Are you sure?</p>\n");
      out.write("                    <button type=\"button\" class=\"btn btn-info my-2\" data-dismiss=\"modal\">Cancel</button>\n");
      out.write("                    <a href=\"#\" id=\"update_link\" class=\"btn btn-danger my-2\">Continue</a>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div><!-- /.modal-content -->\n");
      out.write("    </div><!-- /.modal-dialog -->\n");
      out.write("</div><!-- /.modal -->\n");
      out.write("\n");
      out.write("<script type=\"text/javascript\">\n");
      out.write("\n");
      out.write("    function ajax_confirm_modal(delete_url, elem_id)\n");
      out.write("    {\n");
      out.write("        jQuery('#ajax-alert-modal').modal('show', {backdrop: 'static'});\n");
      out.write("        $(\"#appent_link_a\").bind(\"click\", function () {\n");
      out.write("            delete_by_ajax_calling(delete_url, elem_id);\n");
      out.write("        });\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    function delete_by_ajax_calling(delete_url, elem_id) {\n");
      out.write("        $.ajax({\n");
      out.write("            url: delete_url,\n");
      out.write("            success: function (response) {\n");
      out.write("                var response = JSON.parse(response);\n");
      out.write("                if (response.status == 'success') {\n");
      out.write("                    $('#' + elem_id).fadeOut(600);\n");
      out.write("                    $.NotificationApp.send(\"Success!\", response.message, \"top-right\", \"rgba(0,0,0,0.2)\", \"success\");\n");
      out.write("                } else {\n");
      out.write("                    $.NotificationApp.send(\"Oh snap!\", response.message, \"top-right\", \"rgba(0,0,0,0.2)\", \"error\");\n");
      out.write("                }\n");
      out.write("            }\n");
      out.write("        });\n");
      out.write("    }\n");
      out.write("</script>\n");
      out.write("\n");
      out.write("<!-- Info Alert Modal -->\n");
      out.write("<div id=\"ajax-alert-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n");
      out.write("    <div class=\"modal-dialog modal-sm\">\n");
      out.write("        <div class=\"modal-content\">\n");
      out.write("            <div class=\"modal-body p-4\">\n");
      out.write("                <div class=\"text-center\" id=\"appent_link\">\n");
      out.write("                    <i class=\"dripicons-information h1 text-info\"></i>\n");
      out.write("                    <h4 class=\"mt-2\">Heads up!</h4>\n");
      out.write("                    <p class=\"mt-3\">Are you sure?</p>\n");
      out.write("                    <button type=\"button\" class=\"btn btn-info my-2\" data-dismiss=\"modal\">Cancel</button>\n");
      out.write("                    <a id=\"appent_link_a\" href=\"javascript:;\" class=\"btn btn-danger my-2\" data-dismiss=\"modal\">Continue</a>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div><!-- /.modal-content -->\n");
      out.write("    </div><!-- /.modal-dialog -->\n");
      out.write("</div><!-- /.modal -->  \n");
      out.write("<!-- Our script! :) -->\n");
      out.write("\n");
      out.write("</body>\n");
      out.write("</html>");
      out.write("\n");
      out.write("        </footer>                \n");
      out.write("    </body>\n");
      out.write("\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_forEach_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_0.setParent(null);
    _jspx_th_c_forEach_0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${listCourseOfUser}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_0.setVar("u");
    int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
      if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("                                                    <tr>\n");
          out.write("                                                        <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${u.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("                                                        <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${u.title}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("\n");
          out.write("                                                        <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${u.category_name }", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("                                                       \n");
          out.write("                                                        <td>\n");
          out.write("                                                            <div class=\"dropright dropright\">\n");
          out.write("                                                                <a class=\"btn btn-outline-primary btn-rounded\" href=\"/views/LessionViewUser.jsp\"> \n");
          out.write("                                                                    Khóa Học\n");
          out.write("                                                                    <i class=\"bi bi-book\" ></i>\n");
          out.write("                                                                </a>\n");
          out.write("                                                                    \n");
          out.write("<!--                                                                    viết servlet cho lesson như thế này là bấm vào sẽ ra lesson-->\n");
          out.write("                                                                <button type=\"button\" class=\"btn btn-outline-primary btn-rounded\"  onclick=\"doDelete\"  >\n");
          out.write("                                                                    \n");
          out.write("                                                                    Xóa khóa học\n");
          out.write("                                                                    <i class=\"bi bi-trash\" onclick=\"doDelete\"  href=\"/DeleteMycouse=");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${u.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\"  ></i>\n");
          out.write("                                                                     <li><a class=\"dropdown-item\" href=\"DeleteMycouse?id=");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${u.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\" onclick=\"#\">Delete</a></li>\n");
          out.write("\n");
          out.write("                                                                </button>\n");
          out.write("\n");
          out.write("                                                               <script type=\"text/javascript\">\n");
          out.write("                                                                      function doDelete(course_id){\n");
          out.write("//                                                                            $ajax().del(mycourse,\n");
          out.write("//                                                                                 {course_id:u.id });       \n");
          out.write("                                                                                  $.ajax({\n");
          out.write("                                                                              url: '/mycourse?course_id=");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${u.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write(" ',\n");
          out.write("                                                                                 type: 'DELETE',\n");
          out.write("                                                                               success: function(result) {\n");
          out.write("                                                                               // Do something with the result\n");
          out.write("                                                                                       }\n");
          out.write("                                                                                   });\n");
          out.write("                                                                   }\n");
          out.write("                                                               </script>\n");
          out.write("\n");
          out.write("\n");
          out.write("                                                                <ul class=\"dropdown-menu\">\n");
          out.write("                                                                    <li><a class=\"btn btn-sm btn-outline-primary btn-rounded btn-icon\" href=\"AdminCourseDetail?id=");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${o.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\">\n");
          out.write("                                                                            <i class=\"mdi mdi-dots-vertical\">Khoa hoc</i>\n");
          out.write("                                                                        </a></li>\n");
          out.write("                                                                    <li>Cái này thay bằng button, viết function js rồi gán vào onCLick. trong function đó gọi ajax().del('/mycourse')</li>\n");
          out.write("                                                                </ul>\n");
          out.write("                                                            </div>\n");
          out.write("                                                        </td>\n");
          out.write("                                                    </tr>\n");
          out.write("                                                ");
          int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_0.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_0);
    }
    return false;
  }
}
