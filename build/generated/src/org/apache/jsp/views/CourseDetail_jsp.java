package org.apache.jsp.views;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import dao.*;
import model.*;

public final class CourseDetail_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta charset=\"UTF-8\" />\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\" />\n");
      out.write("        <title>Chi tiết khóa học</title>\n");
      out.write("        <link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC\" crossorigin=\"anonymous\">\n");
      out.write("        <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js\" integrity=\"sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM\" crossorigin=\"anonymous\"></script>\n");
      out.write("        <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js\" integrity=\"sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM\" crossorigin=\"anonymous\"></script>\n");
      out.write("        <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js\" integrity=\"sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF\" crossorigin=\"anonymous\"></script>\n");
      out.write("        <link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">\n");
      out.write("        <link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>\n");
      out.write("        <link href=\"https://fonts.googleapis.com/css2?family=Sarala&display=swap\" rel=\"stylesheet\">\n");
      out.write("        <!-- Font Awesome -->\n");
      out.write("        <link href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css\" rel=\"stylesheet\" />\n");
      out.write("        <!-- MDB -->\n");
      out.write("        <link href=\"https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.css\" rel=\"stylesheet\" />\n");
      out.write("        <!-- MDB -->\n");
      out.write("        <script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.js\"></script>\n");
      out.write("        <link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">\n");
      out.write("        <link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>\n");
      out.write("        <link href=\"https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap\" rel=\"stylesheet\">\n");
      out.write("        <link href=\"/css/Registration.css\" rel=\"stylesheet\" />\n");
      out.write("        <style>\n");
      out.write("\n");
      out.write("            div.stars {\n");
      out.write("                width: 270px;\n");
      out.write("                display: inline-block;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            input.star {\n");
      out.write("                display: none;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            label.star {\n");
      out.write("                float: right;\n");
      out.write("                padding: 10px;\n");
      out.write("                font-size: 18px;\n");
      out.write("                color: #444;\n");
      out.write("                transition: all .2s;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            input.star:checked ~ label.star:before {\n");
      out.write("                content: '\\f005';\n");
      out.write("                color: #FD4;\n");
      out.write("                transition: all .25s;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            input.star-5:checked ~ label.star:before {\n");
      out.write("                color: #FE7;\n");
      out.write("                text-shadow: 0 0 20px #952;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            input.star-1:checked ~ label.star:before {\n");
      out.write("                color: #F62;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            label.star:hover {\n");
      out.write("                transform: rotate(-15deg) scale(1.3);\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            label.star:before {\n");
      out.write("                content: '\\f006';\n");
      out.write("                font-family: FontAwesome;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            #wapper-start {\n");
      out.write("                margin: 0 auto;\n");
      out.write("                width: 66%;\n");
      out.write("                margin-bottom: 50px;\n");
      out.write("                padding-bottom: 20px;\n");
      out.write("                background-color: #fff;\n");
      out.write("                border-radius: 10px 10px 10px 10px;\n");
      out.write("                border: 1px solid #efefef;\n");
      out.write("                transition: all 0.4s ease;\n");
      out.write("                box-shadow: 0 1px 5px 0 rgb(0 0 0 / 25%);\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            #total-rating {\n");
      out.write("                width: 100%;\n");
      out.write("                margin: 0px auto;\n");
      out.write("                text-align: center;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            #total-rating p {\n");
      out.write("                font-size: 18px;\n");
      out.write("                font-weight: 700;\n");
      out.write("            }\n");
      out.write("        </style>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <header>\n");
      out.write("            ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "Header.jsp", out, false);
      out.write("\n");
      out.write("            </header>\n");
      out.write("            <div id=\"main\">\n");
      out.write("                <div id=\"InformationCourse\">\n");
      out.write("                    <div class=\"LeftInformationCourse\">\n");
      out.write("                        <div class=\"NameOfCourse\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${course.tile}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</div>\n");
      out.write("\n");
      out.write("                    <div class=\"InforCourse\">\n");
      out.write("                        <div>\n");
      out.write("                            ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${course.short_description}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\n");
      out.write("                        </div>            </div>\n");
      out.write("                    <div class=\"IntroTeacher\" style=\"width: 100%; height: 30%; display: flex; margin-bottom: 10px; margin-top: 10px;\">\n");
      out.write("                        <div style=\" width: 20%; height: 100%;\"><img src=\"/Content/images/sir_alex.jpg\" style=\"width: 120px; height: 120px; border-radius: 50%; padding: auto;\"></div>\n");
      out.write("                        <div style=\" width: 80%; height: 100%; padding-top: 20px; padding-left: 10px;  overflow: auto; color: white;\">\n");
      out.write("                            <P style=\" font-size: 20px;\"><strong>GV: ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${course.owner}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</strong></P>\n");
      out.write("                            <p style=\" font-size: 15px;\">\n");
      out.write("                                ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${course.user_description}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                    <div class=\"Button\">\n");
      out.write("                        <a href=\"/Order?course_id=1\" class=\"buttonClickBuy\">MUA NGAY CHỈ VỚI ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${course.price}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(" VNĐ</a></div>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("                <div class=\"RightInformationCourse\">\n");
      out.write("                    <iframe src=\"https://www.youtube.com/embed/CFD9EFcNZTQ\"></iframe>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"title-Content-CourseView\">\n");
      out.write("                đánh giá khóa học\n");
      out.write("            </div>\n");
      out.write("            <hr>\n");
      out.write("            <div id=\"wapper-start\">\n");
      out.write("\n");
      out.write("                <form action=\"/DetailCourse/Vote\" method=\"post\"><div class=\"stars\" style=\"width: 60%;\">\n");
      out.write("                        <input class=\"star star-5\" id=\"star-5\" type=\"radio\" name=\"star5\" disabled />\n");
      out.write("                        <label class=\"star star-5\" for=\"star-5\"></label>\n");
      out.write("                        <input class=\"star star-4\" id=\"star-4\" type=\"radio\" name=\"star4\" checked />\n");
      out.write("                        <label class=\"star star-4\" for=\"star-4\"></label>\n");
      out.write("                        <input class=\"star star-3\" id=\"star-3\" type=\"radio\" name=\"star3\" disabled />\n");
      out.write("                        <label class=\"star star-3\" for=\"star-3\"></label>\n");
      out.write("                        <input class=\"star star-2\" id=\"star-2\" type=\"radio\" name=\"star2\" disabled />\n");
      out.write("                        <label class=\"star star-2\" for=\"star-2\"></label>\n");
      out.write("                        <input class=\"star star-1\" id=\"star-1\" type=\"radio\" name=\"star1\" disabled />\n");
      out.write("                        <label class=\"star star-1\" for=\"star-1\"></label>\n");
      out.write("                    </div> </form>\n");
      out.write("                <div id=\"total-rating\">\n");
      out.write("                    <p>Điểm đánh giá: 4</p>\n");
      out.write("                    <p>Số lượt đánh giá: 6</p>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("            <div id=\"IntroduceCourse\">\n");
      out.write("                <div class=\"title-Content-CourseView\">\n");
      out.write("                    giới thiệu khoá học\n");
      out.write("                </div>\n");
      out.write("                <hr>\n");
      out.write("                <div id=\"cover-title-content\">\n");
      out.write("                    <div class=\"content\">\n");
      out.write("                        <p>\n");
      out.write("                            👉 ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${course.descripton}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\n");
      out.write("                        </p>\n");
      out.write("                    </div>              \n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("                        \n");
      out.write("            <div id=\"cover-title-content2\">\n");
      out.write("                <div class=\"title-Content-CourseView\"> nội dung khoá học </div>\n");
      out.write("            </div>\n");
      out.write("            <hr>\n");
      out.write("            ");

                int count = 0;
                int count1 = 0;
            
      out.write("\n");
      out.write("\n");
      out.write("            \n");
      out.write("            <!-- JS - Accordtion effect -->\n");
      out.write("            <script>\n");
      out.write("                const accordionItemHeaders = document.querySelectorAll(\".accordion-item-header\");\n");
      out.write("                accordionItemHeaders.forEach(accordionItemHeader => {\n");
      out.write("                    accordionItemHeader.addEventListener(\"click\", event => {\n");
      out.write("                        accordionItemHeader.classList.toggle(\"active\");\n");
      out.write("                        const accordionItemBody = accordionItemHeader.nextElementSibling;\n");
      out.write("                        if (accordionItemHeader.classList.contains(\"active\")) {\n");
      out.write("                            accordionItemBody.style.maxHeight = accordionItemBody.scrollHeight + \"px\";\n");
      out.write("                        } else {\n");
      out.write("                            accordionItemBody.style.maxHeight = 0;\n");
      out.write("                        }\n");
      out.write("\n");
      out.write("                    });\n");
      out.write("                });\n");
      out.write("\n");
      out.write("                accordionItemHeaders.forEach(accordionItemHeader => {\n");
      out.write("                    const accordionItemBody = accordionItemHeader.nextElementSibling;\n");
      out.write("                    if (accordionItemHeader.classList.contains(\"Leaned\")) {\n");
      out.write("                        accordionItemBody.style.maxHeight = accordionItemBody.scrollHeight + \"px\";\n");
      out.write("                    } else {\n");
      out.write("                        accordionItemBody.style.maxHeight = 0;\n");
      out.write("                    }\n");
      out.write("                });\n");
      out.write("            </script>\n");
      out.write("        </div>\n");
      out.write("        <footer>\n");
      out.write("            ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "Footer.jsp", out, false);
      out.write("\n");
      out.write("        </footer>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
