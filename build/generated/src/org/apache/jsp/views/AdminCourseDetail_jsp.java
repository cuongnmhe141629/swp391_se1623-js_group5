package org.apache.jsp.views;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class AdminCourseDetail_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <title>Cập nhật</title>\r\n");
      out.write("        <!-- all the meta tags -->\r\n");
      out.write("        <meta http-equiv=\"content-type\" content=\"text/html;charset=UTF-8\" />\r\n");
      out.write("        <meta charset=\"utf-8\" />\r\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\" />\r\n");
      out.write("        <link rel=\"shortcut icon\" href=\"/images/favicon.png\">\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        <!-- PAGE CONTAINER-->\r\n");
      out.write("        <div class=\"content-page\">\r\n");
      out.write("            <div class=\"content\">\r\n");
      out.write("                <!-- BEGIN PlACE PAGE CONTENT HERE -->\r\n");
      out.write("                <div class=\"row\">\r\n");
      out.write("                    <div class=\"col-xl-12\">\r\n");
      out.write("                        <div class=\"card\">\r\n");
      out.write("                            <div class=\"card-body\">\r\n");
      out.write("\r\n");
      out.write("                                <div class=\"row\">\r\n");
      out.write("                                    <div class=\"col-md-6\">\r\n");
      out.write("                                        <h4 class=\"header-title my-1\">Update Subject dimension</h4>\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </div>.\r\n");
      out.write("\r\n");
      out.write("                                <div class=\"row\">\r\n");
      out.write("                                    <div class=\"col-xl-12\">\r\n");
      out.write("                                        <form class=\"required-form\" action=\"/AdminUpdateCourse\" method=\"post\">\r\n");
      out.write("                                            <div id=\"basicwizard\">\r\n");
      out.write("\r\n");
      out.write("                                                <ul class=\"nav nav-pills nav-justified form-wizard-header\">\r\n");
      out.write("                                                    <li class=\"nav-item\">\r\n");
      out.write("                                                        <a href=\"#basic\" data-toggle=\"tab\" class=\"nav-link rounded-0 pt-2 pb-2\">\r\n");
      out.write("                                                            <i class=\"mdi mdi-fountain-pen-tip\"></i>\r\n");
      out.write("                                                            <span class=\"d-none d-sm-inline\">Course ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${c.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(" </span>\r\n");
      out.write("                                                        </a>\r\n");
      out.write("                                                    </li>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("                                                    <li class=\"w-100 bg-white pb-3\">\r\n");
      out.write("                                                        <!--ajax page loader-->\r\n");
      out.write("                                                        <div class=\"ajax_loader w-100\">\r\n");
      out.write("                                                            <div class=\"ajax_loaderBar\"></div>\r\n");
      out.write("                                                        </div>\r\n");
      out.write("                                                        <!--end ajax page loader-->\r\n");
      out.write("                                                    </li>\r\n");
      out.write("                                                </ul>\r\n");
      out.write("\r\n");
      out.write("                                                <div class=\"tab-content b-0 mb-0\">\r\n");
      out.write("                                                    <div class=\"tab-pane\" id=\"basic\">\r\n");
      out.write("                                                        <div class=\"row justify-content-center\">\r\n");
      out.write("                                                            <div class=\"col-xl-8\">\r\n");
      out.write("                                                                <input type=\"hidden\" name = \"course_type\" value=\"general\">\r\n");
      out.write("                                                               <div class=\"form-group row mb-3\">\r\n");
      out.write("                                                                            <label class=\"col-md-3 col-form-label\" for=\"course_id\">Course id <span class=\"required\">*</span> </label>\r\n");
      out.write("                                                                            <div class=\"col-md-9\">\r\n");
      out.write("                                                                                <input type=\"text\" class=\"form-control\" id=\"course_id\" name = \"id\" placeholder=\"Enter course title\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${c.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\" readonly>\r\n");
      out.write("                                                                            </div>\r\n");
      out.write("                                                                        </div>\r\n");
      out.write("                                                                            \r\n");
      out.write("                                                                        <div class=\"form-group row mb-3\">\r\n");
      out.write("                                                                            <label class=\"col-md-3 col-form-label\" for=\"course_title\">Course title  </label>\r\n");
      out.write("                                                                            <div class=\"col-md-9\">\r\n");
      out.write("                                                                                <input type=\"text\" class=\"form-control\" id=\"course_title\" name = \"title\" placeholder=\"Enter course title\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${c.title}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\" required>\r\n");
      out.write("                                                               \r\n");
      out.write("                                                                            </div>\r\n");
      out.write("                                                                        </div>\r\n");
      out.write("                                                                        <div class=\"form-group row mb-3\">\r\n");
      out.write("                                                                            <label class=\"col-md-3 col-form-label\" for=\"short_description\">Short description</label>\r\n");
      out.write("                                                                            <div class=\"col-md-9\">\r\n");
      out.write("                                                                                <textarea name=\"short_description\" id = \"short_description\" class=\"form-control\" >");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${c.short_description}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</textarea>\r\n");
      out.write("                                                                            </div>\r\n");
      out.write("                                                                        </div>\r\n");
      out.write("                                                                        <div class=\"form-group row mb-3\">\r\n");
      out.write("                                                                            <label class=\"col-md-3 col-form-label\" for=\"description\">Description</label>\r\n");
      out.write("                                                                            <div class=\"col-md-9\">\r\n");
      out.write("                                                                                <textarea name=\"description\" id = \"description\" class=\"form-control\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${c.descripton}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</textarea>\r\n");
      out.write("                                                                            </div>\r\n");
      out.write("                                                                        </div>\r\n");
      out.write("                                                                        <div class=\"form-group row mb-3\">\r\n");
      out.write("\r\n");
      out.write("                                                                            <label class=\"col-md-3 col-form-label\" for=\"category_id\">Category_id <span class=\"required\">*</span> </label>\r\n");
      out.write("                                                                            <div class=\"col-md-9\">\r\n");
      out.write("                                                          \r\n");
      out.write("                                                                     \r\n");
      out.write("                                                                                <input type=\"text\" class=\"form-control\" id=\"course_price\" name = \"price\" placeholder=\"Enter course \" \r\n");
      out.write("                                                                                       value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${c.category_id==listCategory.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\" required>\r\n");
      out.write("                                                                            </div>\r\n");
      out.write("                                                                        </div>\r\n");
      out.write("                                                                                        <div class=\"form-group row mb-3\">\r\n");
      out.write("\r\n");
      out.write("                                                                            <label class=\"col-md-3 col-form-label\" for=\"is_free_course\">Is_free_course <span class=\"required\">*</span> </label>\r\n");
      out.write("                                                                            <div class=\"col-md-9\">\r\n");
      out.write("                                                                                <input type=\"text\" class=\"form-control\" id=\"course_price\" name = \"price\" placeholder=\"Enter course \" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${c.is_free_course}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\" required>\r\n");
      out.write("                                                                            </div>\r\n");
      out.write("                                                                        </div>\r\n");
      out.write("\r\n");
      out.write("                                                                        <div class=\"form-group row mb-3\">\r\n");
      out.write("                                                                            <div class=\"offset-md-2 col-md-10\">\r\n");
      out.write("                                                                                <div class=\"custom-control custom-checkbox\">\r\n");
      out.write("                                                                                    <button type=\"submit\" id=\"submit\" class=\"btn btn-info offset-5\">Submit</button>\r\n");
      out.write("                                                                                </div>\r\n");
      out.write("                                                                            </div>\r\n");
      out.write("                                                                    </div>\r\n");
      out.write("                                                                </div>\r\n");
      out.write("                                                            </div> <!-- end col -->\r\n");
      out.write("                                                        </div> <!-- end row -->\r\n");
      out.write("                                                    </div> <!-- end tab pane -->\r\n");
      out.write("                                                </div> <!-- tab-content -->\r\n");
      out.write("                                            </div> <!-- end-->\r\n");
      out.write("                                            <div style=\"color: red;\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${success}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</div>\r\n");
      out.write("                                        </form>\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </div><!-- end row-->\r\n");
      out.write("                            </div> <!-- end card-body-->\r\n");
      out.write("                        </div> <!-- end card-->\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("\r\n");
      out.write("                <script type=\"text/javascript\">\r\n");
      out.write("                    $(document).ready(function () {\r\n");
      out.write("                        initSummerNote(['#description']);\r\n");
      out.write("                    });\r\n");
      out.write("                </script>\r\n");
      out.write("\r\n");
      out.write("                <script type=\"text/javascript\">\r\n");
      out.write("                    var blank_outcome = jQuery('#blank_outcome_field').html();\r\n");
      out.write("                    var blank_requirement = jQuery('#blank_requirement_field').html();\r\n");
      out.write("                    jQuery(document).ready(function () {\r\n");
      out.write("                        jQuery('#blank_outcome_field').hide();\r\n");
      out.write("                        jQuery('#blank_requirement_field').hide();\r\n");
      out.write("                    });\r\n");
      out.write("                    function appendOutcome() {\r\n");
      out.write("                        jQuery('#outcomes_area').append(blank_outcome);\r\n");
      out.write("                    }\r\n");
      out.write("                    function removeOutcome(outcomeElem) {\r\n");
      out.write("                        jQuery(outcomeElem).parent().parent().remove();\r\n");
      out.write("                    }\r\n");
      out.write("\r\n");
      out.write("                    function appendRequirement() {\r\n");
      out.write("                        jQuery('#requirement_area').append(blank_requirement);\r\n");
      out.write("                    }\r\n");
      out.write("                    function removeRequirement(requirementElem) {\r\n");
      out.write("                        jQuery(requirementElem).parent().parent().remove();\r\n");
      out.write("                    }\r\n");
      out.write("\r\n");
      out.write("                    function priceChecked(elem) {\r\n");
      out.write("                        if (jQuery('#discountCheckbox').is(':checked')) {\r\n");
      out.write("\r\n");
      out.write("                            jQuery('#discountCheckbox').prop(\"checked\", false);\r\n");
      out.write("                        } else {\r\n");
      out.write("\r\n");
      out.write("                            jQuery('#discountCheckbox').prop(\"checked\", true);\r\n");
      out.write("                        }\r\n");
      out.write("                    }\r\n");
      out.write("                </script>\r\n");
      out.write("\r\n");
      out.write("                <style media=\"screen\">\r\n");
      out.write("                    body {\r\n");
      out.write("                        overflow-x: hidden;\r\n");
      out.write("                    }\r\n");
      out.write("                </style>\r\n");
      out.write("                <!-- END PLACE PAGE CONTENT HERE -->\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("        <!-- END CONTENT -->\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
