USE [master]
GO
/****** Object:  Database [OnlineLearn_SWP391_G5]    Script Date: 11/11/2022 8:59:30 CH ******/
CREATE DATABASE [OnlineLearn_SWP391_G5]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'OnlineLearn_SWP391_G2', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\OnlineLearn_SWP391_G2.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'OnlineLearn_SWP391_G2_log', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\OnlineLearn_SWP391_G2_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [OnlineLearn_SWP391_G5].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET ARITHABORT OFF 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET  DISABLE_BROKER 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET RECOVERY FULL 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET  MULTI_USER 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET DB_CHAINING OFF 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'OnlineLearn_SWP391_G5', N'ON'
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET QUERY_STORE = OFF
GO
USE [OnlineLearn_SWP391_G5]
GO
/****** Object:  User [sa]    Script Date: 11/11/2022 8:59:30 CH ******/
CREATE USER [sa] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [cu]    Script Date: 11/11/2022 8:59:30 CH ******/
CREATE USER [cu] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[Blog]    Script Date: 11/11/2022 8:59:31 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Blog](
	[blog_id] [int] IDENTITY(1,1) NOT NULL,
	[blog_category_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[title] [nvarchar](100) NULL,
	[brief_infor] [nvarchar](100) NULL,
	[description] [nvarchar](max) NULL,
	[thumbnail] [nvarchar](200) NULL,
	[banner] [nvarchar](200) NULL,
	[added_date] [datetime] NULL,
 CONSTRAINT [PK_Blog] PRIMARY KEY CLUSTERED 
(
	[blog_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Blog_Category]    Script Date: 11/11/2022 8:59:31 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Blog_Category](
	[blog_category_id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](100) NULL,
 CONSTRAINT [PK_Blog_Category] PRIMARY KEY CLUSTERED 
(
	[blog_category_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Blog_Comment]    Script Date: 11/11/2022 8:59:31 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Blog_Comment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[blog_id] [int] NULL,
	[user_id] [int] NULL,
	[comment] [nvarchar](300) NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_Blog_Comment] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Course]    Script Date: 11/11/2022 8:59:31 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Course](
	[course_id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](100) NOT NULL,
	[short_description] [nvarchar](200) NULL,
	[description] [nvarchar](max) NULL,
	[category_id] [int] NOT NULL,
	[tag_line] [nvarchar](100) NULL,
	[thumbnail] [nvarchar](200) NULL,
	[is_free_course] [int] NULL,
	[updated_date] [datetime] NULL,
	[status] [bit] NULL,
	[featured_flag] [bit] NULL,
	[owner] [int] NULL,
	[price] [float] NULL,
 CONSTRAINT [PK_Course] PRIMARY KEY CLUSTERED 
(
	[course_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Course_Category]    Script Date: 11/11/2022 8:59:31 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Course_Category](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](200) NULL,
	[thumbnail] [nvarchar](200) NULL,
 CONSTRAINT [PK_Course_Category] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Enrol]    Script Date: 11/11/2022 8:59:31 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Enrol](
	[enrol_id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[course_id] [int] NOT NULL,
	[date_added] [datetime] NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_Enrol_1] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC,
	[course_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lesson]    Script Date: 11/11/2022 8:59:31 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lesson](
	[lesson_id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](100) NULL,
	[course_id] [int] NULL,
	[topic_id] [int] NULL,
	[video_url] [nvarchar](500) NULL,
	[lesson_type] [nvarchar](100) NULL,
	[attachment] [nvarchar](300) NULL,
	[summary] [nvarchar](300) NULL,
	[status] [bit] NULL,
 CONSTRAINT [PK_Lesson] PRIMARY KEY CLUSTERED 
(
	[lesson_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Payment]    Script Date: 11/11/2022 8:59:31 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payment](
	[payment_id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[course_id] [int] NOT NULL,
	[amount] [float] NOT NULL,
	[date_added] [datetime] NULL,
 CONSTRAINT [PK_Payment] PRIMARY KEY CLUSTERED 
(
	[payment_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Price_Package]    Script Date: 11/11/2022 8:59:31 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Price_Package](
	[pricePackage_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[access_duration] [int] NULL,
	[status] [bit] NULL,
	[list_price] [float] NULL,
	[sale_price] [float] NULL,
	[description] [nvarchar](max) NULL,
	[course_id] [int] NULL,
 CONSTRAINT [PK_Price_Package] PRIMARY KEY CLUSTERED 
(
	[pricePackage_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Question]    Script Date: 11/11/2022 8:59:31 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Question](
	[question_id] [int] IDENTITY(1,1) NOT NULL,
	[quiz_id] [int] NULL,
	[title] [nvarchar](100) NULL,
	[type] [nvarchar](100) NULL,
	[content] [nvarchar](max) NULL,
	[level] [int] NULL,
	[dimension] [int] NULL,
	[media] [nvarchar](max) NULL,
	[number_of_option] [int] NULL,
	[answers_options] [nvarchar](200) NULL,
	[correct_options] [nvarchar](200) NULL,
	[explanation] [nvarchar](max) NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_Question] PRIMARY KEY CLUSTERED 
(
	[question_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Quiz]    Script Date: 11/11/2022 8:59:31 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quiz](
	[quiz_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](100) NOT NULL,
	[course_id] [int] NOT NULL,
	[duration] [float] NULL,
	[pass_rate] [float] NULL,
	[type] [nvarchar](100) NULL,
	[level] [int] NULL,
 CONSTRAINT [PK_Quiz] PRIMARY KEY CLUSTERED 
(
	[quiz_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rating]    Script Date: 11/11/2022 8:59:31 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rating](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[rating] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[course_id] [int] NOT NULL,
	[review] [nvarchar](300) NULL,
 CONSTRAINT [PK_Rating] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 11/11/2022 8:59:31 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[role_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[role_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Setting]    Script Date: 11/11/2022 8:59:31 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Setting](
	[setting_id] [int] IDENTITY(1,1) NOT NULL,
	[type] [nvarchar](50) NULL,
	[order] [nvarchar](50) NULL,
	[description] [nvarchar](max) NULL,
	[status] [bit] NULL,
	[user_id] [int] NOT NULL,
 CONSTRAINT [PK_Setting] PRIMARY KEY CLUSTERED 
(
	[setting_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Subject_Dimension]    Script Date: 11/11/2022 8:59:31 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Subject_Dimension](
	[dimension_id] [int] IDENTITY(1,1) NOT NULL,
	[type] [nvarchar](50) NULL,
	[name] [nvarchar](100) NULL,
	[description] [nvarchar](max) NULL,
	[course_id] [int] NULL,
 CONSTRAINT [PK_Subject_Dimension] PRIMARY KEY CLUSTERED 
(
	[dimension_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Topic]    Script Date: 11/11/2022 8:59:31 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Topic](
	[topic_id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](100) NULL,
	[course_id] [int] NULL,
 CONSTRAINT [PK_Section] PRIMARY KEY CLUSTERED 
(
	[topic_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 11/11/2022 8:59:31 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[user_id] [int] IDENTITY(1,1) NOT NULL,
	[fullname] [nvarchar](100) NULL,
	[gender] [bit] NULL,
	[email] [nvarchar](50) NOT NULL,
	[phone] [nvarchar](25) NULL,
	[password] [nvarchar](200) NOT NULL,
	[role_id] [int] NOT NULL,
	[image] [nvarchar](max) NULL,
	[status] [int] NULL,
	[featured] [bit] NULL,
	[hash] [varchar](255) NULL,
	[active] [int] NULL,
	[address] [nvarchar](50) NULL,
	[description] [nvarchar](max) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Blog] ON 

INSERT [dbo].[Blog] ([blog_id], [blog_category_id], [user_id], [title], [brief_infor], [description], [thumbnail], [banner], [added_date]) VALUES (2, 1, 1, N'Coursera Launches Expanded Clips Offering with Nearly 200K Short Videos', N' Lack of career development is a key reason many employees....', N'ack of career development is a key reason many employees leave an organization. Yet employees have less time to devote to learning than ever before. A growing number of employees are now looking to gain job-specific skills much faster through shorter, more targeted content. 

Today, we’re excited to announce the expansion of Clips, which now provides employees access to nearly 200,000 short videos and lessons designed to help them begin learning high-demand skills in under 10 minutes.

With this shift towards microlearning, Clips enables employees to learn specific skills with 5-10 min videos. It also provides clear pathways into courses for deeper skill development, while driving continuous learning to ensure employees have the right skills to accelerate company growth and innovation.

The initial Clips on Coursera offering featured bite-sized videos focused on business, tech, data, leadership, and human skills.  The expanded Clips offering now provides a more comprehensive library of content, including a higher volume of videos for skills that are becoming increasingly more important like leadership and management. Currently, 83 percent of businesses feel it is important to develop leaders at all levels. Yet less than 5 percent have actually implemented leadership training across every level within their organization. To help address this growing need, Coursera has increased the number of leadership and management-related Clips by 10x. 

As learners quickly gain skills through Clips, many then seek opportunities for deeper development.  Thirty-six  percent of learners that have viewed Clips to address their in-the-moment learning needs have gone on to enroll in at least one course. Global industry leaders and government organizations like Google, New York State Department of Labor,  Alstom, and Bosch have adopted Clips to help accelerate skills development within their workforce. Google in particular has made Clips available to all employees worldwide, helping many learners obtain skills that will help them elevate their job performance and begin unlocking new advancement opportunities.

With Clips, Coursera now offers fully scalable learning paths featuring everything from short videos and lessons to professional certificates, and degree programs.

“For my role, developing skills in programming, camera hardware, and design is crucial. Finding relevant courses to acquire these new skills, and gaining practical, hands-on experience has been a challenge,” said Doris Chen, hardware design engineer at Google. “The Clips videos provide relevant content focused on everything from camera and imaging to optical system design. As I continue my training, I’m looking forward to enrolling in more advanced classes and potentially earning an additional degree online to help further my career.” 

“I have always been fascinated by the concept of skills development programs and self-paced learning, and the ways it can assist to level up your career,” said Rajat Batra, software engineer  at Google. “Clips takes this concept to the next level by adding short and focused job related videos. These videos act as a good starting point to develop a higher level of understanding for several concepts.”

Coursera for Business now enables over 3,500 companies across the globe to deploy world-class skills development content from top businesses and universities. The company’s skills platform gives organizations the ability to assess, measure, and benchmark skills in their workforce. Recent platform innovations including LevelSets are helping employees to quickly determine their proficiency in key skills and identify areas to focus on moving forward. In addition, SkillSets help employees develop specific skills for specific roles. These Skillsets provide the foundation for Coursera’s Leadership and Data & Analytics Academies, which offer a packaged learning experience based on the depth of skill needed for specific roles across an organization.

As the pace of innovation accelerates, the demand for new skills will continue to increase. We’re honored to deliver the expanded Clips offering featuring job-relevant content from the world’s leading companies and universities to help learners further their career.', N'blog1.jpg', NULL, CAST(N'2022-09-29T00:00:00.000' AS DateTime))
INSERT [dbo].[Blog] ([blog_id], [blog_category_id], [user_id], [title], [brief_infor], [description], [thumbnail], [banner], [added_date]) VALUES (3, 2, 2, N'Trending online courses and degrees in business and tech  ', N'With more world-class content launching every week, there are always new topic...', N'With more world-class content launching every week, there are always new topics to explore, new skills to learn, and new ways to achieve your goals. These latest courses, Specializations, and Professional Certificate programs cover everything from developing your own paid advertising campaign and building a high-performing team to creating machine learning-powered projects to add to your portfolio. What will you learn next?

Business
Google Digital Marketing & E-commerce Professional Certificate from Google 

Gain in-demand skills and prepare for an entry-level job in marketing or e-commerce. You’ll learn how to use industry-standard tools and platforms like Canva, Constant Contact, Google Ads, Google Analytics, Hootsuite, HubSpot, Mailchimp, Shopify, and Twitter from subject-matter experts at Google. Build your own portfolio with projects to show to potential employers. 

Business Essentials MasterTrack Certificate from IE Business School

Master business essentials across strategy, marketing, and finance, and prepare for career success in managerial roles from one of the world’s top business schools. You’ll cover fundamental concepts such as the basics of accounting for assets and liabilities and learn how to use a statement of cash flows to evaluate a company.

Leading Innovation With Creativity MasterTrack Certificate from HEC Paris

Develop a personal toolbox and leadership framework to foster creativity in your organization and build the workplace skills necessary to manage teams, projects, and organizations more effectively. Incorporate design thinking and creativity into your everyday business practices and learn how to drive innovation at any stage of business growth.

Executive MBA from Indian Institute of Technology Roorkee

Develop an insight into key managerial processes and gain analytical expertise with a technology-centric viewpoint. This highly customizable degree allows you to follow your interests and choose your area of specialization after covering the fundamentals of business and technology.

Global Management Programme in Operations and Supply Chain from The Indian School of Business

Learn in-demand operations and supply chain skills and earn a valuable career credential from a top-ranked, research-driven business school. The program, designed by the renowned faculty of one of the top B schools in India, offers state-of-the-art simulations and interactions with industry leaders.

Global Management Programme for Infrastructure from The Indian School of Business

This program is designed for working professionals so you can develop leadership skills and contribute towards the growth of the infrastructure and real estate sector. You will find the right balance of theory and practice, taught by the renowned faculty from ISB and other premier educational institutes across the world.

Master of Engineering in Engineering Management from University of Colorado Boulder

Whether you are a skilled engineer who wants to advance into high-impact management roles or a management professional looking to lead high-performing technical teams, you can earn your engineering management degree from one of the world’s top institutions and gain a vital leadership education built upon a concrete foundation of in-demand applied skills.

Engineering Management Graduate Certificate from University of Colorado Boulder

In this three-specialization program, which integrates contemporary concepts of leadership, project management, and finance for technical managers, you will gain specific tools and knowledge to lead technical teams.

MS in Management: Digital Transformation in Healthcare from Northeastern University, created and taught in collaboration with Mayo Clinic College of Medicine and Science

Learn cutting-edge skills to be a high-impact digital change agent leading innovation as a manager in the healthcare field in a program that brings together the world’s #1 hospital with a top academic research institution. ', N'blog2.jpg', NULL, CAST(N'2020-12-05T00:00:00.000' AS DateTime))
INSERT [dbo].[Blog] ([blog_id], [blog_category_id], [user_id], [title], [brief_infor], [description], [thumbnail], [banner], [added_date]) VALUES (4, 3, 2, N'Discover Five New Professional Certificate Programs from Meta', N'Meta is expanding its partnership with Coursera by offering  ...', N'Meta is expanding its partnership with Coursera by offering five new Professional Certificates in the field of Software Engineering. When you earn your Professional Certificate, you’ll be ideally positioned to take advantage of high salaries and the growing demand for skilled talent. In the United States alone, software engineering is projected to grow 22% by 2030.

With these Professional Certificate programs from Meta, you can prepare for an in-demand career as a front-end, back-end, Android, or iOS developer or as a database engineer. 

Designed by experts at Meta, these programs are particularly ideal if you want to build a career in this fast-growing field but don’t yet have relevant experience or background in software engineering. Professionals already working in this field can benefit as well, as you’ll be able to stay current with the latest in-demand skills. All programs are completely online and self-paced, at the cost of only $49 per month.

In these programs, you’ll learn industry-standard skills such as HTML, CSS, Java Programming, Python, MySQL, and more, and get hands-on experience by building a portfolio you can use to showcase your new skills to recruiters, hiring managers, and employers.

As soon as you earn your Professional Certificate, you’ll get exclusive access to the Meta Career Programs Job Board—a job search platform where you can connect with 200+ employers who are committed to sourcing talent through Meta’s certificate programs. You’ll also get access to career support resources to help you with your job search.', N'blog3.jpg', NULL, CAST(N'2019-08-25T00:00:00.000' AS DateTime))
INSERT [dbo].[Blog] ([blog_id], [blog_category_id], [user_id], [title], [brief_infor], [description], [thumbnail], [banner], [added_date]) VALUES (5, 4, 4, N'Announcing the Coursera Global Skills Report 2022 ', N'The Global Skills Report draws data from 100 million learners in more than 100 countries...', N'Today, I’m excited to introduce the Coursera Global Skills Report 2022, an in-depth look at the state of skills globally. The Global Skills Report draws data from 100 million learners in more than 100 countries who have used Coursera to develop a new skill during the past year. The report benchmarks three of the most in-demand skill areas driving employment in the digital economy – business, technology, and data science. For the first time, this year’s report also highlights changes in ranking for each country, and state-specific data for the U.S. and India, where regional variations are notable. 

The 2022 Global Skills Report highlights how the acceleration of digital transformation, inflation, and global instability are driving increased demand for digital and human skills needed to thrive in the new economy. The Great Resignation and automation are mandating stronger investments in human capital, as institutions must prioritize developing the high-demand digital and human skills required to build a competitive and equitable workforce. Our data shows these skills are not equally distributed, and students and low-wage workers need access to flexible, affordable, and fast-tracked pathways to entry-level digital jobs that offer a foundation for a stronger and more inclusive economy. 

Understanding the Global Skills Ranking

In the Global Skills Report, over 100 countries are ranked against one another, with percentile rankings attributed to each skill proficiency. A country that shows 100% skills proficiency ranks at the top of the 100+ countries, and a country at 0% is at the bottom. Coursera breaks out each group’s percentile rankings into the following four categories based on quartiles: 

Cutting-edge (76% or above)
Competitive (51% – 75%)
Emerging (26%-50%)
Lagging (25% or below)
Global trends in the report include:

Entry-level or “gateway” certificate course enrollments among women reached 40% in 2021, up significantly from 25% in 2019. Certificates, such as Google IT Support and Google Data Analytics, provide a clear pathway to gain skills needed for high-demand, entry-level digital jobs. These courses require approximately 240 total learning hours, which can be completed in just six months at 10 hours per week. 
There is a strong correlation between skills proficiency, GDP, and broadband access. Wealthier countries scored higher in overall skills proficiency, matched by those with high levels of internet access.
Developed countries saw more learners acquiring human skills including change management and resilience. Learners in developing countries were more focused on digital skills through courses like supply chain systems and mobile architecture. 
The most popular business and technology skills globally in the last year were leadership and management, probability and statistics, and theoretical computer science. For the second year in a row, Switzerland had the highest-skilled learners followed by Denmark, Indonesia, and Belgium. 
Learners also focused on courses that develop the skills needed to understand the COVID-19 pandemic. Enrollment in courses that cover epidemiology and risk management is now four times higher than it was prior to the pandemic. 
Readers can also explore country specific insights. For example, key U.S. insights from the report include: 

The U.S. remained flat in overall skills proficiency at 29th, trailing countries in Asia & Europe. Business skills proficiency rose in the U.S., with key areas like leadership and management increasing from 40% in 2021 to 67% in 2022. However, technology skills proficiency overall dropped significantly from 69% in 2021 to 43% this year. Proficiency in data science also fell sharply from 73% last year to 54% in 2022.
U.S. learners in the Northeast, Upper Midwest, and along the Pacific Coast had the highest skills proficiency in business, while those in the South lagged behind. Three midwestern states including Illinois, Wisconsin, and Indiana ranked highest in business proficiency throughout the U.S. 
Idaho showed the highest levels of technology skills in the country, outpacing tech hubs like California and Massachusetts. Learners in the state also earned a perfect 100% proficiency for mobile development skills. This reflects a growth trend driven by the number of high-tech companies in the state increasing 61% in the last decade.
Learners in the U.S. increased focus on human skills amid rapid workforce changes. Workforce disruption caused by the pandemic and the pace of automation is forcing businesses to quickly adapt. Human skills like resilience, project management, decision making, planning, storytelling, and experiments were increasingly popular among U.S. business learners, as organizations worked to navigate change. 
The U.S. achieves greater gender parity in overall course enrollments but women still lag behind men in STEM. The online course enrollment rate for women reached its highest point (51%) in the last year, continuing a trend that started in 2020. Despite a rise in STEM enrollments from 35% in 2019 to 42% in 2022, women still trail men in the U.S.
The U.S. remains behind the curve in math skills. Proficiency in mathematics among U.S. learners dropped sharply from 56% in 2021 to 40% in 2022. This lags countries throughout Europe including Germany at 81% and the UK at 78% proficiency. Maine, Washington, and New Hampshire had the highest levels of math proficiency in the U.S., while Mississippi, Louisiana, and Tennessee finished in the bottom three.
With over 100 million learners, 7,000+ institutions, and more than 5,000 courses from 250 of the world’s leading universities and industry educators, Coursera has one of the largest data sets for identifying and measuring skill trends. 

To download the full report and explore insights unique to a country or region, visit here.', N'blog4.jpg', NULL, CAST(N'2022-06-14T00:00:00.000' AS DateTime))
INSERT [dbo].[Blog] ([blog_id], [blog_category_id], [user_id], [title], [brief_infor], [description], [thumbnail], [banner], [added_date]) VALUES (6, 5, 4, N' How to Evolve Your Talent Strategy With Workplace Learning ', N' In the face of a global talent shortage, learning and technology ...', N'In the face of a global talent shortage, learning and technology leaders are under immense pressure to source the critical skill sets needed to drive business impact. Failure to do so will cost their organizations trillions in the next decade. 

Compounding the challenge is the ongoing ‘Great Resignation’, following a worldwide string of record-breaking resignations—as employees call for greater investment in their development. Alongside this, the World Bank has warned of global stagflation and a recession ahead, shifting the focus of talent towards improving performance whilst reducing costs. 

What’s more, up to 50% of employers find that the opportunity created by remote global hires is being offset by a parallel rise in competition from abroad, posing the question of whether external hiring is still the most straightforward strategy for closing the skills gap. 

The combined effects of these phenomena underscore the case for reskilling and upskilling from within as the single-most sustainable talent strategy. But creating skills transformation programs that deliver business impact is broad and complex. From identifying and forecasting skills gaps to talent matching and delivering an end-to-end learning program, knowing where to start and scaling rapidly can be overwhelming. 

That’s why we’re excited to launch Coursera’s new ebook, Evolving Your Talent Strategy: The Urgent Need for Workplace Learning—and How to Execute on It.

This piece is built from the ground up to give learning and technology leaders everything they need to transform their approach to talent and secure the skill sets their organization urgently requires. It’s our new one-stop shop, designed for businesses of all shapes and sizes, to kick-start their journey to upskilling and reskilling their talent from within. 

Coursera for Business now provides 3,500 companies worldwide with job-based skills development featuring world-class content, hands-on learning, and the ability to track, measure, and benchmark skills through a single, unified platform. With over 275 university and industry partners, Coursera’s offerings range from over 10,000 bite-sized videos called Clips for “just in time” foundation learning in 5-10 minute chunks to Professional Certificates that can get your employees job-ready in completely new roles and professions in as little as six months. 

In the ebook, I’m proud that we’ve set out to share our expertise with learning and technology leaders worldwide to discover:

The latest on the global skills gap in the current global climate and an analysis of the risk that continuing with business-as-usual talent strategies presents. 
A guide to creating competitive capabilities from within through the process of talent matching—looking beyond traditional talent pools to “build” internal talent pipelines.
An end-to-end approach to building successful skills development programs that can demonstrate the ROI of upskilling and reskilling, bolstering the case for investment.
Skill maps that illuminate some of the ways learning leaders can start building a learning program tailored to their needs using various learning formats – from short-form Clips to Courses, Specializations, Certificates, and Degrees. 
I’m excited to continue our mission to bring the best learning to every corner of the world by empowering leaders everywhere to transform their talent strategies, delivering the critical skills their businesses need for the rocky road ahead. Download the full ebook here.', N'blog5.jpg', NULL, CAST(N'2022-08-22T00:00:00.000' AS DateTime))
INSERT [dbo].[Blog] ([blog_id], [blog_category_id], [user_id], [title], [brief_infor], [description], [thumbnail], [banner], [added_date]) VALUES (7, 1, 6, N'ADP, Accenture, and IBM launch new job-relevant content on Coursera ', N'Candidates must increasingly demonstrate both interpersonal and technical skills to land...', N'Candidates must increasingly demonstrate both interpersonal and technical skills to land and succeed in a role. According to a recent ZipRecruiter report, 93% of employers who posted roles on the hiring platform in the last year said that soft skills – including communication, mentoring, and problem solving – played a critical role in their hiring decisions. 

To help learners develop those critical skills, this quarter we introduced new Specializations and courses from industry partners ADP, Accenture, and IBM. This content is taught by industry experts and covers broad topics, including how to effectively lead teams, leverage technology and data, and communicate in the workplace. The new programs are: 

Leading StandOut® Teams Specialization from ADP – This eight-course program exposes learners to a strengths-based approach to leading a team. Learners will explore practical stories drawn from decades of real-world experience, participate in hands-on application of strength-based coaching principles, and, upon completion, have insight into their own power as a leader. This beginner-level program is designed to be completed within six months if learners commit to about four hours of learning per week. 
“ADP is thrilled to launch the Leading Standout Teams program with Coursera. This robust leadership and coaching program harnesses the groundbreaking Marcus Buckingham strengths-based leadership philosophy and offers learners a certification opportunity. We are looking forward to continuing to partner with Coursera to empower learners to build people-centric skills and competencies through engaging learning formats.” – Aundrea Malone-Program Strategy Director, ADP Enterprise Learning

Understanding Technology and Data and Start Your Lifelong Learning courses from Accenture – These two foundational courses are designed to be completed within 10-15 hours each. Understanding Technology and Data provides insight into how technology and data are applied in everyday life to help learners succeed in a digital work environment. Start Your Lifelong Learning examines how to adopt a growth mindset, interact effectively with others, and think through decisions, preparing learners to resolve their own professional and personal challenges.
People and Soft Skills for Professional and Personal Success Specialization from IBM – This six-course Specialization is especially suitable for learners who want to start a new job or are early in their career. Throughout the program, learners will explore realistic scenarios and mini case studies to practice applying the skills they have learned. The Specialization can be completed in about two months with three hours of learning per week. 
This is the first content on Coursera from new partners ADP and Accenture, and we look forward to launching more expert-taught courses and Specializations from them in the near future. Together, with our 100+ industry partners, we are committed to providing learners with affordable, flexible, and job-relevant learning programs.', N'blog6.jpg', NULL, CAST(N'2022-08-22T00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[Blog] OFF
GO
SET IDENTITY_INSERT [dbo].[Blog_Category] ON 

INSERT [dbo].[Blog_Category] ([blog_category_id], [title]) VALUES (1, N'Category Development')
INSERT [dbo].[Blog_Category] ([blog_category_id], [title]) VALUES (2, N'business and tech')
INSERT [dbo].[Blog_Category] ([blog_category_id], [title]) VALUES (3, N'learn technology')
INSERT [dbo].[Blog_Category] ([blog_category_id], [title]) VALUES (4, N'Language')
INSERT [dbo].[Blog_Category] ([blog_category_id], [title]) VALUES (5, N'Math and Logic')
SET IDENTITY_INSERT [dbo].[Blog_Category] OFF
GO
SET IDENTITY_INSERT [dbo].[Course] ON 

INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (1, N'Lập trình C/C++', N'Trang bị cho bạn kỹ năng lập trình ngôn ngữ C/C++ từ cơ bản đến nâng cao, được minh họa thông qua các bài tập thực hành thực tế nhất về C/C++', N'Ngôn ngữ lập trình C++ được xây dựng và phát triển từ ngôn ngữ C. Ngôn ngữ C++ không phải là ngôn ngữ hướng đối tượng hoàn toàn mà là ngôn ngữ “đa hướng”. Vì C++ hỗ trợ cả lập trình hướng hành động và lập trình hướng đối tượng. Nó là một trong những ngôn ngữ phổ biến để viết các ứng dụng máy tính – và ngôn ngữ thông dụng nhất để lập trình games. 
 Khóa học sẽ trang bị cho học viên các kỹ năng lập trình được minh hoạ cụ thể bằng ngôn ngữ lập trình C/C++ từ cơ bản đến nâng cao. Khóa học bao gồm các kỹ thuật lập trình trên các kiểu dữ liệu cơ bản, các phát biểu lựa chọn, câu lệnh điều khiển, vòng lặp, mảng, con trỏ, kiểu cấu trúc. Bên cạnh đó khóa học cũng trang bị cho học viên kiến thức xử lý tập tin, cách viết chương trình theo kiểu lập trình hàm...
Qua khóa học Học lập trình C/C++ TỪ A - Z tại Unica.vn, học viên có thể tự nâng cao kỹ năng lập trình của mình, dễ dàng tiếp cận các ngôn ngữ cấp cao và công nghệ mới. Đây là khóa học tạo tiền đề tốt cho việc tiếp cận phương pháp lập trình hướng đối tượng, một phương pháp lập trình cần phải có của một lập trình viên.', 1, N'2000', N'../images/kh1.jpg', 1, NULL, 1, NULL, 2, 0)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (3, N'Lập trình C#', N' Khóa học là cẩm nang đầy đủ về C# cung cấp trọn bộ kiến thức cơ bản - có thể tạo ra một ứng dụng C# hoàn chỉnh', N'Có thể tạo ra một ứng dụng C# hoàn chỉnh sau khi hoàn thành khóa học.
 Cung cấp những cơ hội thực hành tạo ứng dụng C# ngay trong quá trình học.', 1, NULL, N'../images/kh2.jpg', 0, NULL, 1, NULL, 2, 150000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (4, N'Lập trình Java', N' Tìm hiểu cơ bản về ngôn ngữ lập trình Java ', N'Viết phần mềm quản lí cho khách hàng bằng ngôn ngữ của Java.
 Học và rèn luyện hiệu quả thông qua rất nhiều bài tập rèn luyện để nâng cao kiến thức và kỹ năng lập trình Java hiệu quả', 1, N'1200', N'../images/kh3.jpg', 1, NULL, 1, NULL, 125, 0)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (5, N'Lập trình web với C#', N'ASP.NET MVC - Một trong những framework làm việc tuyệt vời của dân lập trình web', N'Làm nền tảng cho việc nghiên cứu các kỹ thuật và công nghệ mới của Microsoft.
 Tăng khả năng thăng tiến trong công việc', 1, N'2000', N'../images/kh4.jpg', 0, NULL, 1, NULL, 6, 450000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (6, N'Lập trình web với Java', N'Spring Boot và tầm  quan trọng của Spring Boot', N'Biết cách tạo, build và run project với Spring Boot nhanh nhất.
 Nắm được cách tạo login và tạo ứng dụng CRUD MyContact với Spring Boot, Mysql', 1, NULL, N'../images/kh5.jpg', 0, NULL, 0, NULL, 125, 600000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (47, N'Lập trình Python', N'Nhanh chóng thành thạo các thao tác làm việc và áp dụng vào công việc lập trình', N'Tiếp cận và làm việc và thực hành với Python từ con số 0.
 Xây dựng Website, Game, Application hoặc Automation bằng ngôn ngữ Python', 1, NULL, N'../images/kh5.jpg', 0, NULL, 1, NULL, 119, 550000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (48, N'Lập trình Back-end với Python', N'Có nền tảng vững chắc để tham gia các dự án liên quan tới lập trình Python', N'Cơ sở để học tiếp các khóa: Cấu trúc dữ liệu, lập trình Kotlin, C#, lập trình java, lập trình Android.
 Khóa học cung cấp trọn bộ kiến thức từ cơ bản của lập trình Python,học viên có thể tạo ra một ứng dụng Python hoàn chỉnh', 1, NULL, N'../images/kh7.jpg', 0, NULL, 1, NULL, 2, 130000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (49, N'Lập trình Web', N'Tự tin ứng tuyển vào các vị trí lập trình viên Backend tại các doanh nghiệp hoặc học cao', N'Ứng dụng thành thạo lập trình Backend cho website.
 Là cách tư duy lập trình theo hướng Model - View - Controller', 1, NULL, N'../images/kh6.jpg', 1, NULL, 1, NULL, 6, 490000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (50, N'Học ngôn ngữ nhật', N'Introduction to Web Development with HTML, CSS, JavaScript', N'Want to take the first steps to become a Cloud Application Developer? This course will lead you through the languages and tools you will need to develop your own Cloud Apps.

Beginning with an explanation of how internet servers and clients work together to deliver applications to users, this course then takes you through the context for application development in the Cloud, introducing front-end, back-end, and full-stack development.

You’ll then focus on the languages you need for front-end development, working with HTML, CSS, and JavaScript.

Finally, you will discover tools that help you to store your projects and keep track of changes made to project files, such as Git and GitHub.', 2, NULL, N'../images/nn2.jpg', 0, NULL, 1, NULL, 6, 450000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (53, N'Luyện thi IELTS ', N'Financial Markets', N'An overview of the ideas, methods, and institutions that permit human society to manage risks and foster enterprise.  Emphasis on financially-savvy leadership skills. Description of practices today and analysis of prospects for the future. Introduction to risk management and behavioral finance principles to understand the real-world functioning of securities, insurance, and banking industries.  The ultimate goal of this course is using such industries effectively and towards a better society.3', 2, NULL, N'../images/kn6.jpg', 1, NULL, 1, NULL, 6, 450000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (54, N'Học ngôn ngữ hàn ', N'Modern and Contemporary Art and Design', N'This Specialization will introduce you to the art of our time. Through original films and audio, you will go behind the scenes to look closely at artworks and into studios to hear directly from artists, designers, curators and others. This Specialization is for anyone who would like to learn more about modern and contemporary art. No prior knowledge is required. Enroll to receive invitations to virtual events, gain exclusive access to MoMA resources, and share ideas with an international learner community.

Start with Modern Art & Ideas to learn how artists have taken inspiration from their environment and responded to social issues over the past 150 years. Next, take a deep dive into Seeing Through Photographs, exploring photography from its origins in the mid-1800s through the present. This course addresses the gap between seeing and truly understanding photographs by introducing ideas, approaches and technologies that inform their making.

Explore artworks made since 1980 in What Is Contemporary Art? Ranging from 3-D–printed glass and fiber sculptures to performances in a factory, the works in this course introduce you to the diverse materials, motivations and methods of artists working today. Complete the Specialization with Fashion as Design, and investigate the choices you make about fashion in relation to expression, sustainability, labor practices, identity and more. Learn from makers working with clothing every day—and, in some cases, reinventing it for the future.', 2, NULL, N'../images/nn5.jpg', 0, NULL, 1, NULL, 6, 450000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (55, N'Khóa học quay phim cơ bản', N'Chinese for Beginners', N'Nowadays, there is an increasing number of people who are interested in Chinese culture and language. And it is useful to know about the language when coming to China for travel or business. This is an ABC Chinese course for beginners, including introduction of phonetics and daily expressions. After taking this class, learners can have a basic understanding of Chinese Mandarin and make basic conversations of daily living such as exchanging personal information, talking about daily arrangements and food, asking about price, introducing the city and the weather, telling your hobbies etc. Selected topics and situations come from real life scenarios and can be used for everyday communications. In addition to the dialogues, the selection of reading materials and practice activities will make the content as rich and varied as possible, in order to stimulate the learners’ interests. This is an elementary course on Chinese speaking. The learners don’t need to study Chinese characters, so it is easier to follow and complete this course.', 3, NULL, N'../images/dp4.jpg', 0, NULL, 1, NULL, 6, 355000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (56, N'Khóa học quay phim nâng cao', N'The Data Scientist’s Toolbox', N'In this course you will get an introduction to the main tools and ideas in the data scientist''s toolbox. The course gives an overview of the data, questions, and tools that data analysts and data scientists work with. There are two components to this course. The first is a conceptual introduction to the ideas behind turning data into actionable knowledge. The second is a practical introduction to the tools that will be used in the program like version control, markdown, git, GitHub, R, and RStudio.', 3, NULL, N'../images/dp1.jpg', 1, NULL, 1, NULL, 6, 355000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (68, N'Dựng Phim Chuyên Nghiệp', N'Linux and Bash for Data Engineering', N'In this course you will get an introduction to the main tools and ideas in the data scientist''s toolbox. The course gives an overview of the data, questions, and tools that data analysts and data scientists work with. There are two components to this course. The first is a conceptual introduction to the ideas behind turning data into actionable knowledge. The second is a practical introduction to the tools that will be used in the program like version control, markdown, git, GitHub, R, and RStudio.', 3, NULL, N'../images/kh9.jpg', 0, NULL, NULL, NULL, 6, 450000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (69, N'Dựng phim bằng Adobe Premiere', N'Linux and Bash for Data Engineering', N'In this course you will get an introduction to the main tools and ideas in the data scientist''s toolbox. The course gives an overview of the data, questions, and tools that data analysts and data scientists work with. There are two components to this course. The first is a conceptual introduction to the ideas behind turning data into actionable knowledge. The second is a practical introduction to the tools that will be used in the program like version control, markdown, git, GitHub, R, and RStudio.', 3, NULL, N'../images/dp3.jpg', 1, NULL, NULL, NULL, 6, 450000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (70, N'Kỹ năng thuyết trình ', N'Linux and Bash for Data Engineering', N'In this course you will get an introduction to the main tools and ideas in the data scientist''s toolbox. The course gives an overview of the data, questions, and tools that data analysts and data scientists work with. There are two components to this course. The first is a conceptual introduction to the ideas behind turning data into actionable knowledge. The second is a practical introduction to the tools that will be used in the program like version control, markdown, git, GitHub, R, and RStudio.', 4, NULL, N'../images/kn5.jpg', 1, NULL, NULL, NULL, 119, 450000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (71, N'Kỹ năng viết', N'csLinux and Bash for Data Engineering', N'In this course you will get an introduction to the main tools and ideas in the data scientist''s toolbox. The course gives an overview of the data, questions, and tools that data analysts and data scientists work with. There are two components to this course. The first is a conceptual introduction to the ideas behind turning data into actionable knowledge. The second is a practical introduction to the tools that will be used in the program like version control, markdown, git, GitHub, R, and RStudio.', 4, NULL, N'../images/kn6.jpg', 0, NULL, NULL, NULL, 119, 355000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (72, N'Kỹ năng lắng nghe', N'Linux and Bash for Data Engineering', N'In this course you will get an introduction to the main tools and ideas in the data scientist''s toolbox. The course gives an overview of the data, questions, and tools that data analysts and data scientists work with. There are two components to this course. The first is a conceptual introduction to the ideas behind turning data into actionable knowledge. The second is a practical introduction to the tools that will be used in the program like version control, markdown, git, GitHub, R, and RStudio.', 4, NULL, N'../images/kn2.jpg', 0, NULL, NULL, NULL, 119, 450000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (73, N'Kỹ năng quản lý ', N'Building Web Applications in PHP.', N'In this course, you''ll explore the basic structure of a web application, and how a web browser interacts with a web server. You''ll be introduced to the request/response cycle, including GET/POST/Redirect. You''ll also gain an introductory understanding of Hypertext Markup Language (HTML), as well as the basic syntax and data structures of the PHP language, variables, logic, iteration, arrays, error handling, and superglobal variables, among other elements. An introduction to Cascading Style Sheets (CSS) will allow you to style markup for webpages. Lastly, you''ll gain the skills and knowledge to install and use an integrated PHP/MySQL environment like XAMPP or MAMP.', 4, NULL, N'../images/kn3.jpg', 0, NULL, 1, NULL, 119, 450000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (78, N'Kỹ năng làm chủ cảm xúc', N'Linux and Bash for Data Engineering', N'In this second course of the Python, Bash and SQL Essentials for Data Engineering Specialization, you will learn the fundamentals of Linux necessary to perform data engineering tasks. Additionally, you will explore how to use both Bash and zsh configurations, and develop the syntax needed to interact and control Linux. These skills will allow you to manage and manipulate databases in a Bash environment.', 4, NULL, N'../images/kn1.jpg', 0, NULL, 1, NULL, 119, 355000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (79, N'Học nấu món Âu', N'Financial Markets', N'An overview of the ideas, methods, and institutions that permit human society to manage risks and foster enterprise.  Emphasis on financially-savvy leadership skills. Description of practices today and analysis of prospects for the future. Introduction to risk management and behavioral finance principles to understand the real-world functioning of securities, insurance, and banking industries.  The ultimate goal of this course is using such industries effectively and towards a better society.3', 5, NULL, N'../images/food5.jpg', 1, NULL, 1, NULL, 125, 450000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (82, N'Học nấu món Pháp', N'Chinese for Beginners', N'Nowadays, there is an increasing number of people who are interested in Chinese culture and language. And it is useful to know about the language when coming to China for travel or business. This is an ABC Chinese course for beginners, including introduction of phonetics and daily expressions. After taking this class, learners can have a basic understanding of Chinese Mandarin and make basic conversations of daily living such as exchanging personal information, talking about daily arrangements and food, asking about price, introducing the city and the weather, telling your hobbies etc. Selected topics and situations come from real life scenarios and can be used for everyday communications. In addition to the dialogues, the selection of reading materials and practice activities will make the content as rich and varied as possible, in order to stimulate the learners’ interests. This is an elementary course on Chinese speaking. The learners don’t need to study Chinese characters, so it is easier to follow and complete this course.', 5, NULL, N'../images/food9.jpg', 0, NULL, 1, NULL, 125, 450000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (83, N'Học nấu món việt', N'The Data Scientist’s Toolbox', N'In this course you will get an introduction to the main tools and ideas in the data scientist''s toolbox. The course gives an overview of the data, questions, and tools that data analysts and data scientists work with. There are two components to this course. The first is a conceptual introduction to the ideas behind turning data into actionable knowledge. The second is a practical introduction to the tools that will be used in the program like version control, markdown, git, GitHub, R, and RStudio.', 5, NULL, N'../images/food1.jpg', 0, NULL, 1, NULL, 125, 450000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (84, N'Học nẩu món Nhật', N'Web Design for Everybody: Basics of Web Development & Coding ', N'This Specialization covers how to write syntactically correct HTML5 and CSS3, and how to create interactive web experiences with JavaScript. Mastering this range of technologies will allow you to develop high quality web sites that, work seamlessly on mobile, tablet, and large screen browsers accessible. During the capstone you will develop a professional-quality web portfolio demonstrating your growth as a web developer and your knowledge of accessible web design. This will include your ability to design and implement a responsive site that utilizes tools to create a site that is accessible to a wide audience, including those with visual, audial, physical, and cognitive impairments.', 5, NULL, N'../images/food5.jpg', 0, NULL, 1, NULL, 125, 355000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (85, N'Học nấu món Hàn ', N'Building Web Applications in PHP.', N'In this course, you''ll explore the basic structure of a web application, and how a web browser interacts with a web server. You''ll be introduced to the request/response cycle, including GET/POST/Redirect. You''ll also gain an introductory understanding of Hypertext Markup Language (HTML), as well as the basic syntax and data structures of the PHP language, variables, logic, iteration, arrays, error handling, and superglobal variables, among other elements. An introduction to Cascading Style Sheets (CSS) will allow you to style markup for webpages. Lastly, you''ll gain the skills and knowledge to install and use an integrated PHP/MySQL environment like XAMPP or MAMP.', 5, NULL, N'../images/food7.jpg', 0, NULL, 1, NULL, 125, 355000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (90, N'Chuẩn cơm mẹ nấu', N'Linux and Bash for Data Engineering', N'In this second course of the Python, Bash and SQL Essentials for Data Engineering Specialization, you will learn the fundamentals of Linux necessary to perform data engineering tasks. Additionally, you will explore how to use both Bash and zsh configurations, and develop the syntax needed to interact and control Linux. These skills will allow you to manage and manipulate databases in a Bash environment.', 5, NULL, N'../images/food10.jpg', 1, NULL, 1, NULL, 125, 355000)
INSERT [dbo].[Course] ([course_id], [title], [short_description], [description], [category_id], [tag_line], [thumbnail], [is_free_course], [updated_date], [status], [featured_flag], [owner], [price]) VALUES (91, N'Khởi Nghiệp Thời 4.0', N'Financial Markets', N'An overview of the ideas, methods, and institutions that permit human society to manage risks and foster enterprise.  Emphasis on financially-savvy leadership skills. Description of practices today and analysis of prospects for the future. Introduction to risk management and behavioral finance principles to understand the real-world functioning of securities, insurance, and banking industries.  The ultimate goal of this course is using such industries effectively and towards a better society.3', 6, NULL, N'../images/kh11.jpg', NULL, NULL, 1, NULL, 125, 450000)
SET IDENTITY_INSERT [dbo].[Course] OFF
GO
SET IDENTITY_INSERT [dbo].[Course_Category] ON 

INSERT [dbo].[Course_Category] ([id], [name], [thumbnail]) VALUES (1, N'Lập trình', N'Null')
INSERT [dbo].[Course_Category] ([id], [name], [thumbnail]) VALUES (2, N'Ngoại Ngữ', N'null')
INSERT [dbo].[Course_Category] ([id], [name], [thumbnail]) VALUES (3, N'Nhiếp ảnh, Dựng Phim', NULL)
INSERT [dbo].[Course_Category] ([id], [name], [thumbnail]) VALUES (4, N'Kỹ năng mềm', NULL)
INSERT [dbo].[Course_Category] ([id], [name], [thumbnail]) VALUES (5, N'Nấu ăn', NULL)
INSERT [dbo].[Course_Category] ([id], [name], [thumbnail]) VALUES (6, N'Kinh doanh', NULL)
SET IDENTITY_INSERT [dbo].[Course_Category] OFF
GO
SET IDENTITY_INSERT [dbo].[Enrol] ON 

INSERT [dbo].[Enrol] ([enrol_id], [user_id], [course_id], [date_added], [status]) VALUES (1, 4, 1, NULL, NULL)
INSERT [dbo].[Enrol] ([enrol_id], [user_id], [course_id], [date_added], [status]) VALUES (4, 4, 3, NULL, NULL)
INSERT [dbo].[Enrol] ([enrol_id], [user_id], [course_id], [date_added], [status]) VALUES (3, 4, 4, NULL, NULL)
INSERT [dbo].[Enrol] ([enrol_id], [user_id], [course_id], [date_added], [status]) VALUES (11, 4, 6, NULL, NULL)
INSERT [dbo].[Enrol] ([enrol_id], [user_id], [course_id], [date_added], [status]) VALUES (6, 8, 1, NULL, NULL)
INSERT [dbo].[Enrol] ([enrol_id], [user_id], [course_id], [date_added], [status]) VALUES (5, 8, 3, NULL, NULL)
INSERT [dbo].[Enrol] ([enrol_id], [user_id], [course_id], [date_added], [status]) VALUES (10, 9, 1, NULL, NULL)
INSERT [dbo].[Enrol] ([enrol_id], [user_id], [course_id], [date_added], [status]) VALUES (7, 9, 4, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Enrol] OFF
GO
SET IDENTITY_INSERT [dbo].[Lesson] ON 

INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (2, N'Giới thiệu khóa học', 1, 1, N'https://www.youtube.com/embed/ILr_rU-lISk', N'Chương 1', NULL, N'I originally placed these optional resources at the end of Week Two.', 1)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (3, N'Biến và hằng trong C', 1, 1, N'https://www.youtube.com/embed/q8UGqw6Cbnk', N'Chương 1', NULL, N'I originally placed these optional resources at the end of Week Two.', 1)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (4, N'Các kiểu dữ liệu trong C', 1, 1, N'https://www.youtube.com/embed/jtS7g65zrWU', N'Chương 1', NULL, N'My css folder is in the SAME directory as my html files.

My link will be: <link rel="stylesheet" href="css/style.css">', 1)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (5, N'Hiện tượng tràn số trong C', 1, 1, N'https://www.youtube.com/embed/nQ93_TpS14M', N'Chương 1', NULL, NULL, 1)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (6, N'Nhập xuất trong C', 1, 1, N'https://www.youtube.com/embed/AlxkkNlVQd8', N'Chương 1', NULL, NULL, 0)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (7, N'Toán tử trong C', 1, 1, N'https://www.youtube.com/embed/3BOza8aCxNs', N'Chương 1', NULL, NULL, 1)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (8, N'Cấu trúc điều khiển if else', 1, 2, N'https://www.youtube.com/embed/i3nJyEt42Y8', N'Chương 2', NULL, NULL, 1)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (9, N'Vòng lặp for trong C', 1, 2, N'https://www.youtube.com/embed/aL59MpOFMe0', N'Chương 2', NULL, NULL, 1)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (10, N'Vòng lặp while và do … while', 1, 2, N'https://www.youtube.com/embed/i3nJyEt42Y8', N'Chương 2', NULL, NULL, 0)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (11, N'Sự linh hoạt trong sử dụng vòng lặp', 1, 2, N'https://www.youtube.com/embed/i3nJyEt42Y8', N'Chương 2', NULL, NULL, 1)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (12, N'Một số hàm trong thư viện toán học math.h', 1, 3, N'https://www.youtube.com/embed/xt5rScAb9lI', N'Chương 3', NULL, NULL, 1)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (13, N'Hàm người dùng định nghĩa', 1, 3, N'https://www.youtube.com/embed/4i--rEoaMn8', N'Chương 3', NULL, NULL, 0)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (14, N'Hàm đệ quy', 1, 3, N'https://www.youtube.com/embed/4i--rEoaMn8', N'Chương 3', NULL, NULL, 0)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (15, N'Nhập xuất mảng 1 chiều trong C', 1, 3, N'https://www.youtube.com/embed/cgKiUF98fLo', N'Chương 3', NULL, NULL, 1)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (16, N'Thêm, xóa phần tử trong mảng 1 chiều', 1, 4, N'https://www.youtube.com/embed/cgKiUF98fLo', N'Chương 4', NULL, NULL, 0)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (17, N'Nhập xuất chuỗi trong C', 1, 4, N'https://www.youtube.com/embed/gPzzRrQ5SfY', N'Chương 4', NULL, NULL, 0)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (18, N'Các hàm trong thư viện string.h', 1, 5, N'https://www.youtube.com/embed/gXhS1LI2Y1k', N'Chương 5', NULL, NULL, 1)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (19, N'Các hàm trong thư viện string.h', 1, 5, N'https://www.youtube.com/embed/gXhS1LI2Y1k', N'Chương 5', NULL, NULL, 1)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (20, N'Đảo ngược chuỗi trong C', 1, 5, N'https://www.youtube.com/embed/4u0RdM7xub8', N'Chương 5', NULL, NULL, 1)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (21, N'Con trỏ trong C', 1, 6, N'https://www.youtube.com/embed/4u0RdM7xub8', N'Chương 6', NULL, NULL, 1)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (22, N'Mối liên hệ giữa con trỏ và mảng trong C', 1, 6, N'https://www.youtube.com/embed/4u0RdM7xub8', N'Chương 6', NULL, NULL, 1)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (28, N' C# là gì ', 3, 7, N'https://www.youtube.com/embed/9kohr6pMwag', N'Chương 1', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (29, N'Cấu trúc lệnh cơ bản', 3, 7, N'https://www.youtube.com/embed/FhAIc0tlyaQ', N'Chương 1', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (30, N'Nhập xuất cơ bản', 3, 7, N'https://www.youtube.com/embed/BAscPWPtCD8', N'Chương 1', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (31, N'Biến trong C#', 3, 8, N'https://www.youtube.com/embed/IEz7uMSHitM', N'Chương 2', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (32, N'Kiểu dữ liệu trong C#', 3, 8, N'https://www.youtube.com/embed/yrH7Qe8FXqE', N'Chương 2', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (33, N'Toán tử trong C#', 3, 9, N'https://www.youtube.com/embed/niz7Gg8uB-k', N'Chương 3', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (34, N'Hằng', 3, 9, N'https://www.youtube.com/embed/13NRSYgKh0o', N'Chương 3', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (35, N'Giới thiệu Java', 4, 10, N'https://www.youtube.com/embed/3gtOAlcovoQ', N'Chương 1', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (36, N'Cài đặt môi trường Java ', 4, 10, N'https://www.youtube.com/embed/KjMRn1YQcLc', N'Chương 1', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (37, N'Chương trình Java đầu tiên', 4, 11, N'https://www.youtube.com/embed/KjMRn1YQcLc', N'Chương 2', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (38, N'Biến trong Java ', 4, 11, N'https://www.youtube.com/embed/G2mCSTtBojM', N'Chương 2', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (39, N'Kiểu dữ liệu trong Java', 4, 12, N'https://www.youtube.com/embed/4k_5vWY2wps', N'Chương 3', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (40, N'Toán tử trong Java', 4, 12, N'https://www.youtube.com/embed/H9FmP010A_Q', N'Chương 3', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (41, N'Hằng trong Java', 5, 13, N'https://www.youtube.com/embed/dqybUkGCaVw', N'Chương 1', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (42, N'Ép kiểu trong Java ', 5, 13, N'https://www.youtube.com/embed/kOMiIKLCK34', N'Chương 1', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (43, N'Cấu trúc rẽ nhánh trong Java', 5, 14, N'https://www.youtube.com/embed/vradAZcby8I', N'Chương 2', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (44, N'Vòng lặp While trong Java', 5, 14, N'https://www.youtube.com/embed/tDfQ33fmmvs', N'Chương 2', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (45, N'Mảng trong Java', 5, 15, N'https://www.youtube.com/embed/1QVfZFOt7uI', N'Chương 3', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (46, N'Foreach trong Java', 5, 15, N'https://www.youtube.com/embed/SVnPYiHS68U', N'Chương 3', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (47, N'Giới thiệu ngôn ngữ lập trình Python', 6, 16, N'https://www.youtube.com/embed/NZj6LI5a9vc', N'Chương 1', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (48, N'Cài đặt môi trường Python', 6, 16, N'https://www.youtube.com/embed/jf-q_dG8WzI', N'Chương 1', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (49, N'Biến(Variable) trong Python ', 6, 17, N'https://www.youtube.com/embed/nclE18Yl-kA', N'Chương 2', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (50, N'Kiểu số trong Python ', 6, 17, N'https://www.youtube.com/embed/IAVvgqDBiv0', N'Chương 2', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (51, N'Kiểu chuỗi trong Python', 6, 18, N'https://www.youtube.com/embed/Vb6XWSLPQfg', N'Chương 3', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (52, N' List trong Python', 6, 18, N'https://www.youtube.com/embed/u2Kd3weqPZE', N'Chương 3', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (53, N'Tuple trong Python', 47, 19, N'https://www.youtube.com/embed/dDFFCbRGm3o', N'Chương 1', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (54, N'Hashable và Unhashable trong Python', 47, 19, N'https://www.youtube.com/embed/gw9zbl2Q7r4', N'Chương 1', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (55, N'Hàm xuất trong Python', 47, 20, N'https://www.youtube.com/embed/rhOyCSIf1is', N'Chương 2', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (56, N'Hàm nhập trong Python', 47, 20, N'https://www.youtube.com/embed/rK4MphZVhDM', N'Chương 2', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (57, N'Function trong python - Sơ lược về hàm', 47, 21, N'https://www.youtube.com/embed/a6FnNvt3Fw4', N'Chương 3', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (58, N'Function trong python - Positional', 47, 21, N'https://www.youtube.com/embed/M77p3PB-qzM', N'Chương 3', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (59, N'Spring boot là gì', 48, 22, N'https://www.youtube.com/embed/GBOMXhDLxmc', N'Chương 1', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (60, N'Restful api web service trong java web spring boot phần 1', 48, 22, N'https://www.youtube.com/embed/Ptjqr1HPgPk', N'Chương 1', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (61, N'Tạo ứng dụng Spring Boot ', 48, 23, N'https://www.youtube.com/embed/CZJTBlEUvls', N'Chương 2', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (62, N'Giới thiệu về Controller của Spring MVC', 48, 23, N'https://www.youtube.com/embed/SGUbh6hjVdE', N'Chương 2', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (63, N'Đọc dữ liệu trong action', 48, 24, N'https://www.youtube.com/embed/nPN0AD_dlrU', N'Chương 3', NULL, NULL, NULL)
INSERT [dbo].[Lesson] ([lesson_id], [title], [course_id], [topic_id], [video_url], [lesson_type], [attachment], [summary], [status]) VALUES (64, N'Gọi các action của controller theo phương thức GET hoặc POST', 48, 24, N'https://www.youtube.com/embed/aP0lDxgmeps', N'Chương 3', NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Lesson] OFF
GO
SET IDENTITY_INSERT [dbo].[Price_Package] ON 

INSERT [dbo].[Price_Package] ([pricePackage_id], [name], [access_duration], [status], [list_price], [sale_price], [description], [course_id]) VALUES (1, N'Gói truy cập 3 tháng', 3, 1, 250000, 150000, N'acc', 1)
INSERT [dbo].[Price_Package] ([pricePackage_id], [name], [access_duration], [status], [list_price], [sale_price], [description], [course_id]) VALUES (2, N'Gói truy cập 6 tháng', 6, 0, 500000, 450000, N'acc', 1)
INSERT [dbo].[Price_Package] ([pricePackage_id], [name], [access_duration], [status], [list_price], [sale_price], [description], [course_id]) VALUES (3, N'Gói vô thời hạn', 99999, 1, 1000000, 600000, N'ttt', 1)
SET IDENTITY_INSERT [dbo].[Price_Package] OFF
GO
SET IDENTITY_INSERT [dbo].[Rating] ON 

INSERT [dbo].[Rating] ([id], [rating], [user_id], [course_id], [review]) VALUES (2, 4, 4, 1, N'acc')
INSERT [dbo].[Rating] ([id], [rating], [user_id], [course_id], [review]) VALUES (3, 4, 8, 1, N'acc')
INSERT [dbo].[Rating] ([id], [rating], [user_id], [course_id], [review]) VALUES (4, 3, 9, 5, N'acc')
INSERT [dbo].[Rating] ([id], [rating], [user_id], [course_id], [review]) VALUES (5, 4, 4, 3, N'acc')
INSERT [dbo].[Rating] ([id], [rating], [user_id], [course_id], [review]) VALUES (6, 3, 4, 4, N'acc')
INSERT [dbo].[Rating] ([id], [rating], [user_id], [course_id], [review]) VALUES (7, 4, 4, 3, N'acc')
INSERT [dbo].[Rating] ([id], [rating], [user_id], [course_id], [review]) VALUES (8, 5, 9, 1, N'acc')
INSERT [dbo].[Rating] ([id], [rating], [user_id], [course_id], [review]) VALUES (9, 5, 8, 1, N'acc')
INSERT [dbo].[Rating] ([id], [rating], [user_id], [course_id], [review]) VALUES (10, 4, 4, 4, N'acc')
INSERT [dbo].[Rating] ([id], [rating], [user_id], [course_id], [review]) VALUES (11, 4, 9, 5, N'acc')
INSERT [dbo].[Rating] ([id], [rating], [user_id], [course_id], [review]) VALUES (12, 3, 8, 1, N'acc')
INSERT [dbo].[Rating] ([id], [rating], [user_id], [course_id], [review]) VALUES (13, 5, 4, 5, N'acc')
INSERT [dbo].[Rating] ([id], [rating], [user_id], [course_id], [review]) VALUES (14, 4, 8, 4, N'acc')
INSERT [dbo].[Rating] ([id], [rating], [user_id], [course_id], [review]) VALUES (15, 5, 4, 1, N'acc')
INSERT [dbo].[Rating] ([id], [rating], [user_id], [course_id], [review]) VALUES (16, 4, 4, 5, N'acc')
INSERT [dbo].[Rating] ([id], [rating], [user_id], [course_id], [review]) VALUES (17, 3, 8, 6, N'acc')
INSERT [dbo].[Rating] ([id], [rating], [user_id], [course_id], [review]) VALUES (18, 2, 4, 4, N'acc')
INSERT [dbo].[Rating] ([id], [rating], [user_id], [course_id], [review]) VALUES (19, 4, 8, 3, N'acc')
INSERT [dbo].[Rating] ([id], [rating], [user_id], [course_id], [review]) VALUES (20, 5, 4, 5, N'acc')
INSERT [dbo].[Rating] ([id], [rating], [user_id], [course_id], [review]) VALUES (21, 4, 9, 6, N'acc')
INSERT [dbo].[Rating] ([id], [rating], [user_id], [course_id], [review]) VALUES (22, 3, 8, 4, N'acc')
INSERT [dbo].[Rating] ([id], [rating], [user_id], [course_id], [review]) VALUES (23, 5, 9, 5, N'acc')
SET IDENTITY_INSERT [dbo].[Rating] OFF
GO
SET IDENTITY_INSERT [dbo].[Role] ON 

INSERT [dbo].[Role] ([role_id], [name]) VALUES (1, N'Admin')
INSERT [dbo].[Role] ([role_id], [name]) VALUES (2, N'Expert')
INSERT [dbo].[Role] ([role_id], [name]) VALUES (3, N'Sale')
INSERT [dbo].[Role] ([role_id], [name]) VALUES (4, N'Marketing')
INSERT [dbo].[Role] ([role_id], [name]) VALUES (5, N'Customer')
INSERT [dbo].[Role] ([role_id], [name]) VALUES (6, N'Guest')
SET IDENTITY_INSERT [dbo].[Role] OFF
GO
SET IDENTITY_INSERT [dbo].[Subject_Dimension] ON 

INSERT [dbo].[Subject_Dimension] ([dimension_id], [type], [name], [description], [course_id]) VALUES (1, N'Domain', N'Business', N'&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; abc&nbsp;', 1)
INSERT [dbo].[Subject_Dimension] ([dimension_id], [type], [name], [description], [course_id]) VALUES (2, N'Domain', N'People', N'scc', 1)
INSERT [dbo].[Subject_Dimension] ([dimension_id], [type], [name], [description], [course_id]) VALUES (3, N'Domain', N'Process', N'scc', 1)
INSERT [dbo].[Subject_Dimension] ([dimension_id], [type], [name], [description], [course_id]) VALUES (4, N'Group', N'Intilating', N'scc', 4)
INSERT [dbo].[Subject_Dimension] ([dimension_id], [type], [name], [description], [course_id]) VALUES (5, N'Group', N'Planning', N'erre', 4)
INSERT [dbo].[Subject_Dimension] ([dimension_id], [type], [name], [description], [course_id]) VALUES (6, N'Group', N'Executing', N'mmm', 1)
INSERT [dbo].[Subject_Dimension] ([dimension_id], [type], [name], [description], [course_id]) VALUES (7, N'fresheri', N'zzzi', N'<p>zzz</p>', 4)
INSERT [dbo].[Subject_Dimension] ([dimension_id], [type], [name], [description], [course_id]) VALUES (26, N'Domainn', N'ds', N'd', 1)
INSERT [dbo].[Subject_Dimension] ([dimension_id], [type], [name], [description], [course_id]) VALUES (27, N'Domainn', N'sa', N'as', 4)
INSERT [dbo].[Subject_Dimension] ([dimension_id], [type], [name], [description], [course_id]) VALUES (28, N'Domainn', N'ewq', N'ewq', 4)
INSERT [dbo].[Subject_Dimension] ([dimension_id], [type], [name], [description], [course_id]) VALUES (32, N'Domainn', N'fds', N'sdf', 1)
SET IDENTITY_INSERT [dbo].[Subject_Dimension] OFF
GO
SET IDENTITY_INSERT [dbo].[Topic] ON 

INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (1, N'Giới thiệu về ngôn ngữ lập trình C', 1)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (2, N'Cấu trúc điều khiển & rẽ nhánh', 1)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (3, N'Hàm trong C', 1)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (4, N'Mảng trong C', 1)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (5, N'Chuỗi trong C', 1)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (6, N'Con trỏ trong C', 1)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (7, N'Giới thiệu về C#', 3)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (8, N'Kiểu dữ liệu,Biến và biểu thức', 3)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (9, N'Các phép toán trong C#', 3)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (10, N'Giới thiệu ngôn ngữ lập trình Java', 4)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (11, N'Giới thiệu ngôn ngữ lập trình Java', 4)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (12, N'Xử lý lỗi trong Java', 4)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (13, N'Các thư viện quan trọng thường dùng trong Java/Android', 5)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (14, N'Xử lý chuỗi trong Java', 5)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (15, N'Xử lý mảng trong Java', 5)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (16, N'Các khái niệm cơ bản trong lập trình Python', 6)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (17, N'Biểu thức trong lập trình Python', 6)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (18, N'Đối tượng String trong lập trình Python', 6)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (19, N'Đối tượng List, Tuples, Sets và Dictionary', 47)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (20, N'Thao tác với hàm tự định nghĩa trong lập trình Python', 47)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (21, N'Hướng đối tượng và thao tác với File trong Python', 47)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (22, N'Cấu hình Environment về Spring Boot', 48)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (23, N'Tạo project hello đầu tiên với Spring Boot', 48)
INSERT [dbo].[Topic] ([topic_id], [title], [course_id]) VALUES (24, N'Tích hợp Thymeleaf template vào Spring Boot', 48)
SET IDENTITY_INSERT [dbo].[Topic] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([user_id], [fullname], [gender], [email], [phone], [password], [role_id], [image], [status], [featured], [hash], [active], [address], [description]) VALUES (1, N'Nguyen Cuongg', 1, N'cuongnmc00@gmail.com', N'0982468398', N'123', 1, N'https://yt3.ggpht.com/a/AATXAJxQKukYxK9pjbXPUrzEChNRF-ZVVPQd-4zg2Q=s900-c-k-c0xffffffff-no-rj-mo', 1, 1, N'9da9f4840c0eca5edbbcc024661a37f5', 1, NULL, NULL)
INSERT [dbo].[User] ([user_id], [fullname], [gender], [email], [phone], [password], [role_id], [image], [status], [featured], [hash], [active], [address], [description]) VALUES (2, N'Long Dinh', 1, N'long@gmail.com', N'0938293093', N'123', 2, N'https://th.bing.com/th/id/R.c468d31c314f47b22634d571c0909c74?rik=tYYmcIKWBRD%2fCw&pid=ImgRaw&r=0', 1, NULL, NULL, NULL, NULL, N'Giáo viên được học sinh yêu mến với 10 năm kinh nghiệm')
INSERT [dbo].[User] ([user_id], [fullname], [gender], [email], [phone], [password], [role_id], [image], [status], [featured], [hash], [active], [address], [description]) VALUES (4, N'Hoang Linhh', 0, N'linh@gmail.com', N'06938193821', N'1234', 5, N'../images/avatar/NQA_8601.JPG', 1, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([user_id], [fullname], [gender], [email], [phone], [password], [role_id], [image], [status], [featured], [hash], [active], [address], [description]) VALUES (6, N'Dao Khai', 1, N'khai@gmail.com', N'0123456789', N'123', 2, N'http://lavenderstudio.com.vn/wp-content/uploads/2018/05/dich-vu-chup-anh-chan-dung-dep-20.jpg', 1, NULL, NULL, NULL, NULL, N'CEO tập đoàn FPT với vốn kiến thức phong phú và trình độ cao')
INSERT [dbo].[User] ([user_id], [fullname], [gender], [email], [phone], [password], [role_id], [image], [status], [featured], [hash], [active], [address], [description]) VALUES (8, N'Ha Nguyen', 0, N'ha@gmail.com', N'012345798', N'1234', 5, N'../images/avatar/NQA_9085.JPG', 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([user_id], [fullname], [gender], [email], [phone], [password], [role_id], [image], [status], [featured], [hash], [active], [address], [description]) VALUES (9, N'hoang', 1, N'hoang@gmail.com', N'0123465789', N'123', 5, N'../images/1.png', 1, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([user_id], [fullname], [gender], [email], [phone], [password], [role_id], [image], [status], [featured], [hash], [active], [address], [description]) VALUES (114, N'Han', 0, N'jh@gmail.com', N'0577656332', N'yX$5z$', 5, NULL, 1, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([user_id], [fullname], [gender], [email], [phone], [password], [role_id], [image], [status], [featured], [hash], [active], [address], [description]) VALUES (119, N'Nam Cao', 1, N'nam@gmail.com', N'0986757476', N'123', 2, N'https://th.bing.com/th/id/R.da66ee3ea1be278dcd0c2cf789be0c10?rik=GZiGf12OZ6L7Vw&pid=ImgRaw&r=0', 1, NULL, NULL, NULL, NULL, N'Giáo sư đạt giải nobel  Toán học, được trao tặng giải Nghiên cứu Clay của Viện Toán học Clay cùng với Giáo sư Gérard Laumon vì đã chứng minh được Bổ đề cơ bản cho các nhóm Unita. Năm 2005, khi được 33 tuổi, năm 2009, công trình Bổ đề cơ bản cho các đại số Lie của ông đã được tạp chí Time bình chọn là một trong 10 phát minh khoa học tiêu biểu của năm 2009')
INSERT [dbo].[User] ([user_id], [fullname], [gender], [email], [phone], [password], [role_id], [image], [status], [featured], [hash], [active], [address], [description]) VALUES (120, N'Hong', 0, N'hong@gmail.com', N'0865356454', N'123', 5, NULL, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([user_id], [fullname], [gender], [email], [phone], [password], [role_id], [image], [status], [featured], [hash], [active], [address], [description]) VALUES (124, N'Mai Dao', 0, N'maidao@gmail.com', N'032432134', N'123', 5, NULL, 1, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([user_id], [fullname], [gender], [email], [phone], [password], [role_id], [image], [status], [featured], [hash], [active], [address], [description]) VALUES (125, N'Hanh Nguyen', 1, N'hanh@gmail.com', N'032432134', N'123', 2, N'https://th.bing.com/th/id/OIP.J0hsWabKojM_JNCbGKggEgAAAA?pid=ImgDet&rs=1', 0, NULL, NULL, NULL, NULL, N'Trẻ trung, xinh đẹp, tâm huyết và rất được lòng học sinh')
INSERT [dbo].[User] ([user_id], [fullname], [gender], [email], [phone], [password], [role_id], [image], [status], [featured], [hash], [active], [address], [description]) VALUES (126, N'Phuc Nguyen', 1, N'phuc@gmail.com', N'092432431', N'123', 5, NULL, 1, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([user_id], [fullname], [gender], [email], [phone], [password], [role_id], [image], [status], [featured], [hash], [active], [address], [description]) VALUES (127, N'Vuong', 1, N'vuong@gmail.com', N'0982468398', N'sL@7pP', 2, NULL, 1, NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[User] ([user_id], [fullname], [gender], [email], [phone], [password], [role_id], [image], [status], [featured], [hash], [active], [address], [description]) VALUES (128, N'Nguyễn Thu Linh', 1, N'cuongnmc@gmail.com', N'0982468398', N'123', 5, NULL, 1, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[User] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ_email]    Script Date: 11/11/2022 8:59:31 CH ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [UQ_email] UNIQUE NONCLUSTERED 
(
	[email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Blog]  WITH CHECK ADD  CONSTRAINT [FK_Blog_Blog_Category] FOREIGN KEY([blog_category_id])
REFERENCES [dbo].[Blog_Category] ([blog_category_id])
GO
ALTER TABLE [dbo].[Blog] CHECK CONSTRAINT [FK_Blog_Blog_Category]
GO
ALTER TABLE [dbo].[Blog]  WITH CHECK ADD  CONSTRAINT [FK_Blog_User] FOREIGN KEY([user_id])
REFERENCES [dbo].[User] ([user_id])
GO
ALTER TABLE [dbo].[Blog] CHECK CONSTRAINT [FK_Blog_User]
GO
ALTER TABLE [dbo].[Blog_Comment]  WITH CHECK ADD  CONSTRAINT [FK_Blog_Comment_Blog] FOREIGN KEY([blog_id])
REFERENCES [dbo].[Blog] ([blog_id])
GO
ALTER TABLE [dbo].[Blog_Comment] CHECK CONSTRAINT [FK_Blog_Comment_Blog]
GO
ALTER TABLE [dbo].[Blog_Comment]  WITH CHECK ADD  CONSTRAINT [FK_Blog_Comment_User] FOREIGN KEY([user_id])
REFERENCES [dbo].[User] ([user_id])
GO
ALTER TABLE [dbo].[Blog_Comment] CHECK CONSTRAINT [FK_Blog_Comment_User]
GO
ALTER TABLE [dbo].[Course]  WITH CHECK ADD  CONSTRAINT [FK_Course_Course_Category] FOREIGN KEY([category_id])
REFERENCES [dbo].[Course_Category] ([id])
GO
ALTER TABLE [dbo].[Course] CHECK CONSTRAINT [FK_Course_Course_Category]
GO
ALTER TABLE [dbo].[Course]  WITH CHECK ADD  CONSTRAINT [FK_Course_User] FOREIGN KEY([owner])
REFERENCES [dbo].[User] ([user_id])
GO
ALTER TABLE [dbo].[Course] CHECK CONSTRAINT [FK_Course_User]
GO
ALTER TABLE [dbo].[Enrol]  WITH CHECK ADD  CONSTRAINT [FK_Enrol_Course] FOREIGN KEY([course_id])
REFERENCES [dbo].[Course] ([course_id])
GO
ALTER TABLE [dbo].[Enrol] CHECK CONSTRAINT [FK_Enrol_Course]
GO
ALTER TABLE [dbo].[Enrol]  WITH CHECK ADD  CONSTRAINT [FK_Enrol_User] FOREIGN KEY([user_id])
REFERENCES [dbo].[User] ([user_id])
GO
ALTER TABLE [dbo].[Enrol] CHECK CONSTRAINT [FK_Enrol_User]
GO
ALTER TABLE [dbo].[Lesson]  WITH CHECK ADD  CONSTRAINT [FK_Lesson_Course] FOREIGN KEY([course_id])
REFERENCES [dbo].[Course] ([course_id])
GO
ALTER TABLE [dbo].[Lesson] CHECK CONSTRAINT [FK_Lesson_Course]
GO
ALTER TABLE [dbo].[Lesson]  WITH CHECK ADD  CONSTRAINT [FK_Lesson_Topic] FOREIGN KEY([topic_id])
REFERENCES [dbo].[Topic] ([topic_id])
GO
ALTER TABLE [dbo].[Lesson] CHECK CONSTRAINT [FK_Lesson_Topic]
GO
ALTER TABLE [dbo].[Payment]  WITH CHECK ADD  CONSTRAINT [FK_Payment_Course] FOREIGN KEY([course_id])
REFERENCES [dbo].[Course] ([course_id])
GO
ALTER TABLE [dbo].[Payment] CHECK CONSTRAINT [FK_Payment_Course]
GO
ALTER TABLE [dbo].[Payment]  WITH CHECK ADD  CONSTRAINT [FK_Payment_User] FOREIGN KEY([user_id])
REFERENCES [dbo].[User] ([user_id])
GO
ALTER TABLE [dbo].[Payment] CHECK CONSTRAINT [FK_Payment_User]
GO
ALTER TABLE [dbo].[Price_Package]  WITH CHECK ADD  CONSTRAINT [FK_Price_Package_Course] FOREIGN KEY([course_id])
REFERENCES [dbo].[Course] ([course_id])
GO
ALTER TABLE [dbo].[Price_Package] CHECK CONSTRAINT [FK_Price_Package_Course]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FK_Question_Quiz] FOREIGN KEY([quiz_id])
REFERENCES [dbo].[Quiz] ([quiz_id])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FK_Question_Quiz]
GO
ALTER TABLE [dbo].[Quiz]  WITH CHECK ADD  CONSTRAINT [FK_Quiz_Course] FOREIGN KEY([course_id])
REFERENCES [dbo].[Course] ([course_id])
GO
ALTER TABLE [dbo].[Quiz] CHECK CONSTRAINT [FK_Quiz_Course]
GO
ALTER TABLE [dbo].[Rating]  WITH CHECK ADD  CONSTRAINT [FK_Rating_Course] FOREIGN KEY([course_id])
REFERENCES [dbo].[Course] ([course_id])
GO
ALTER TABLE [dbo].[Rating] CHECK CONSTRAINT [FK_Rating_Course]
GO
ALTER TABLE [dbo].[Rating]  WITH CHECK ADD  CONSTRAINT [FK_Rating_User] FOREIGN KEY([user_id])
REFERENCES [dbo].[User] ([user_id])
GO
ALTER TABLE [dbo].[Rating] CHECK CONSTRAINT [FK_Rating_User]
GO
ALTER TABLE [dbo].[Setting]  WITH CHECK ADD  CONSTRAINT [FK_Setting_User] FOREIGN KEY([user_id])
REFERENCES [dbo].[User] ([user_id])
GO
ALTER TABLE [dbo].[Setting] CHECK CONSTRAINT [FK_Setting_User]
GO
ALTER TABLE [dbo].[Subject_Dimension]  WITH CHECK ADD  CONSTRAINT [FK_Subject_Dimension_Course] FOREIGN KEY([course_id])
REFERENCES [dbo].[Course] ([course_id])
GO
ALTER TABLE [dbo].[Subject_Dimension] CHECK CONSTRAINT [FK_Subject_Dimension_Course]
GO
ALTER TABLE [dbo].[Topic]  WITH CHECK ADD  CONSTRAINT [FK_Section_Course] FOREIGN KEY([course_id])
REFERENCES [dbo].[Course] ([course_id])
GO
ALTER TABLE [dbo].[Topic] CHECK CONSTRAINT [FK_Section_Course]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Role] FOREIGN KEY([role_id])
REFERENCES [dbo].[Role] ([role_id])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Role]
GO
USE [master]
GO
ALTER DATABASE [OnlineLearn_SWP391_G5] SET  READ_WRITE 
GO
